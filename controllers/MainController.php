<?php

namespace app\controllers;

use app\helpers\Constants;
use app\models\LoginForm;
use app\models\User;
use Yii;
use app\components\Controller;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * Базовый контроллер для приложения
 *
 * @copyright 	2017 Alex Nem
 * @link 		https://github.com/darkoffalex
 * @author 		Alex Nem
 *
 * @package app\controllers
 */
class MainController extends Controller
{

    public function init()
    {
        parent::init();

        Yii::$app->user->loginUrl = '/login';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?', '@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['user'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Обработчик ошибок (эмуляция action'а error)
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Главная страница
     * @return string
     */
    public function actionIndex()
    {
        /** @var User $user */
        $user = Yii::$app->user->identity;

        if($user) {

            switch ($user->role) {
                case Constants::RBAC_DIRECTOR:
                case Constants::RBAC_ADMIN:
                    $redirectUrl = Url::to(['/account/main']);
                    break;
                case Constants::RBAC_TEACHER:
                    $redirectUrl = Url::to(['/teacher']);
                    break;
                case Constants::RBAC_STUDENT:
                    $redirectUrl = Url::to(['/student']);
                    break;
                default:
                    $redirectUrl = Url::to(['/']);
            }

            return $this->redirect($redirectUrl);
        }


        return $this->render('index');
    }

    /**
     * Logout
     *
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(Url::to(['/']));
    }

    /**
     * Логин админ-пользователя при помощи формы
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'login';
        $this->view->title = 'Авторизация';

        if (!Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['/']));
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            $user = $model->getUser();

            switch ($user->role) {
                case Constants::RBAC_DIRECTOR:
                case Constants::RBAC_ADMIN:
                    $redirectUrl = Url::to(['/account/main']);
                    break;
                case Constants::RBAC_TEACHER:
                    $redirectUrl = Url::to(['/teacher']);
                    break;
                case Constants::RBAC_STUDENT:
                    $redirectUrl = Url::to(['/student']);
                    break;
                default:
                    $redirectUrl = Url::to(['/']);
            }

            return $this->redirect($redirectUrl);
        }

        return $this->render('login', compact('model'));
    }
}
