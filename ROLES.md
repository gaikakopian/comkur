Роли и полномочия
===============

Система полномия построена на [RBAC](http://www.yiiframework.com/doc-2.0/guide-security-authorization.html#rbac).

Базовая роль - Пользователь, все остальные роли наследуются от нее.

Системные роли
------------

| Роль          | Обозначение   |
|---------------|---------------|
| Пользователь  | user          |
| Директор      | director      |
| Администратор | administrator |
| Педагог       | teacher       |
| Ученик        | student       |

Таблица полномочий
------------


| Роль/Полномочие | Пользователь | Директор | Администратор | Педагог | Ученик |
|-----------------|:------------:|:--------:|:-------------:|:-------:|:------:|
| showProfile     |              |          |               |         |        |
| updateProfile   |       +      |          |               |         |        |
| manageUsers     |              |     +    |               |         |        |
| manageStudents  |              |     +    |       +       |         |        |
| manageTeachers  |              |     +    |       +       |         |        |
| createUser      |              |     +    |               |         |        |
| updateUser      |              |     +    |               |         |        |
| deleteUser      |              |     +    |               |         |        |
| manageCourses   |              |     +    |               |         |        |
| createCourse    |              |     +    |               |         |        |
| updateCourse    |              |     +    |               |         |        |
| deleteCourse    |              |     +    |               |         |        |
| manageLessons   |              |     +    |               |         |        |
| createLesson    |              |     +    |               |         |        |
| updateLesson    |              |     +    |               |         |        |
| deleteLesson    |              |     +    |               |         |        |
| learnLesson     |              |          |               |         |    +   |
| reviewLesson    |              |          |               |    +    |        |
| planLesson      |              |          |               |    +    |        |
| uploadImage     |       +      |          |               |         |        |
| uploadFile      |       +      |          |               |         |        |
| downloadFile    |       +      |          |               |         |        |
| useChat         |       +      |          |               |         |        |

Управление ролями в системе
------------

### Назначение роли

```php
$role = Yii::$app->authManager->getRole('director');
Yii::$app->authManager->assign($role, $user->id);
```

### Проверка роли

```php
$role = Yii::$app->authManager->getRole('director');
$hasRole = Yii::$app->authManager->checkAccess($user->id, $role);
```

### Проверка полномочия

```php
$hasPermission = Yii::$app->authManager->checkAccess($user->id, 'manageUsers');
```