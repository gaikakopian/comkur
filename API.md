REST API
===============

API реализовано по технологии [REST](http://www.yiiframework.com/doc-2.0/guide-rest-quick-start.html) в виде отдельного модуля "API"

Авторизация реализована с композитным подходом проверки ключа:

1) HttpBasic
2) HttpBearerAuth
3) QueryParamAuth

В самом простом случае, для взаимодействием с API токен необходимо передавать в виде `GET` параметра `access-token`.
Сам `access-token` привязывается к пользователю, и при работе с ним доступны те же полномочия, что имеет пользователь-владелец токена.

Выполнение запросов
------------

Для выполнения запросов необходимо явно указывать Content-Type с помощью соответственного заголовка:

`Content-Type: application/json`

Все поля, передаваемые при выполнении `POST` запросов должны быть валидным JSON, передаваемым в теле запроса.


Генерация access-token
------------

Токен авторизации `access-token` геренируется автоматически при регистрации каждого пользователя. 
Для использования ключа директора и/или администратора, необходимо авторизоваться в системе с нужной учетной записью,
перейти в раздел Настройки -> API и скопировать ключ.

Так же есть возможнось повторной генерации ключа.

**Внимание**

После регенерации - старые ключи использовать нельзя. При проверка сверяются только активные ключи.

Получение списка курсов
------------

*авторизация не требуется*

Пример запроса:

`GET` http://study2.local/api/courses

Пример ответа:

```json
[
    {
        "id": 1,
        "name": "Тестовый курс (2 занятия)",
        "description": "<p>Описание тестового курса</p>"
    },
    {
        "id": 2,
        "name": "Новый курс (0 занятий)",
        "description": "<p>Описание нового курса</p>"
    }
]
```

Получение списка студентов
------------

Пример запроса:

`GET` http://study2.local/api/students?access-token=[token]

Пример ответа:

```json
[
    {
        "id": 10,
        "username": "apitest@example.com",
        "name": "Api Tester",
        "skype": null,
        "phone_number": null
    },
    {
        "id": 4,
        "username": "student@example.com",
        "name": "Ученик",
        "skype": "2",
        "phone_number": "1"
    },
    {
        "id": 5,
        "username": "student2@example.com",
        "name": "Ученик 2",
        "skype": "2",
        "phone_number": "1"
    }
]
```


Регистрация нового студента
------------

Пример запроса:

`POST` http://study2.local/api/students/create?access-token=[token]

```json
{
	"email": "apitest@example.com",
	"name": "Api Tester",
	"courseId": 1,
	"part": 3,
	"notify": true
}
```

Пример успешного ответа:

```json
{
    "status": "ok",
    "id": 10
}
```

Пример неуспешного ответа:

```json
{
    "status": "error",
    "messages": {
        "email": ["Необходимо заполнить «Email»."]
    }
}
```

Привязка курса к студенту
------------

Пример запроса:

`POST` http://study2.local/api/students/append-course?access-token=[token]

```json
{
	"studentId": 10,
	"courseId": 2
}
```

Пример ответа:

```json
{
    "status": "ok"
}
```

Получение списка уроков
------------

Пример запроса:

`GET` http://study2.local/api/lessons?access-token=[token]

Пример ответа:

```json
[
    {
        "id": 5,
        "code": "kak-bystro-razogret-golos",
        "name": "Как быстро разогреть голос",
        "image_url": null,
        "video_url": null,
        "description": "<p><iframe width=\"500\" height=\"281\" src=\"//www.youtube.com/embed/TT4b-FVdreM\" frameborder=\"0\" allowfullscreen=\"\"></iframe></p>",
        "course_id": null,
        "theme_id": 3,
        "course_part": null,
        "priority": 0,
        "create_date": "2017-09-13 21:15:54",
        "update_date": "2017-09-13 21:16:48"
    }
]
```