<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "chat".
 *
 * @property integer $id
 * @property integer $author_id
 * @property integer $recipient_id
 * @property string $title
 * @property string $create_date
 * @property string $update_date
 * @property string $author_last_date
 * @property string $recipient_last_date
 * @property integer $author_unread_count
 * @property integer $recipient_unread_count
 *
 * @property User $author
 * @property User $recipient
 * @property Message[] $messages
 */
class Chat extends \yii\db\ActiveRecord
{

    public $message;
    public $recipient_name;
    public $recipient_group;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id', 'message'], 'required'],
            [['author_id', 'recipient_id', 'author_unread_count', 'recipient_unread_count'], 'integer'],
            [['create_date', 'update_date', 'message', 'author_last_date', 'recipient_last_date', 'recipient_name', 'recipient_group'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['recipient_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['recipient_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_date',
                'updatedAtAttribute' => 'update_date',
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'author_id' => 'Автор',
            'recipient_id' => 'Получатель',
            'recipient_name' => 'Получатель',
            'recipient_group' => 'Группа получателей',
            'title' => 'Заголовок',
            'create_date' => 'Дата создания',
            'update_date' => 'Дата изменения',
            'author_last_date' => 'Дата прочтения автором',
            'author_unread_count' => 'Не прочитано автором',
            'recipient_last_date' => 'Дата прочтения получателем',
            'recipient_unread_count' => 'Не прочитано получателем',
            'message' => 'Сообщение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipient()
    {
        return $this->hasOne(User::className(), ['id' => 'recipient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['chat_id' => 'id']);
    }

    /**
     * Get unread messages count
     *
     * @param $id
     * @param $authorId
     * @param bool $admin
     *
     * @return int
     */
    public static function unreadCount($id, $authorId, $admin = false)
    {

        $ownCount = Message::find()
            ->joinWith('chat')
            ->where(['{{message}}.recipient_id' => $authorId])
            ->andWhere(['<', '{{chat}}.author_last_date', new Expression('{{message}}.create_date')])
            ->andWhere(['{{chat}}.author_id' => $authorId, '{{chat}}.id' => $id])
            ->andWhere(['<>', '{{message}}.author_id', $authorId])
            ->count();

        $twoCount = Message::find()
            ->joinWith('chat')
            ->where(['{{message}}.recipient_id' => $authorId])
            ->andWhere(['<', '{{chat}}.recipient_last_date', new Expression('{{message}}.create_date')])
            ->andWhere(['{{chat}}.recipient_id' => $authorId, '{{chat}}.id' => $id])
            ->andWhere(['<>', '{{message}}.author_id', $authorId])
            ->count();

        $threeCount = 0;

        if($admin) {

            $threeCount = Message::find()
                ->joinWith('chat')
                ->where(['{{message}}.recipient_id' => null])
                ->andWhere(['<', '{{chat}}.recipient_last_date', new Expression('{{message}}.create_date')])
                ->andWhere(['{{chat}}.recipient_id' => null, '{{chat}}.id' => $id])
                ->andWhere(['<>', '{{message}}.author_id', $authorId])
                ->count();
        }

        return $ownCount + $twoCount + $threeCount;
    }
}
