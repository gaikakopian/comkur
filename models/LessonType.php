<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lesson_type".
 *
 * @property integer $id
 * @property string $name
 */
class LessonType extends \yii\db\ActiveRecord
{
    const TYPE_OFFLINE = 1;
    const TYPE_ONLINE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lesson_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'name' => 'Наименование',
        ];
    }
}
