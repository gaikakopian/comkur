<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lesson_status".
 *
 * @property integer $id
 * @property string $name
 */
class LessonStatus extends \yii\db\ActiveRecord
{
    const STATUS_PLANNED    = 1; // Запланирован
    const STATUS_AVAILABLE  = 2; // Назначен
    const STATUS_LEARNING   = 3; // Изучение
    const STATUS_REVIEW     = 4; // Отправлен
    const STATUS_FINISHED   = 5; // Завершен

    const DEFAULT_STATUS = self::STATUS_PLANNED;
    const STATE_OPEN = [self::STATUS_AVAILABLE, self::STATUS_LEARNING, self::STATUS_REVIEW, self::STATUS_FINISHED];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lesson_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'name' => 'Статус',
        ];
    }
}
