<?php

namespace app\models;

use Yii;

/**
 * Class CourseOfStudent
 * @package app\models
 */
class StudentCourse extends StudentCourseDB
{
    /**
     * @var bool translate labels
     */
    public $trlLabels = true;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['course_id'], 'required'];
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $baseLabels = parent::attributeLabels();

        if($this->trlLabels){
            foreach($baseLabels as $attribute => $label){
                $baseLabels[$attribute] = Yii::t('app',$label);
            }
        }

        return $baseLabels;
    }
}
