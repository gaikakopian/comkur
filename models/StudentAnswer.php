<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "student_answer".
 *
 * @property integer $id
 * @property integer $course_id
 * @property integer $lesson_id
 * @property integer $user_id
 * @property string $content
 * @property string $video_url
 * @property string $create_date
 * @property string $update_date
 * @property string $expire_date
 *
 * @property Course $course
 * @property Lesson $lesson
 */
class StudentAnswer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_answer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'lesson_id', 'user_id'], 'integer'],
            [['lesson_id', 'user_id'], 'required'],
            [['content'], 'string'],
            [['video_url'], 'url'],
            [['create_date', 'update_date', 'expire_date'], 'safe'],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['course_id' => 'id']],
            [['lesson_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lesson::className(), 'targetAttribute' => ['lesson_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_date',
                'updatedAtAttribute' => 'update_date',
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'course_id' => 'Курс',
            'lesson_id' => 'Урок',
            'user_id' => 'Студент',
            'content' => 'Ответ',
            'video_url' => 'Ссылка на видео',
            'create_date' => 'Дата создания',
            'update_date' => 'Дата изменения',
            'expire_date' => 'Срок истечения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLesson()
    {
        return $this->hasOne(Lesson::className(), ['id' => 'lesson_id']);
    }
}
