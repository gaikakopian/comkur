<?php

namespace app\models;

use Yii;
use app\helpers\Constants;
use Imagine\Exception\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * Class User
 *
 * @package app\models
 *
 * @property User $teacher
 * @property User[] $students
 * @property User[] $teachers
 */
class User extends UserDB implements IdentityInterface
{
    public $role;
    public $primaryRole;

    /**
     * @var string password string
     */
    public $password;

    /**
     * @var bool translate labels
     */
    public $trlLabels = true;

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status_id' => [UserStatus::STATUS_ACTIVE]]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token, 'status_id' => [UserStatus::STATUS_ACTIVE]]);
    }

    /**
     * Finds user by username
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status_id' => [UserStatus::STATUS_ACTIVE]]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Finds out if password reset token is valid
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * Finds user by password reset token
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status_id' => Constants::STATUS_ENABLED,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $baseLabels = parent::attributeLabels();
        $baseLabels['password'] = 'Password';

        if($this->trlLabels){
            foreach($baseLabels as $attribute => $label){
                $baseLabels[$attribute] = Yii::t('app', $label);
            }
        }

        return $baseLabels;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['username', 'unique'];
        $rules[] = ['username', 'required'];
        $rules[] = ['username', 'email'];

        $rules[] = ['password', 'required', 'on' => 'create'];
        $rules[] = ['password', 'string', 'min' => 6];

        $rules[] = ['role', 'required', 'on' => 'create'];

        return $rules;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(User::className(), ['id' => 'teacher_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentsRelation()
    {
        return $this->hasMany(StudentTeacher::className(), ['teacher_id' => 'id'])
            ->andWhere(['is_active' => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeachersRelation()
    {
        return $this->hasMany(StudentTeacher::className(), ['student_id' => 'id'])
            ->andWhere(['is_active' => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(User::className(), ['id' => 'student_id'])
            ->via('studentsRelation');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeachers()
    {
        return $this->hasMany(User::className(), ['id' => 'teacher_id'])
            ->via('teachersRelation');
    }

    /**
     * Check user role
     *
     * @param $role
     *
     * @return bool
     */
    public function hasRole($role)
    {
        return Yii::$app->authManager->checkAccess($this->id, $role);
    }

    /**
     * Get user primary role
     *
     * @return mixed
     */
    public function getPrimaryRole()
    {
        $roles = Yii::$app->authManager->getRolesByUser($this->id);

        return $roles ? array_keys(Yii::$app->authManager->getRolesByUser($this->id))[0] : null;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->role = $this->getPrimaryRole();
        //$this->birth_date = $this->birth_date ? \DateTime::createFromFormat('Y-m-d', $this->birth_date)->format('d.m.Y') : $this->birth_date;
    }
}
