<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "course_type".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Course[] $courses
 */
class CourseType extends \yii\db\ActiveRecord
{

    const TYPE_REGULAR = 1;
    const TYPE_DEMO = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'name' => 'Тип курса',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(Course::className(), ['type_id' => 'id']);
    }
}
