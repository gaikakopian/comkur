<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "student_lesson".
 *
 * @property integer $id
 * @property integer $lesson_id
 * @property integer $user_id
 * @property integer $course_id
 * @property integer $parent_id
 * @property integer $type_id
 * @property integer $status_id
 * @property integer $priority
 *
 * @property string $start_date
 * @property string $end_date
 * @property string $expire_date
 * @property string $create_date
 * @property string $update_date
 *
 * @property Course $course
 * @property Lesson $lesson
 * @property User $user
 * @property LessonType $type
 * @property StudentCourse $parent
 */
class StudentLessonDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_lesson';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['lesson_id', 'user_id', 'course_id', 'priority', 'type_id', 'parent_id', 'status_id'], 'integer'],
            [['create_date', 'update_date', 'start_date', 'end_date', 'expire_date', 'previous_id', 'start_time'], 'safe'],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['course_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => StudentCourse::className(), 'targetAttribute' => ['parent_id' => 'id']],
            [['lesson_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lesson::className(), 'targetAttribute' => ['lesson_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => LessonStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_date',
                'updatedAtAttribute' => 'update_date',
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lesson_id' => 'Урок',
            'user_id' => 'Ученик',
            'course_id' => 'Курс',
            'parent_id' => 'Parent ID',
            'type_id' => 'Тип',
            'status_id' => 'Статус',
            'priority' => 'Приоритет',
            'create_date' => 'Дата создания',
            'update_date' => 'Дата изменения',
            'start_date' => 'Дата начала',
            'start_time' => 'Время',
            'previous_id' => 'Добавить после...',
            'theme' => 'Тема урока'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLesson()
    {
        return $this->hasOne(Lesson::className(), ['id' => 'lesson_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(LessonType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(StudentCourse::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(LessonStatus::className(), ['id' => 'status_id']);
    }

    /**
     * Check lesson available status
     * @return bool
     */
    public function isAvailable()
    {
        if($this->status_id == LessonStatus::STATUS_PLANNED)
            return false;

        if($this->parent->access_for_part && $this->lesson->course_part > $this->parent->access_for_part)
            return false;

        if($this->type_id == LessonType::TYPE_ONLINE)
            return false;

        return true;
    }
}
