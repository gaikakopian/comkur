<?php

namespace app\models;

use app\helpers\Help;
use Yii;
use app\models\StudentCourse;
use yii\data\ActiveDataProvider;

/**
 * Class Goods
 * @package app\models
 */
class PaymentsSearch extends Payments
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id','status'], 'string'],
            [['id', 'user_id', 'good_id','price'], 'integer'],
            [['created_at'], 'date', 'format' => 'dd.MM.yyyy - dd.MM.yyyy'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $baseLabels = parent::attributeLabels();
        return $baseLabels;
    }

    /**
     * Посроение запроса поиска
     *
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        
        $q = parent::find()->orderBy(['created_at' => SORT_DESC]);

        $this->load($params);

        if($this->validate()){

            if(!empty($this->id)){
                $q->andWhere(['id' => $this->id]);
            }

            if(!empty($this->good_id)){
                $q->andWhere(['good_id' => $this->good_id]);
            }

            if(!empty($this->status)){
                $q->andWhere(['status' => $this->status]);
            }

            if(!empty($this->order_id)){
                $q->andWhere(['order_id' => $this->order_id]);
            }

            if(!empty($this->created_at)){
                $range = explode(' - ',$this->created_at);
                $date_from = Help::dateReformat($range[0],'Y-m-d','d.m.Y');
                $date_to = Help::dateReformat($range[1],'Y-m-d','d.m.Y');
                $q->andWhere('created_at >= :from2 AND created_at <= :to2',['from2' => $date_from, 'to2' => $date_to]);
            }
        }

        return new ActiveDataProvider([
            'query' => $q,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
    }
}
