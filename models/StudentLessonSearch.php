<?php

namespace app\models;

use app\helpers\Help;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class Course
 * @package app\models
 */
class StudentLessonSearch extends StudentLesson
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lesson_id', 'parent_id', 'status_id'], 'integer'],
            [['course_id', 'user_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $baseLabels = parent::attributeLabels();
        return $baseLabels;
    }

    /**
     * Посроение запроса поиска
     *
     * @param $params
     * @param bool $curseLessons
     *
     * @return ActiveDataProvider
     */
    public function search($params, $curseLessons = true)
    {
        $q = parent::find()->with(['course']);

        if($curseLessons === true)
            $q->andWhere(['is not', 'course_id', null]);
        elseif($curseLessons === false)
            $q->andWhere(['course_id' => null]);

        $this->load($params);

        if($this->validate()){

            if(!empty($this->id)){
                $q->andWhere(['id' => $this->id]);
            }

            if(!empty($this->parent_id)){
                $q->andWhere(['parent_id' => $this->parent_id]);
            }

            if(!empty($this->course_id)){
                $q->andWhere(['course_id' => $this->course_id]);
            }

            if(!empty($this->user_id)){
                $q->andWhere(['user_id' => $this->user_id]);
            }

            if(!empty($this->type_id)){
                $q->andWhere(['type_id' => $this->type_id]);
            }

            if(!empty($this->status_id)){
                $q->andWhere(['status_id' => $this->status_id]);
            }

            if(!empty($this->name)){
                $q->andWhere(['like','name', $this->name]);
            }

            if(!empty($this->description)){
                $q->andWhere(['like', 'description', $this->description]);
            }

            if(!empty($this->create_date)){
                $range = explode(' - ',$this->create_date);
                $date_from = Help::dateReformat($range[0],'Y-m-d','d.m.Y');
                $date_to = Help::dateReformat($range[1],'Y-m-d','d.m.Y');
                $q->andWhere('created_at >= :from2 AND created_at <= :to2',['from2' => $date_from, 'to2' => $date_to]);
            }
        }

        return new ActiveDataProvider([
            'query' => $q,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => ['priority' => SORT_ASC]
            ]
        ]);
    }
}
