<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "template".
 *
 * @property integer $id
 * @property string $code
 * @property string $title
 * @property string $content
 * @property string $create_date
 * @property string $update_date
 */
class Template extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'title', 'content'], 'required'],
            [['content'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['code', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_date',
                'updatedAtAttribute' => 'update_date',
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'code' => 'Код',
            'title' => 'Заголовок',
            'content' => 'Сообщение',
            'create_date' => 'Дата создания',
            'update_date' => 'Дата изменения',
        ];
    }
}
