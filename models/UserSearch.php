<?php

namespace app\models;

use app\helpers\Help;
use yii\data\ActiveDataProvider;

class UserSearch extends User
{

    public $role;

    /**
     * Validation rules for search
     * @return array
     */
    public function rules()
    {
        return [
            [['username', 'name', 'role'], 'string', 'max' => 255],
            [['status_id', 'role_id', 'id', 'teacher_id'], 'integer'],
            [['created_at', 'last_online_at'], 'date', 'format' => 'dd.MM.yyyy - dd.MM.yyyy']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $baseLabels = parent::attributeLabels();

        return $baseLabels;
    }

    /**
     * Build search query and return as result data provider
     *
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params = null)
    {
        $q = User::find()->joinWith(['roles']);

        if ($params)
            $this->load($params);

        if ($this->validate()) {

            if (!empty($this->id)) {
                $q->andWhere(['id' => $this->id]);
            }

            if (!empty($this->username)) {
                $q->andWhere(['like', 'username', $this->username]);
            }

            if (!empty($this->name)) {
                $q->andWhere(['like', 'name', $this->name]);
            }

            if (!empty($this->role_id)) {
                $q->andWhere(['role_id' => $this->role_id]);
            }

            if (!empty($this->status_id)) {
                $q->andWhere(['status_id' => $this->status_id]);
            }

            if($this->teacher_id === -1)
                $q->andWhere(['teacher_id' => null]);
            elseif (!empty($this->teacher_id)) {
                $q->andWhere(['teacher_id' => $this->teacher_id]);
            }

            if (!empty($this->role)) {
                $q->andWhere(['item_name' => $this->role]);
            }

            if (!empty($this->last_online_at)) {
                $range = explode(' - ', $this->last_online_at);
                $date_from = Help::dateReformat($range[0], 'Y-m-d', 'd.m.Y');
                $date_to = Help::dateReformat($range[1], 'Y-m-d', 'd.m.Y');
                $q->andWhere('last_online_at >= :from1 AND last_online_at <= :to1', ['from1' => $date_from, 'to1' => $date_to]);
            }

            if (!empty($this->created_at)) {
                $range = explode(' - ', $this->created_at);
                $date_from = Help::dateReformat($range[0], 'Y-m-d', 'd.m.Y');
                $date_to = Help::dateReformat($range[1], 'Y-m-d', 'd.m.Y');
                $q->andWhere('created_at >= :from2 AND created_at <= :to2', ['from2' => $date_from, 'to2' => $date_to]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $q,
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $dataProvider->sort->attributes['role'] = [
            'asc' => ['auth_assignment.item_name' => SORT_ASC],
            'desc' => ['auth_assignment.item_name' => SORT_DESC],
        ];

        return $dataProvider;
    }
}