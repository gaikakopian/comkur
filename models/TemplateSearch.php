<?php

namespace app\models;

use app\helpers\Help;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class TemplateSearch
 * @package app\models
 */
class TemplateSearch extends Template
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'title', 'content'], 'string'],
            [['id'], 'integer'],
            [['create_date'], 'date', 'format' => 'dd.MM.yyyy - dd.MM.yyyy'],
        ];
    }

    /**
     * Построение запроса поиска
     *
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $q = parent::find();

        $this->load($params);

        if($this->validate()){

            if(!empty($this->id)){
                $q->andWhere(['id' => $this->id]);
            }

            if(!empty($this->code)){
                $q->andWhere(['code' => $this->code]);
            }

            if(!empty($this->create_date)){
                $range = explode(' - ',$this->create_date);
                $date_from = Help::dateReformat($range[0],'Y-m-d','d.m.Y');
                $date_to = Help::dateReformat($range[1],'Y-m-d','d.m.Y');
                $q->andWhere('create_date >= :from2 AND create_date <= :to2',['from2' => $date_from, 'to2' => $date_to]);
            }
        }

        return new ActiveDataProvider([
            'query' => $q,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
    }
}
