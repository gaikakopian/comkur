<?php

namespace app\models;

use app\helpers\Help;
use Yii;
use app\models\StudentCourse;
use yii\data\ActiveDataProvider;

/**
 * Class Goods
 * @package app\models
 */
class GoodsSearch extends Goods
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title','short_descr','photo','type'], 'string'],
            [['id', 'course_id', 'part','price'], 'integer'],
            [['created_at'], 'date', 'format' => 'dd.MM.yyyy - dd.MM.yyyy'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $baseLabels = parent::attributeLabels();
        return $baseLabels;
    }

    /**
     * Посроение запроса поиска
     *
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {   
        $q = parent::find()
                ->orderBy(['priority' => SORT_ASC]);

        $this->load($params);

        if($this->validate()){

            if(!empty($this->id)){
                $q->andWhere(['id' => $this->id]);
            }

            if(!empty($this->course_id)){
                $q->andWhere(['course_id' => $this->course_id]);
            }

            if(!empty($this->stage)){
                $q->andWhere(['stage' => $this->stage]);
            }

            if(!empty($this->part)){
                $q->andWhere(['part' => $this->part]);
            }

            if(!empty($this->type)){
                $q->andWhere(['type' => $this->type]);
            }

            if(!empty($this->title)){
                $q->andWhere(['like','title', $this->title]);
            }

            if(!empty($this->short_descr)){
                $q->andWhere(['like', 'short_descr', $this->short_descr]);
            }

            if(!empty($this->created_at)){
                $range = explode(' - ',$this->created_at);
                $date_from = Help::dateReformat($range[0],'Y-m-d','d.m.Y');
                $date_to = Help::dateReformat($range[1],'Y-m-d','d.m.Y');
                $q->andWhere('created_at >= :from2 AND created_at <= :to2',['from2' => $date_from, 'to2' => $date_to]);
            }
        }

        return new ActiveDataProvider([
            'query' => $q,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
    }
}
