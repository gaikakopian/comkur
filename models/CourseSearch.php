<?php

namespace app\models;

use app\helpers\Help;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class Course
 * @package app\models
 */
class CourseSearch extends Course
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','description'], 'string'],
            [['type_id'], 'integer'],
            [['create_date'], 'date', 'format' => 'dd.MM.yyyy - dd.MM.yyyy'],
            [['id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $baseLabels = parent::attributeLabels();
        return $baseLabels;
    }

    /**
     * Посроение запроса поиска
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $q = parent::find()->with('lessons');

        $this->load($params);

        if($this->validate()){

            if(!empty($this->id)){
                $q->andWhere(['id' => $this->id]);
            }

            if(!empty($this->name)){
                $q->andWhere(['like','name', $this->name]);
            }

            if(!empty($this->description)){
                $q->andWhere(['like','description', $this->description]);
            }

            if(!empty($this->type_id)){
                $q->andWhere(['type_id' => $this->type_id]);
            }

            if(!empty($this->create_date)){
                $range = explode(' - ',$this->create_date);
                $date_from = Help::dateReformat($range[0],'Y-m-d','d.m.Y');
                $date_to = Help::dateReformat($range[1],'Y-m-d','d.m.Y');
                $q->andWhere('create_date >= :from2 AND create_date <= :to2',['from2' => $date_from, 'to2' => $date_to]);
            }
        }

        return new ActiveDataProvider([
            'query' => $q,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
    }
}
