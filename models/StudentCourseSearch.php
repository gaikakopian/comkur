<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class CourseOfStudent
 * @package app\models
 */
class StudentCourseSearch extends StudentCourseDB
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'course_id', 'user_id', 'access_for_part'], 'integer'],
            [['id'], 'safe']
        ];
    }

    /**
     * Посроение запроса поиска
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $q = parent::find();

        $this->load($params);

        if($this->validate()){

            if(!empty($this->id)){
                $q->andWhere(['id' => $this->id]);
            }

            if(!empty($this->user_id)){
                $q->andWhere(['user_id' => $this->user_id]);
            }

            if(!empty($this->course_id)){
                $q->andWhere(['course_id' => $this->course_id]);
            }

        }

        return new ActiveDataProvider([
            'query' => $q,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
    }
}
