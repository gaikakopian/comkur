<?php

namespace app\models;

use Yii;

/**
 * Class Goods
 * @package app\models
 */
class Goods extends GoodsDB
{
    /**
     * @var bool translate labels
     */
    public $trlLabels = true;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $baseLabels = parent::attributeLabels();

        if($this->trlLabels){
            foreach($baseLabels as $attribute => $label){
                $baseLabels[$attribute] = Yii::t('app',$label);
            }
        }

        return $baseLabels;
    }
}
