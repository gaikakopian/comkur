<?php

namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "lesson_pattern".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $image_url
 * @property string $video_url
 * @property string $description
 * @property integer $course_id
 * @property integer $theme_id
 * @property integer $course_part
 * @property integer $priority
 * @property string $create_date
 * @property string $update_date
 * @property integer $course_stage
 *
 * @property StudentLesson[] $lessonOfStudents
 * @property Course $course
 * @property LessonTheme $theme
 */
class LessonDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lesson';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['code', 'description'], 'string'],
            [['course_id', 'course_part', 'priority','course_stage'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['name', 'image_url'], 'string', 'max' => 255],
            [['video_url'], 'url'],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['course_id' => 'id']],
            [['theme_id'], 'exist', 'skipOnError' => true, 'targetClass' => LessonTheme::className(), 'targetAttribute' => ['theme_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_date',
                'updatedAtAttribute' => 'update_date',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'code'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'code' => 'Код',
            'image_url' => 'Ссылка на изображение',
            'video_url' => 'Ссылка на видео',
            'description' => 'Описание',
            'course_id' => 'Курс',
            'theme_id' => 'Тема',
            'course_part' => 'Часть',
            'priority' => 'Номер',
            'create_date' => 'Дата создания',
            'update_date' => 'Дата изменения',
            'theme_name' => 'Тема',
            'course_stage' => 'Ступень'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessonOfStudents()
    {
        return $this->hasMany(StudentLesson::className(), ['lesson_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTheme()
    {
        return $this->hasOne(LessonTheme::className(), ['id' => 'theme_id']);
    }
}
