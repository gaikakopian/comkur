<?php

namespace app\models;

use Yii;

/**
 * Class LessonPattern
 * @package app\models
 */
class Lesson extends LessonDB
{
    /**
     * @var bool translate labels
     */
    public $trlLabels = true;

    public $theme_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();

        $rules[] = [['video_url'], 'url'];
        $rules[] = [['theme_name'], 'safe', 'when' => function($model) {
            return $model->course_id == null;
        }];

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $baseLabels = parent::attributeLabels();

        if($this->trlLabels){
            foreach($baseLabels as $attribute => $label){
                $baseLabels[$attribute] = Yii::t('app',$label);
            }
        }

        return $baseLabels;
    }
}
