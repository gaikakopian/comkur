<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "teacher_review".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $lesson_id
 * @property string $content
 * @property string $create_date
 * @property string $update_date
 *
 * @property User $user
 * @property StudentLesson $lesson
 */
class TeacherReview extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teacher_review';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'lesson_id', 'content'], 'required'],
            [['user_id', 'lesson_id'], 'integer'],
            [['content'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['lesson_id'], 'exist', 'skipOnError' => true, 'targetClass' => StudentLesson::className(), 'targetAttribute' => ['lesson_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_date',
                'updatedAtAttribute' => 'update_date',
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'user_id' => 'Автор',
            'lesson_id' => 'Занятие',
            'content' => 'Отчет',
            'create_date' => 'Дата создания',
            'update_date' => 'Дата изменения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLesson()
    {
        return $this->hasOne(StudentLesson::className(), ['id' => 'lesson_id']);
    }
}
