<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\models\Goods;

/**
 * This is the model class for table "payments".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $order_id
 * @property integer $price
 * @property integer $good_id
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 *
 */
class PaymentsDB extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','order_id','price','good_id'], 'required'],
            [['order_id'], 'string'],
            [['user_id', 'price','good_id'], 'integer'],
            [['created_at', 'updated_at','status'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'order_id' => 'Заказ',
            'price' => 'Цена',
            'good_id' => 'Товар',
            'status' => 'Статус',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения'
        ];
    }
}
