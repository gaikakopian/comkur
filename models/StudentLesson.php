<?php

namespace app\models;

use Yii;

/**
 * Class LessonOfStudent
 * @package app\models
 */
class StudentLesson extends StudentLessonDB
{

    public $previous_id;
    public $start_time;
    public $theme;

    /**
     * @var bool translate labels
     */
    public $trlLabels = true;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $baseLabels = parent::attributeLabels();

        if($this->trlLabels){
            foreach($baseLabels as $attribute => $label){
                $baseLabels[$attribute] = Yii::t('app', $label);
            }
        }

        return $baseLabels;
    }
}
