<?php

namespace app\models;

use app\helpers\Help;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * Class ChatSearch
 * @package app\models
 */
class ChatSearch extends Chat
{

    public $admin = false;
    public $user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string'],
            [['id', 'author_id'], 'integer'],
            [['create_date'], 'date', 'format' => 'dd.MM.yyyy - dd.MM.yyyy'],
            [['recipient_id', 'author_last_date', 'recipient_last_date'], 'safe']
        ];
    }

    /**
     * Посроение запроса поиска
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $q = static::find();//->joinWith('messages');

        $this->load($params);

        if($this->validate()){

            if(!empty($this->id)){
                $q->andWhere(['id' => $this->id]);
            }

            if(!empty($this->title)){
                $q->andWhere(['like', 'title', $this->title]);
            }

            if($this->admin) {
                $q->andWhere(['or', ['author_id' => Yii::$app->user->identity->getId()], ['recipient_id' => Yii::$app->user->identity->getId()], ['recipient_id' => null]]);
            } elseif($this->user) {
                $q->andWhere(['or', ['author_id' => Yii::$app->user->identity->getId()], ['recipient_id' => Yii::$app->user->identity->getId()]]);
            }

            else {

                if(!empty($this->author_id)) {
                    $q->andWhere(['author_id' => $this->author_id]);
                }

                if(!empty($this->recipient_id)) {

                    $q->andWhere(['recipient_id' => $this->recipient_id]);

                    if(is_array($this->recipient_id) && $this->recipient_id[1] === null)
                        $q->orWhere(['recipient_id' => null]);
                }
            }

            if(!empty($this->create_date)){
                $range = explode(' - ',$this->create_date);
                $date_from = Help::dateReformat($range[0],'Y-m-d','d.m.Y');
                $date_to = Help::dateReformat($range[1],'Y-m-d','d.m.Y');
                $q->andWhere('create_date >= :from2 AND create_date <= :to2',['from2' => $date_from, 'to2' => $date_to]);
            }
        }

        return new ActiveDataProvider([
            'query' => $q,
            'pagination' =>
                [
                    'pageSize' => 20
                ],
            'sort' => [
                'attributes' => [
                    'status' =>[
                        'asc' => [new Expression("case author_id when '" . Yii::$app->user->identity->getId() ."' then author_unread_count else recipient_unread_count end DESC, update_date DESC")],
                        'desc' => '',
            ]
        ],
        'defaultOrder' => ['status'=>SORT_ASC]
    ],

            ]);
    }
}
