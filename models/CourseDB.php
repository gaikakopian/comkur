<?php

namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "course".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $description
 * @property string $image_url
 * @property string $video_url
 * @property integer $type_id
 * @property string $create_date
 * @property string $update_date
 * @property integer $active

 * @property StudentCourse[] $courseOfStudents
 * @property StudentLesson[] $lessonOfStudents
 * @property Lesson[] $lessons
 * @property Count[] $count
 * @property CourseType $type
 */
class CourseDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['type_id','active'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['name', 'code', 'image_url', 'video_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_date',
                'updatedAtAttribute' => 'update_date',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'code'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => Yii::t('app', 'Course code'),
            'name' => 'Name',
            'description' => 'Description',
            'image_url' => 'Image Url',
            'video_url' => 'Video Url',
            'type_id' => 'Type ID',
            'create_date' => 'Created At',
            'update_date' => 'Updated At',
            'active' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseOfStudents()
    {
        return $this->hasMany(StudentCourse::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessonOfStudents()
    {
        return $this->hasMany(StudentLesson::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessons()
    {
        return $this->hasMany(Lesson::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStages()
    {
        return $this->hasMany(Lesson::className(), ['course_id' => 'id'])->groupBy('course_stage');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParts()
    {
        return $this->hasMany(Lesson::className(), ['course_id' => 'id'])->groupBy('course_part');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(CourseType::className(), ['id' => 'type_id']);
    }

}
