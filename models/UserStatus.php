<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_status".
 *
 * @property integer $id
 * @property string $name
 */
class UserStatus extends \yii\db\ActiveRecord
{

    const STATUS_NEW = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_BLOCKED = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'name' => 'Статус',
        ];
    }
}
