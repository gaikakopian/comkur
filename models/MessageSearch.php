<?php

namespace app\models;

use app\helpers\Help;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class MessageSearch
 * @package app\models
 */
class MessageSearch extends Message
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['id', 'author_id', 'recipient_id', 'is_read', 'chat_id'], 'integer'],
            [['create_date'], 'date', 'format' => 'dd.MM.yyyy - dd.MM.yyyy'],
        ];
    }

    /**
     * Посроение запроса поиска
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $q = parent::find();

        $this->load($params);

        if($this->validate()){

            if(!empty($this->id)){
                $q->andWhere(['id' => $this->id]);
            }

            if(!empty($this->chat_id)){
                $q->andWhere(['id' => $this->chat_id]);
            }

            if(!empty($this->content)){
                $q->andWhere(['like', 'content', $this->content]);
            }

            if(!empty($this->author_id)){
                $q->andWhere(['author_id' => $this->author_id]);
            }

            if(!empty($this->recipient_id)){
                $q->andWhere(['recipient_id' => $this->recipient_id]);
            }

            if(!empty($this->create_date)){
                $range = explode(' - ',$this->create_date);
                $date_from = Help::dateReformat($range[0],'Y-m-d','d.m.Y');
                $date_to = Help::dateReformat($range[1],'Y-m-d','d.m.Y');
                $q->andWhere('create_date >= :from2 AND create_date <= :to2',['from2' => $date_from, 'to2' => $date_to]);
            }
        }

        return new ActiveDataProvider([
            'query' => $q,
            'pagination' => [
                'pageSize' => 20
            ],
        ]);
    }
}
