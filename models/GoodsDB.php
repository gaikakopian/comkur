<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\models\Goods;
use app\models\StudentCourse;

/**
 * This is the model class for table "goods".
 *
 * @property integer $id
 * @property integer $course_id
 * @property string $title
 * @property string $photo
 * @property string $short_descr
 * @property integer $type
 * @property integer $part
 * @property integer $stage
 * @property integer $price
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Course $course
 */
class GoodsDB extends \yii\db\ActiveRecord
{
    public $imageFile;
    public $imageName;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id','price','type','title'], 'required'],
            [['short_descr'], 'string'],
            [['course_id', 'price'], 'integer'],
            [['created_at', 'updated_at','stage','part','active','priority'], 'safe'],
            [['title', 'photo','type',], 'string', 'max' => 255],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['course_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Курс',
            'title' => 'Название',
            'photo' => 'Картинка',
            'imageFile' => 'Картинка',
            'short_descr' => 'Короткое описание',
            'type' => 'Тип',
            'stage' => 'Ступень',
            'part' => 'Часть',
            'price' => 'Цена',
            'active' => 'Активен',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStored()
    {
        return $this->hasOne(StudentCourse::className(), ['good_id' => 'id'])->andOnCondition(['student_course.user_id' => Yii::$app->user->identity->id]);
    }

    // найти по курсу товар
    public static function findGood($course , $stage = null , $part = null){
        
        return Goods::find()
                ->where(['course_id' => $course])
                ->andWhere(['stage' => $stage])
                ->andWhere(['part' => $part])
                ->one();
    }
    // найти по курсу товар для редактирования товара
    public static function findGoodUnique($course , $id, $stage = null , $part = null){
        
        $good =  Goods::find()
                ->where(['course_id' => $course])
                ->andWhere(['stage' => $stage])
                ->andWhere(['part' => $part])
                ->one();
        
        
        if (!$good || $good->id == $id){
            return false;
        }else{
            return true;
        }
    }
    // Сменить статус товара 
    public static function changeStatus($course , $status){
        $goods = Goods::find()
                        ->where(['course_id' => $course])
                        ->all();

        foreach ($goods as $good){
            $good->active = $status;
            $good->save();
        }

        return true;
    }

    // Загрузить фото для товара
    public function upload()
    {
        //Проверка на существование директорий для хранения картинок если не существует они добавляются
        if (!file_exists(Yii::getAlias('@webroot').'/backend/images')) {
            mkdir(Yii::getAlias('@webroot').'/backend/images', 0777, true);
        }
        if (!file_exists(Yii::getAlias('@webroot').'/backend/images/goods')) {
            mkdir(Yii::getAlias('@webroot').'/backend/images/goods', 0777, true);
        }
        if ($this->validate()) {
            $img_name =  md5($this->imageFile->baseName . microtime()) . '.' . $this->imageFile->extension;
            $this->imageFile->saveAs(Yii::getAlias('@webroot').'/backend/images/goods/' .$img_name);
            $this->imageName = $img_name;
            return true;
        } else {
            return false;
        }
    }
}
