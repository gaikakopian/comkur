<?php

namespace app\models;

use app\models\auth\Assignment;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $confirmation_code
 * @property string $name
 * @property string $birth_date
 * @property string $phone_number
 * @property string $skype
 * @property integer $role_id
 * @property integer $status_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $last_login_at
 * @property string $last_online_at
 * @property integer $created_by_id
 * @property integer $updated_by_id
 * @property integer $teacher_id
 *
 * @property StudentCourse[] $courseOfStudents
 * @property StudentLesson[] $lessonOfStudents
 * @property UserStatus $status
 */
class UserDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['birth_date', 'created_at', 'updated_at', 'last_login_at', 'last_online_at', 'role'], 'safe'],
            [['role_id', 'status_id', 'created_by_id', 'updated_by_id', 'teacher_id'], 'integer'],
            [['username', 'auth_key', 'password_hash', 'password_reset_token', 'confirmation_code', 'name', 'phone_number', 'skype'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'confirmation_code' => 'Confirmation Code',
            'name' => 'Name',
            'birth_date' => 'Birth Date',
            'phone_number' => 'Phone Number',
            'skype' => 'Skype',
            'role_id' => 'Role ID',
            'status_id' => 'Status ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'last_login_at' => 'Last Login At',
            'last_online_at' => 'Last Online At',
            'created_by_id' => 'Created By ID',
            'updated_by_id' => 'Updated By ID',
            'teacher_id' => 'Teacher ID',
            'role' => 'Role'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseOfStudents()
    {
        return $this->hasMany(StudentCourse::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessonOfStudents()
    {
        return $this->hasMany(StudentLesson::className(), ['student_id' => 'id']);
    }

    /**
     * Get user roles
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRoles()
    {
        return $this->hasMany(Assignment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(UserStatus::className(), ['id' => 'status_id']);
    }
}
