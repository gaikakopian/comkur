<?php

use yii\db\Migration;

class m170925_065552_lesson_status extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `student_lesson`
ADD `status_id` int(11) NOT NULL DEFAULT '1' COMMENT 'Статус урока' AFTER `type_id`,
ADD FOREIGN KEY (`status_id`) REFERENCES `lesson_status` (`id`),
COMMENT='Уроки студентов';
SQL;
        $this->execute($sql);

    }

    public function safeDown()
    {
        echo "m170925_065552_lesson_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170925_065552_lesson_status cannot be reverted.\n";

        return false;
    }
    */
}
