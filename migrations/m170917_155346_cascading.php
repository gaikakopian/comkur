<?php

use yii\db\Migration;

class m170917_155346_cascading extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
SET foreign_key_checks = 0;        
ALTER TABLE `student_lesson`
DROP FOREIGN KEY `student_lesson_ibfk_1`,
ADD FOREIGN KEY (`lesson_id`) REFERENCES `lesson` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE `student_answer`
DROP FOREIGN KEY `student_answer_ibfk_2`,
ADD FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

ALTER TABLE `student_answer`
DROP FOREIGN KEY `student_answer_ibfk_3`,
ADD FOREIGN KEY (`lesson_id`) REFERENCES `lesson` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;
SQL;

        $this->execute($sql);

    }

    public function safeDown()
    {
        echo "m170917_155346_cascading cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170917_155346_cascading cannot be reverted.\n";

        return false;
    }
    */
}
