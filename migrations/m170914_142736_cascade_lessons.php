<?php

use yii\db\Migration;

class m170914_142736_cascade_lessons extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
SET foreign_key_checks = 0;
ALTER TABLE `lesson`
ADD FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE,
COMMENT='Уроки';
SQL;
        $this->execute($sql);

    }

    public function safeDown()
    {
        echo "m170914_142736_cascade_lessons cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170914_142736_cascade_lessons cannot be reverted.\n";

        return false;
    }
    */
}
