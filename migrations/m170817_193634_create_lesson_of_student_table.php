<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lesson_of_student`.
 * Has foreign keys to the tables:
 *
 * - `lesson_pattern`
 * - `free_lesson`
 * - `user`
 * - `course`
 */
class m170817_193634_create_lesson_of_student_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('lesson_of_student', [
            'id' => $this->primaryKey(),
            'course_lesson_id' => $this->integer(),
            'free_lesson_id' => $this->integer(),
            'type_id' => $this->integer(),
            'student_id' => $this->integer(),
            'course_id' => $this->integer(),
            'status_id' => $this->integer(),
            'priority' => $this->integer(),
            'student_answer' => $this->integer(),
        ]);

        // creates index for column `course_lesson_id`
        $this->createIndex(
            'idx-lesson_of_student-course_lesson_id',
            'lesson_of_student',
            'course_lesson_id'
        );

        // add foreign key for table `lesson_pattern`
        $this->addForeignKey(
            'fk-lesson_of_student-course_lesson_id',
            'lesson_of_student',
            'course_lesson_id',
            'lesson_pattern',
            'id',
            'CASCADE',
            'NO ACTION'
        );

        // creates index for column `free_lesson_id`
        $this->createIndex(
            'idx-lesson_of_student-free_lesson_id',
            'lesson_of_student',
            'free_lesson_id'
        );

        // add foreign key for table `free_lesson`
        $this->addForeignKey(
            'fk-lesson_of_student-free_lesson_id',
            'lesson_of_student',
            'free_lesson_id',
            'free_lesson',
            'id',
            'CASCADE',
            'NO ACTION'
        );

        // creates index for column `student_id`
        $this->createIndex(
            'idx-lesson_of_student-student_id',
            'lesson_of_student',
            'student_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-lesson_of_student-student_id',
            'lesson_of_student',
            'student_id',
            'user',
            'id',
            'CASCADE',
            'NO ACTION'
        );

        // creates index for column `course_id`
        $this->createIndex(
            'idx-lesson_of_student-course_id',
            'lesson_of_student',
            'course_id'
        );

        // add foreign key for table `course`
        $this->addForeignKey(
            'fk-lesson_of_student-course_id',
            'lesson_of_student',
            'course_id',
            'course',
            'id',
            'CASCADE',
            'NO ACTION'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `lesson_pattern`
        $this->dropForeignKey(
            'fk-lesson_of_student-course_lesson_id',
            'lesson_of_student'
        );

        // drops index for column `course_lesson_id`
        $this->dropIndex(
            'idx-lesson_of_student-course_lesson_id',
            'lesson_of_student'
        );

        // drops foreign key for table `free_lesson`
        $this->dropForeignKey(
            'fk-lesson_of_student-free_lesson_id',
            'lesson_of_student'
        );

        // drops index for column `free_lesson_id`
        $this->dropIndex(
            'idx-lesson_of_student-free_lesson_id',
            'lesson_of_student'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-lesson_of_student-student_id',
            'lesson_of_student'
        );

        // drops index for column `student_id`
        $this->dropIndex(
            'idx-lesson_of_student-student_id',
            'lesson_of_student'
        );

        // drops foreign key for table `course`
        $this->dropForeignKey(
            'fk-lesson_of_student-course_id',
            'lesson_of_student'
        );

        // drops index for column `course_id`
        $this->dropIndex(
            'idx-lesson_of_student-course_id',
            'lesson_of_student'
        );

        $this->dropTable('lesson_of_student');
    }
}
