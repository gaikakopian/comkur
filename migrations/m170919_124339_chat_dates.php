<?php

use yii\db\Migration;

class m170919_124339_chat_dates extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `chat`
ADD `author_last_date` datetime NOT NULL COMMENT 'Дата прочтения автором',
ADD `recipient_last_date` datetime NOT NULL COMMENT 'Дата прочтения получателем' AFTER `author_last_date`,
COMMENT='Чаты';
SQL;
        $this->execute($sql);

    }

    public function safeDown()
    {
        echo "m170919_124339_chat_dates cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170919_124339_chat_dates cannot be reverted.\n";

        return false;
    }
    */
}
