<?php

use yii\db\Migration;

/**
 * Class m180118_081242_add_unique_to_username_in_user_table
 */
class m180118_081242_add_unique_to_username_in_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex('username', 'user', 'username', $unique = true );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex('username', 'user');
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180118_081242_add_unique_to_username_in_user_table cannot be reverted.\n";

        return false;
    }
    */
}
