<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170601_135452_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'auth_key' => $this->string(),
            'password_hash' => $this->string(),
            'password_reset_token' => $this->string(),

            'confirmation_code' => $this->string(),

            'name' => $this->string(),
            'birth_date' => $this->date(),
            'phone_number' => $this->string(),
            'skype' => $this->string(),

            'role_id' => $this->integer(),
            'status_id' => $this->integer(),

            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'last_login_at' => $this->dateTime(),
            'last_online_at' => $this->dateTime(),
            'created_by_id' => $this->integer(),
            'updated_by_id' => $this->integer()
        ]);

        $this->insert('user',[
            'username' => 'director@example.com',
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password_hash' => Yii::$app->security->generatePasswordHash('123456'),
            'name' => 'Директор',
            'birth_date' => '1980-01-01',
            'status_id' => \app\models\UserStatus::STATUS_ACTIVE,
            'role_id' => \app\helpers\Constants::RBAC_DIRECTOR,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'last_login_at' => date('Y-m-d H:i:s'),
            'last_online_at' => date('Y-m-d H:i:s'),
            'created_by_id' => 0,
            'updated_by_id' => 0,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
