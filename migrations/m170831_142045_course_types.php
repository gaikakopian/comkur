<?php

use yii\db\Migration;

class m170831_142045_course_types extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
-- Adminer 4.1.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `course_type`;
CREATE TABLE `course_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `name` varchar(255) NOT NULL COMMENT 'Тип курса',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Типы курсов';

INSERT INTO `course_type` (`id`, `name`) VALUES
(1,	'Обычный'),
(2,	'Демо');

-- 2017-08-31 14:21:16
SQL;
        $this->execute($sql);

        $sql = <<<SQL
ALTER TABLE `course`
CHANGE `type_id` `type_id` int(11) NOT NULL DEFAULT '1' COMMENT 'Тип' AFTER `video_url`,
ADD FOREIGN KEY (`type_id`) REFERENCES `course_type` (`id`),
COMMENT=' Курсы';
SQL;

        $this->execute($sql);


    }

    public function safeDown()
    {
        echo "m170831_142045_course_types cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170831_142045_course_types cannot be reverted.\n";

        return false;
    }
    */
}
