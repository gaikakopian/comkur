<?php

use yii\db\Migration;

class m170829_083757_student_courses extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
SET foreign_key_checks = 0;
ALTER TABLE `student_lesson`
ADD `parent_id` int(11) NULL COMMENT 'Программа' AFTER `lesson_id`,
ADD FOREIGN KEY (`parent_id`) REFERENCES `student_course` (`id`),
COMMENT='Уроки студентов';
SQL;

        $this->execute($sql);

    }

    public function safeDown()
    {
        echo "m170829_083757_student_courses cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170829_083757_student_courses cannot be reverted.\n";

        return false;
    }
    */
}
