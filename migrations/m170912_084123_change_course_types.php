<?php

use yii\db\Migration;

class m170912_084123_change_course_types extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
UPDATE `course_type` SET
`name` = 'Тест'
WHERE `id` = '2';
SQL;
        $this->execute($sql);

    }

    public function safeDown()
    {
        echo "m170912_084123_change_course_types cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170912_084123_change_course_types cannot be reverted.\n";

        return false;
    }
    */
}
