<?php

use yii\db\Migration;

/**
 * Class m180106_155714_add_access_stage_field_to_student_course_table
 */
class m180106_155714_add_access_stage_field_to_student_course_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('student_course', 'access_stage', $this->integer());
        $this->addColumn('student_course', 'good_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('student_course', 'access_stage');
        $this->dropColumn('student_course', 'good_id');
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180106_155714_add_access_stage_field_to_student_course_table cannot be reverted.\n";

        return false;
    }
    */
}
