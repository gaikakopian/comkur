<?php

use yii\db\Migration;

class m170829_060732_extract_relations extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE `student_teacher` (
  `id` int NOT NULL COMMENT 'ИД' AUTO_INCREMENT PRIMARY KEY,
  `teacher_id` int(11) NULL COMMENT 'Преподаватель',
  `student_id` int(11) NOT NULL COMMENT 'Студент',
  `course_id` int(11) NULL COMMENT 'Курс',
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Флаг активности',
  `create_date` datetime NOT NULL COMMENT 'Дата создания',
  `update_date` datetime NOT NULL COMMENT 'Дата изменения',
  FOREIGN KEY (`teacher_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  FOREIGN KEY (`student_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE SET NULL
) COMMENT='Связь студент-учитель';


SQL;

        $this->execute($sql);
    }

    public function safeDown()
    {
        echo "m170829_060732_extract_relations cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170829_060732_extract_relations cannot be reverted.\n";

        return false;
    }
    */
}
