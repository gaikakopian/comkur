<?php

use yii\db\Migration;

class m170921_183239_new_notices extends Migration
{
    public function safeUp()
    {
        $this->truncateTable(\app\models\Template::tableName());

        $sql = <<<SQL
INSERT INTO `template` (`id`, `code`, `title`, `content`, `create_date`, `update_date`) VALUES
(1,	'user-register',	'Регистрация пользователя',	'<p>Уважаемый(ая) {name}!</p><p>Спасибо за регистрацию, ваша учетная запись активирована.</p><p>Для входа используйте пароль {password}.</p><p>{link}</p>',	'2017-08-30 17:37:43',	'2017-08-31 12:03:41'),
(2,	'new-student',	'УРА! Новый ученик в нашей школе!',	'<p>В нашей школе появился новый ученик! Сейчас у тебя есть шанс сделать его обучение вокалу незабываемым и помочь осуществить мечту! Для этого зайди в личный кабинет и нажми кнопку Стать Преподавателем<i></i></p><p>{link}</p>',	'2017-08-31 13:53:02',	'2017-09-22 00:19:36'),
(3,	'new-answer',	'Получен новый отчет от {name}',	'<p>Вы получили новый отчет от <i>{name}</i> для просмотра пройдите в личный кабинет <i>{link}</i></p>',	'2017-09-22 00:17:24',	'2017-09-22 00:17:24'),
(4,	'new-review',	'Отчет по домашнему заданию получен',	'<p>Вы получили отчет педагога по домашнему заданию. </p><p>Для просмотра пройдите в личный кабинет по ссылке ниже:</p><p>{link}</p>',	'2017-09-22 00:18:18',	'2017-09-22 00:18:18'),
(5,	'new-director',	'Ура! Новый ученик в нашей школе!',	'<p>Молодец! В твоей школе появился еще один новый ученик! Сделай все, чтоб он остался очень доволен результатом!<strong> </strong></p><p>{link}</p>',	'2017-09-22 00:20:50',	'2017-09-22 00:20:50');
SQL;
        $this->execute($sql);

    }

    public function safeDown()
    {
        echo "m170921_183239_new_notices cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170921_183239_new_notices cannot be reverted.\n";

        return false;
    }
    */
}
