<?php

use yii\db\Migration;

class m170830_102550_lesson_fk extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `student_lesson`
DROP FOREIGN KEY `student_lesson_ibfk_4`,
ADD FOREIGN KEY (`parent_id`) REFERENCES `student_course` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
SQL;
        $this->execute($sql);

    }

    public function safeDown()
    {
        echo "m170830_102550_lesson_fk cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170830_102550_lesson_fk cannot be reverted.\n";

        return false;
    }
    */
}
