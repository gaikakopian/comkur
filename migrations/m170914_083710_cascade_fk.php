<?php

use yii\db\Migration;

class m170914_083710_cascade_fk extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
SET foreign_key_checks = 0;        
ALTER TABLE `student_course`
ADD FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
ADD FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE,
COMMENT='Курсы студентов';
SQL;
        $this->execute($sql);

    }

    public function safeDown()
    {
        echo "m170914_083710_cascade_fk cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170914_083710_cascade_fk cannot be reverted.\n";

        return false;
    }
    */
}
