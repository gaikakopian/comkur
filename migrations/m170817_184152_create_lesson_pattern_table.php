<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lesson_pattern`.
 * Has foreign keys to the tables:
 *
 * - `course`
 */
class m170817_184152_create_lesson_pattern_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('lesson_pattern', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'image_url' => $this->string(),
            'video_url' => $this->string(),
            'description' => $this->text(),
            'course_id' => $this->integer(),
            'course_part' => $this->integer(),
            'priority' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by_id' => $this->integer(),
            'updated_by_id' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('lesson_pattern');
    }
}
