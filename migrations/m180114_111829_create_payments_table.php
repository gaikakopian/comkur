<?php

use yii\db\Migration;

/**
 * Handles the creation of table `payments`.
 */
class m180114_111829_create_payments_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('payments', [
            'id' => $this->primaryKey(),
            'order_id' => $this->string(),
            'user_id' => $this->integer()->notNull(),
            'good_id' => $this->integer()->notNull(),
            'price' => $this->integer(40),
            'status' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('payments');
        return true;
    }
}
