<?php

use yii\db\Migration;

class m170829_132718_lesson_statuses extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
        
-- Adminer 4.1.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `lesson_type`;
CREATE TABLE `lesson_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `name` varchar(255) NOT NULL COMMENT 'Наименование',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Типы уроков';

INSERT INTO `lesson_type` (`id`, `name`) VALUES
(1,	'Оффлайн урок'),
(2,	'Онлайн урок');

DROP TABLE IF EXISTS `student_lesson`;
CREATE TABLE `student_lesson` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `course_id` int(11) DEFAULT NULL COMMENT 'Курс',
  `lesson_id` int(11) DEFAULT NULL COMMENT 'Урок',
  `parent_id` int(11) DEFAULT NULL COMMENT 'Программа',
  `user_id` int(11) NOT NULL COMMENT 'Студент',
  `priority` int(11) DEFAULT NULL COMMENT 'Приоритет',
  `type_id` int(11) NOT NULL DEFAULT '1' COMMENT 'Тип занятия',
  `start_date` datetime DEFAULT NULL COMMENT 'Дата начала',
  `end_date` datetime DEFAULT NULL COMMENT 'Дата окончания',
  `expire_date` datetime DEFAULT NULL COMMENT 'Срок истечения',
  `create_date` datetime NOT NULL COMMENT 'Дата создания',
  `update_date` datetime NOT NULL COMMENT 'Дата изменения',
  PRIMARY KEY (`id`),
  KEY `idx-lesson_of_student-course_lesson_id` (`lesson_id`),
  KEY `idx-lesson_of_student-student_id` (`user_id`),
  KEY `idx-lesson_of_student-course_id` (`course_id`),
  KEY `type_id` (`type_id`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `fk-lesson_of_student-course_id` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `student_lesson_ibfk_1` FOREIGN KEY (`lesson_id`) REFERENCES `lesson` (`id`),
  CONSTRAINT `student_lesson_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `student_lesson_ibfk_3` FOREIGN KEY (`type_id`) REFERENCES `lesson_type` (`id`),
  CONSTRAINT `student_lesson_ibfk_4` FOREIGN KEY (`parent_id`) REFERENCES `student_course` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Уроки студентов';

-- 2017-08-29 14:37:33
        
SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `lesson_status`;
CREATE TABLE `lesson_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `name` varchar(255) NOT NULL COMMENT 'Статус',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Статусы уроков';

INSERT INTO `lesson_status` (`id`, `name`) VALUES
(1,	'Запланирован'),
(2,	'Назначен'),
(3,	'Изучение'),
(4,	'Отправлен'),
(5,	'Пройден');
SQL;
        $this->execute($sql);

        $sql = <<<SQL
SET foreign_key_checks = 0;        
ALTER TABLE `student_lesson`
ADD `status_id` int(11) NOT NULL DEFAULT '1' COMMENT 'Статус' AFTER `type_id`,
ADD FOREIGN KEY (`status_id`) REFERENCES `lesson_status` (`id`),
COMMENT='Уроки студентов';
SQL;
        $this->execute($sql);

    }

    public function safeDown()
    {
        echo "m170829_132718_lesson_statuses cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170829_132718_lesson_statuses cannot be reverted.\n";

        return false;
    }
    */
}
