<?php

use yii\db\Migration;

/**
 * Handles adding theme_id to table `free_lesson`.
 * Has foreign keys to the tables:
 *
 * - `lessons_theme`
 */
class m170825_124813_add_theme_id_column_to_free_lesson_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('free_lesson', 'theme_id', $this->integer());

        // creates index for column `theme_id`
        $this->createIndex(
            'idx-free_lesson-theme_id',
            'free_lesson',
            'theme_id'
        );

        // add foreign key for table `lessons_theme`
        $this->addForeignKey(
            'fk-free_lesson-theme_id',
            'free_lesson',
            'theme_id',
            'lessons_theme',
            'id',
            'CASCADE',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `lessons_theme`
        $this->dropForeignKey(
            'fk-free_lesson-theme_id',
            'free_lesson'
        );

        // drops index for column `theme_id`
        $this->dropIndex(
            'idx-free_lesson-theme_id',
            'free_lesson'
        );

        $this->dropColumn('free_lesson', 'theme_id');
    }
}
