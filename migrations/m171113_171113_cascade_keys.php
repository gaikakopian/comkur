<?php

use yii\db\Migration;

class m171113_171113_cascade_keys extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `message`
DROP FOREIGN KEY `message_ibfk_3`,
ADD FOREIGN KEY (`recipient_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;
SQL;
        $this->execute($sql);

        $sql = <<<SQL
ALTER TABLE `student_lesson`
DROP FOREIGN KEY `student_lesson_ibfk_2`,
ADD FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;
SQL;
        $this->execute($sql);
    }

    public function safeDown()
    {
        echo "m171113_171113_cascade_keys cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171113_171113_cascade_keys cannot be reverted.\n";

        return false;
    }
    */
}
