<?php

use yii\db\Migration;

/**
 * Class m180104_075629_add_active_field_to_courses
 */
class m180104_075629_add_active_field_to_courses extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('course', 'active', $this->integer()->defaultValue('1'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('course', 'active');
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180104_075629_add_active_field_to_courses cannot be reverted.\n";

        return false;
    }
    */
}
