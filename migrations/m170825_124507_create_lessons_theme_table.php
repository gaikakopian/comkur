<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lessons_theme`.
 */
class m170825_124507_create_lessons_theme_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('lessons_theme', [
            'id' => $this->primaryKey(),
            'theme_name' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('lessons_theme');
    }
}
