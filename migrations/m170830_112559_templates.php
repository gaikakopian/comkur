<?php

use yii\db\Migration;

class m170830_112559_templates extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE `template` (
  `id` int NOT NULL COMMENT 'ИД' AUTO_INCREMENT PRIMARY KEY,
  `code` varchar(255) NOT NULL COMMENT 'Код',
  `title` varchar(255) NOT NULL COMMENT 'Заголовок',
  `content` text NOT NULL COMMENT 'Сообщение',
  `create_date` datetime NOT NULL COMMENT 'Дата создания',
  `update_date` datetime NOT NULL COMMENT 'Дата изменения'
) COMMENT='Шаблоны уведомлений';
SQL;
        $this->execute($sql);

    }

    public function safeDown()
    {
        echo "m170830_112559_templates cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170830_112559_templates cannot be reverted.\n";

        return false;
    }
    */
}
