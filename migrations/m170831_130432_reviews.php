<?php

use yii\db\Migration;

class m170831_130432_reviews extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
-- Adminer 4.1.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `teacher_review`;
CREATE TABLE `teacher_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `user_id` int(11) NOT NULL COMMENT 'Автор',
  `lesson_id` int(11) NOT NULL COMMENT 'Занятие',
  `content` text NOT NULL COMMENT 'Отчет',
  `create_date` datetime NOT NULL COMMENT 'Дата создания',
  `update_date` datetime NOT NULL COMMENT 'Дата изменения',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `lesson_id` (`lesson_id`),
  CONSTRAINT `teacher_review_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `teacher_review_ibfk_2` FOREIGN KEY (`lesson_id`) REFERENCES `student_lesson` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Отчеты по урокам';


-- 2017-08-31 13:04:20
SQL;
        $this->execute($sql);

    }

    public function safeDown()
    {
        echo "m170831_130432_reviews cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170831_130432_reviews cannot be reverted.\n";

        return false;
    }
    */
}
