<?php

use yii\db\Migration;

class m170831_093927_chats extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
-- Adminer 4.1.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `chat`;
CREATE TABLE `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `author_id` int(11) NOT NULL COMMENT 'Автор',
  `recipient_id` int(11) DEFAULT NULL COMMENT 'Получатель',
  `title` varchar(255) DEFAULT NULL COMMENT 'Заголовок',
  `create_date` datetime NOT NULL COMMENT 'Дата создания',
  `update_date` datetime NOT NULL COMMENT 'Дата изменения',
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`),
  KEY `recipient_id` (`recipient_id`),
  CONSTRAINT `chat_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `chat_ibfk_2` FOREIGN KEY (`recipient_id`) REFERENCES `user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Чаты';


DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `chat_id` int(11) NOT NULL COMMENT 'Чат',
  `author_id` int(11) NOT NULL COMMENT 'Автор',
  `recipient_id` int(11) DEFAULT NULL COMMENT 'Получатель',
  `content` text NOT NULL COMMENT 'Сообщение',
  `is_read` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Отметка о прочтении',
  `create_date` datetime NOT NULL COMMENT 'Дата создания',
  `update_date` datetime NOT NULL COMMENT 'Дата изменения',
  PRIMARY KEY (`id`),
  KEY `chat_id` (`chat_id`),
  KEY `author_id` (`author_id`),
  KEY `recepient_id` (`recipient_id`),
  CONSTRAINT `message_ibfk_1` FOREIGN KEY (`chat_id`) REFERENCES `chat` (`id`) ON DELETE CASCADE,
  CONSTRAINT `message_ibfk_2` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `message_ibfk_3` FOREIGN KEY (`recipient_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Сообщения';


-- 2017-08-31 09:40:26
SQL;
        $this->execute($sql);

    }

    public function safeDown()
    {
        echo "m170831_093927_chats cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170831_093927_chats cannot be reverted.\n";

        return false;
    }
    */
}
