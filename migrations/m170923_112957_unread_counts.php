<?php

use yii\db\Migration;

class m170923_112957_unread_counts extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `chat`
ADD `author_unread_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Не прочитано автором',
ADD `recipient_unread_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Не прочитано получателем' AFTER `author_unread_count`,
COMMENT='Чаты';
SQL;
        $this->execute($sql);

    }

    public function safeDown()
    {
        echo "m170923_112957_unread_counts cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170923_112957_unread_counts cannot be reverted.\n";

        return false;
    }
    */
}
