<?php

use yii\db\Migration;

/**
 * Class m180103_123654_add_stage_field_to_lesson_table
 */
class m180103_123654_add_stage_field_to_lesson_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('lesson', 'course_stage', $this->integer()->defaultValue('0'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('lesson', 'course_stage');
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180103_123654_add_stage_field_to_lesson_table cannot be reverted.\n";

        return false;
    }
    */
}
