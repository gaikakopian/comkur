<?php

use yii\db\Migration;

class m170925_090624_external_video_url extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
ALTER TABLE `lesson`
CHANGE `video_url` `video_url` text COLLATE 'utf8_general_ci' NULL COMMENT 'Видео' AFTER `image_url`,
COMMENT='Уроки';
SQL;
        $this->execute($sql);

        $sql = <<<SQL
ALTER TABLE `student_answer`
ADD `video_url` text COLLATE 'utf8_general_ci' NULL COMMENT 'Ссылка на видео' AFTER `content`,
COMMENT='Ответы на уроки';
SQL;
        $this->execute($sql);


    }

    public function safeDown()
    {
        echo "m170925_090624_external_video_url cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170925_090624_external_video_url cannot be reverted.\n";

        return false;
    }
    */
}
