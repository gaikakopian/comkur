<?php

use yii\db\Migration;

class m171122_092008_demo_courses_statuses extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
UPDATE student_lesson sl
LEFT JOIN course c ON c.id = sl.course_id
SET sl.status_id = 2
WHERE sl.status_id = 1 AND c.type_id = 2
SQL;
        $this->execute($sql);

    }

    public function safeDown()
    {
        echo "m171122_092008_demo_courses_statuses cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171122_092008_demo_courses_statuses cannot be reverted.\n";

        return false;
    }
    */
}
