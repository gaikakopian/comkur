<?php

use yii\db\Migration;

/**
 * Handles the creation of table `course_of_student`.
 * Has foreign keys to the tables:
 *
 * - `course`
 * - `user`
 */
class m170817_182736_create_course_of_student_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('course_of_student', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer(),
            'user_id' => $this->integer(),
            'access_for_part' => $this->integer(),
        ]);

        // creates index for column `course_id`
        $this->createIndex(
            'idx-course_of_student-course_id',
            'course_of_student',
            'course_id'
        );

        // add foreign key for table `course`
        $this->addForeignKey(
            'fk-course_of_student-course_id',
            'course_of_student',
            'course_id',
            'course',
            'id',
            'CASCADE',
            'NO ACTION'
        );

        // creates index for column `user_id`
        $this->createIndex(
            'idx-course_of_student-user_id',
            'course_of_student',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-course_of_student-user_id',
            'course_of_student',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'NO ACTION'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `course`
        $this->dropForeignKey(
            'fk-course_of_student-course_id',
            'course_of_student'
        );

        // drops index for column `course_id`
        $this->dropIndex(
            'idx-course_of_student-course_id',
            'course_of_student'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-course_of_student-user_id',
            'course_of_student'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-course_of_student-user_id',
            'course_of_student'
        );

        $this->dropTable('course_of_student');
    }
}
