<?php

use yii\db\Migration;

class m170831_075440_new_student_notice extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
INSERT INTO `template` (`code`, `title`, `content`, `create_date`, `update_date`) VALUES
('new-student',	'Регистрация студента',	'<p>Уважаемый(ая) {name}!</p><p>Зарегистрирован новый студент {student} с курсом {course}.</p><p>Для того, чтобы стать преподавателем - перейдите по ссылке:</p><p>{link}</p>',	'2017-08-31 13:53:02',	'2017-08-31 13:53:02');
SQL;
        $this->execute($sql);

    }

    public function safeDown()
    {
        echo "m170831_075440_new_student_notice cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170831_075440_new_student_notice cannot be reverted.\n";

        return false;
    }
    */
}
