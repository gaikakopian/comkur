<?php

use yii\db\Migration;

/**
 * Handles the creation of table `goods`.
 */
class m180104_094139_create_goods_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('goods', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer()->notNull(),
            'title' => $this->string(),
            'photo' => $this->string(),
            'short_descr' => $this->text(),
            'type' => $this->string(),
            'stage' => $this->integer(),
            'part' => $this->integer(),
            'price' => $this->integer(40),
            'active' => $this->integer()->defaultValue('1'),
            'priority' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('goods');
        return true;
    }
}
