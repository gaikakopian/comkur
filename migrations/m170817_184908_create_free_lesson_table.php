<?php

use yii\db\Migration;

/**
 * Handles the creation of table `free_lesson`.
 */
class m170817_184908_create_free_lesson_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('free_lesson', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'image_url' => $this->string(),
            'video_url' => $this->string(),
            'description' => $this->text(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by_id' => $this->integer(),
            'updated_by_id' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('free_lesson');
    }
}
