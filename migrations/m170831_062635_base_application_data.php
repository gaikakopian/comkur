<?php

use yii\db\Migration;

class m170831_062635_base_application_data extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
INSERT INTO `lesson_theme` (`name`, `description`, `create_date`, `update_date`) VALUES
('Без категории',	NULL,	'2017-08-29 14:42:39',	'2017-08-29 14:42:39');
SQL;

        $this->execute($sql);

        $sql = <<<SQL
INSERT INTO `template` (`code`, `title`, `content`, `create_date`, `update_date`) VALUES
('user-register',	'Регистрация пользователя',	'<p>Уважаемый(ая) {name}!</p><p>Спасибо за регистрацию, ваша учетная запись активирована.</p><p>Для входа используйте пароль {password}.</p><p>{link}</p>',	'2017-08-30 17:37:43',	'2017-08-31 12:03:41');
SQL;
        $this->execute($sql);

    }

    public function safeDown()
    {
        echo "m170831_062635_base_application_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170831_062635_base_application_data cannot be reverted.\n";

        return false;
    }
    */
}
