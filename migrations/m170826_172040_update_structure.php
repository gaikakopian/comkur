<?php

use yii\db\Migration;

class m170826_172040_update_structure extends Migration
{
    public function safeUp()
    {
        $sql = <<<SQL
SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

ALTER TABLE `course`
CHANGE `id` `id` int(11) NOT NULL COMMENT 'ИД' AUTO_INCREMENT FIRST,
CHANGE `name` `name` varchar(255) COLLATE 'utf8_general_ci' NOT NULL COMMENT 'Название' AFTER `id`,
CHANGE `description` `description` text COLLATE 'utf8_general_ci' NULL COMMENT 'Описание' AFTER `name`,
CHANGE `image_url` `image_url` varchar(255) COLLATE 'utf8_general_ci' NULL COMMENT 'Изображение' AFTER `description`,
CHANGE `video_url` `video_url` varchar(255) COLLATE 'utf8_general_ci' NULL COMMENT 'Видео' AFTER `image_url`,
CHANGE `type_id` `type_id` int(11) NULL COMMENT 'Тип' AFTER `video_url`,
CHANGE `created_at` `create_date` datetime NOT NULL COMMENT 'Дата создания' AFTER `type_id`,
CHANGE `updated_at` `update_date` datetime NOT NULL COMMENT 'Дата изменения' AFTER `create_date`,
DROP `created_by_id`,
DROP `updated_by_id`,
COMMENT=' Курсы';
        
ALTER TABLE `course`
ADD `code` varchar(255) COLLATE 'utf8_general_ci' NOT NULL COMMENT 'Код' AFTER `name`,
COMMENT=' Курсы';
        
ALTER TABLE `course`
CHANGE `code` `code` varchar(255) COLLATE 'utf8_general_ci' NOT NULL COMMENT 'Код' AFTER `id`,
CHANGE `name` `name` varchar(255) COLLATE 'utf8_general_ci' NOT NULL COMMENT 'Название' AFTER `code`,
COMMENT=' Курсы';

ALTER TABLE `lesson_pattern`
CHANGE `id` `id` int(11) NOT NULL COMMENT 'ИД' AUTO_INCREMENT FIRST,
ADD `code` varchar(255) COLLATE 'utf8_general_ci' NOT NULL COMMENT 'Код' AFTER `id`,
CHANGE `name` `name` varchar(255) COLLATE 'utf8_general_ci' NOT NULL COMMENT 'Наименование' AFTER `code`,
CHANGE `image_url` `image_url` varchar(255) COLLATE 'utf8_general_ci' NULL COMMENT 'Изображение' AFTER `name`,
CHANGE `video_url` `video_url` varchar(255) COLLATE 'utf8_general_ci' NULL COMMENT 'Видео' AFTER `image_url`,
CHANGE `description` `description` text COLLATE 'utf8_general_ci' NOT NULL COMMENT 'Описание' AFTER `video_url`,
CHANGE `course_id` `course_id` int(11) NULL COMMENT 'Курс' AFTER `description`,
CHANGE `course_part` `course_part` int(11) NULL COMMENT 'Часть' AFTER `course_id`,
CHANGE `priority` `priority` int(11) NOT NULL COMMENT 'Приоритет' AFTER `course_part`,
CHANGE `created_at` `create_date` datetime NOT NULL COMMENT 'Дата создания' AFTER `priority`,
CHANGE `updated_at` `update_date` datetime NOT NULL COMMENT 'Дата изменения' AFTER `create_date`,
DROP `created_by_id`,
DROP `updated_by_id`,
RENAME TO `lesson`,
COMMENT='Уроки';

ALTER TABLE `course_of_student`
CHANGE `id` `id` int(11) NOT NULL COMMENT 'ИД' AUTO_INCREMENT FIRST,
CHANGE `user_id` `user_id` int(11) NULL COMMENT 'Студент' AFTER `id`,
CHANGE `course_id` `course_id` int(11) NULL COMMENT 'Курс' AFTER `user_id`,
CHANGE `access_for_part` `access_for_part` int(11) NULL COMMENT 'Оплата' AFTER `course_id`,
ADD `start_date` datetime NULL COMMENT 'Дата начала',
ADD `end_date` datetime NULL COMMENT 'Дата завершения' AFTER `start_date`,
ADD `create_date` datetime NULL COMMENT 'Дата создания' AFTER `end_date`,
ADD `update_date` datetime NULL COMMENT 'Дата изменения' AFTER `create_date`,
RENAME TO `course_student`,
COMMENT='Курсы студентов';

ALTER TABLE `course_student`
CHANGE `start_date` `start_date` datetime NOT NULL COMMENT 'Дата начала' AFTER `access_for_part`,
CHANGE `end_date` `end_date` datetime NOT NULL COMMENT 'Дата завершения' AFTER `start_date`,
CHANGE `create_date` `create_date` datetime NOT NULL COMMENT 'Дата создания' AFTER `end_date`,
CHANGE `update_date` `update_date` datetime NOT NULL COMMENT 'Дата изменения' AFTER `create_date`,
COMMENT='Курсы студентов';

ALTER TABLE `course_student`
CHANGE `end_date` `end_date` datetime NULL COMMENT 'Дата завершения' AFTER `start_date`,
COMMENT='Курсы студентов';

DROP TABLE IF EXISTS `submission`;
CREATE TABLE `submission` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `user_id` int(11) NOT NULL COMMENT 'Студент',
  `course_id` int(11) NOT NULL COMMENT 'Курс',
  `lesson_id` int(11) NOT NULL COMMENT 'Урок',
  `content` text COMMENT 'Ответ',
  `create_date` datetime NOT NULL COMMENT 'Дата создания',
  `update_date` datetime NOT NULL COMMENT 'Дата изменения',
  `expire_date` datetime DEFAULT NULL COMMENT 'Срок истечения',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `course_id` (`course_id`),
  KEY `lesson_id` (`lesson_id`),
  CONSTRAINT `submission_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `submission_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  CONSTRAINT `submission_ibfk_3` FOREIGN KEY (`lesson_id`) REFERENCES `lesson` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Ответы на уроки';

ALTER TABLE `lessons_theme`
CHANGE `id` `id` int(11) NOT NULL COMMENT 'ИД' AUTO_INCREMENT FIRST,
CHANGE `theme_name` `name` varchar(255) COLLATE 'utf8_general_ci' NOT NULL COMMENT 'Название' AFTER `id`,
ADD `description` text COLLATE 'utf8_general_ci' NULL COMMENT 'Описание',
ADD `create_date` datetime NOT NULL COMMENT 'Дата создания' AFTER `description`,
ADD `update_date` datetime NOT NULL COMMENT 'Дата изменения' AFTER `create_date`,
RENAME TO `lesson_theme`,
COMMENT='Темы уроков';

ALTER TABLE `lesson`
ADD `theme_id` int(11) NULL COMMENT 'Тема' AFTER `course_id`,
ADD FOREIGN KEY (`theme_id`) REFERENCES `lesson_theme` (`id`) ON DELETE SET NULL,
COMMENT='Уроки';

ALTER TABLE `lesson_of_student`
DROP FOREIGN KEY `fk-lesson_of_student-student_id`;

ALTER TABLE `lesson_of_student`
DROP FOREIGN KEY `fk-lesson_of_student-free_lesson_id`;

ALTER TABLE `lesson_of_student`
DROP FOREIGN KEY `fk-lesson_of_student-course_lesson_id`;

ALTER TABLE `lesson_of_student`
CHANGE `id` `id` int(11) NOT NULL COMMENT 'ИД' AUTO_INCREMENT FIRST,
CHANGE `course_id` `course_id` int(11) NULL COMMENT 'Курс' AFTER `id`,
CHANGE `course_lesson_id` `lesson_id` int(11) NOT NULL COMMENT 'Урок' AFTER `course_id`,
DROP `free_lesson_id`,
DROP `type_id`,
CHANGE `student_id` `user_id` int(11) NOT NULL COMMENT 'Студент' AFTER `lesson_id`,
DROP `status_id`,
CHANGE `priority` `priority` int(11) NULL COMMENT 'Приоритет' AFTER `user_id`,
ADD `create_date` datetime NOT NULL COMMENT 'Дата создания' AFTER `priority`,
ADD `update_date` datetime NOT NULL COMMENT 'Дата изменения' AFTER `create_date`,
DROP `student_answer`,
ADD FOREIGN KEY (`lesson_id`) REFERENCES `lesson` (`id`),
ADD FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
COMMENT='Уроки студентов';

ALTER TABLE `lesson_of_student`
RENAME TO `student_lesson`,
COMMENT='Уроки студентов';

ALTER TABLE `submission`
CHANGE `course_id` `course_id` int(11) NULL COMMENT 'Курс' AFTER `id`,
CHANGE `lesson_id` `lesson_id` int(11) NOT NULL COMMENT 'Урок' AFTER `course_id`,
CHANGE `user_id` `user_id` int(11) NOT NULL COMMENT 'Студент' AFTER `lesson_id`,
RENAME TO `student_answer`,
COMMENT='Ответы на уроки';

ALTER TABLE `student_lesson`
ADD `expire_date` datetime NULL COMMENT 'Срок истечения',
COMMENT='Уроки студентов';

ALTER TABLE `course_student`
RENAME TO `student_course`,
COMMENT='Курсы студентов';

ALTER TABLE `student_answer`
DROP FOREIGN KEY `student_answer_ibfk_1`,
ADD FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

CREATE TABLE `lesson_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `name` varchar(255) NOT NULL COMMENT 'Наименование',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Типы уроков';

INSERT INTO `lesson_type` (`id`, `name`) VALUES
(1,	'Оффлайн урок'),
(2,	'Онлайн урок');

ALTER TABLE `student_lesson`
CHANGE `lesson_id` `lesson_id` int(11) NULL COMMENT 'Урок' AFTER `course_id`,
ADD `type_id` int(11) NOT NULL DEFAULT '1' COMMENT 'Тип занятия' AFTER `priority`,
ADD `start_date` datetime NULL COMMENT 'Дата начала' AFTER `type_id`,
ADD `end_date` datetime NULL COMMENT 'Дата окончания' AFTER `start_date`,
CHANGE `expire_date` `expire_date` datetime NULL COMMENT 'Срок истечения' AFTER `end_date`,
CHANGE `create_date` `create_date` datetime NOT NULL COMMENT 'Дата создания' AFTER `expire_date`,
CHANGE `update_date` `update_date` datetime NOT NULL COMMENT 'Дата изменения' AFTER `create_date`,
ADD FOREIGN KEY (`type_id`) REFERENCES `lesson_type` (`id`),
COMMENT='Уроки студентов';

DROP TABLE `free_lesson`;
-- 2017-08-26 18:19:39;
SQL;

        $this->execute($sql);

        $sql = <<<SQL
SET foreign_key_checks = 0;        
CREATE TABLE `user_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `name` varchar(255) NOT NULL COMMENT 'Наименование',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Статусы пользователей';

INSERT INTO `user_status` (`id`, `name`) VALUES
(1,	'Новый'),
(2,	'Активирован'),
(3,	'Заблокирован');
SQL;

        $this->execute($sql);


        $sql = <<<SQL
SET foreign_key_checks = 0;
ALTER TABLE `user`
CHANGE `id` `id` int(11) NOT NULL COMMENT 'ИД' AUTO_INCREMENT FIRST,
CHANGE `username` `username` varchar(255) COLLATE 'utf8_general_ci' NOT NULL COMMENT 'Email' AFTER `id`,
CHANGE `auth_key` `auth_key` varchar(255) COLLATE 'utf8_general_ci' NULL COMMENT 'Ключ авторизации' AFTER `username`,
CHANGE `password_hash` `password_hash` varchar(255) COLLATE 'utf8_general_ci' NULL COMMENT 'Хэш пароля' AFTER `auth_key`,
CHANGE `password_reset_token` `password_reset_token` varchar(255) COLLATE 'utf8_general_ci' NULL COMMENT 'Токен сброса пароля' AFTER `password_hash`,
CHANGE `confirmation_code` `confirmation_code` varchar(255) COLLATE 'utf8_general_ci' NULL COMMENT 'Код подтверждения' AFTER `password_reset_token`,
CHANGE `name` `name` varchar(255) COLLATE 'utf8_general_ci' NULL COMMENT 'Имя' AFTER `confirmation_code`,
CHANGE `birth_date` `birth_date` date NULL COMMENT 'Дата рождения' AFTER `name`,
CHANGE `phone_number` `phone_number` varchar(255) COLLATE 'utf8_general_ci' NULL COMMENT 'Телефон' AFTER `birth_date`,
CHANGE `skype` `skype` varchar(255) COLLATE 'utf8_general_ci' NULL COMMENT 'Скайп' AFTER `phone_number`,
CHANGE `role_id` `role_id` int(11) NULL COMMENT 'Роль' AFTER `skype`,
CHANGE `status_id` `status_id` int(11) NOT NULL COMMENT 'Статус' AFTER `role_id`,
CHANGE `created_at` `created_at` datetime NULL COMMENT 'Дата создания' AFTER `status_id`,
CHANGE `updated_at` `updated_at` datetime NULL COMMENT 'Дата изменения' AFTER `created_at`,
CHANGE `last_login_at` `last_login_at` datetime NULL COMMENT 'Дата последнего входа' AFTER `updated_at`,
CHANGE `last_online_at` `last_online_at` datetime NULL COMMENT 'Дата последнего действия' AFTER `last_login_at`,
CHANGE `created_by_id` `created_by_id` int(11) NULL COMMENT 'Создано (автор)' AFTER `last_online_at`,
CHANGE `updated_by_id` `updated_by_id` int(11) NULL COMMENT 'Отредактировано (автор)' AFTER `created_by_id`,
CHANGE `teacher_id` `teacher_id` int(11) NULL COMMENT 'Учитель' AFTER `updated_by_id`,
ADD FOREIGN KEY (`status_id`) REFERENCES `user_status` (`id`),
COMMENT='Пользователи';
SQL;

        $this->execute($sql);
    }

    public function safeDown()
    {
        echo "m170826_172040_update_structure cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170826_172040_update_structure cannot be reverted.\n";

        return false;
    }
    */
}
