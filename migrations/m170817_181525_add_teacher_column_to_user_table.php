<?php

use yii\db\Migration;

/**
 * Handles adding teacher to table `user`.
 */
class m170817_181525_add_teacher_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'teacher_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'teacher_id');
    }
}
