<?php

namespace app\modules\teacher\controllers;

use app\helpers\Access;
use app\helpers\Constants;
use app\helpers\Help;
use app\helpers\Mailer;
use app\models\Course;
use app\models\Lesson;
use app\models\LessonStatus;
use app\models\LessonTheme;
use app\models\LessonType;
use app\models\StudentAnswer;
use app\models\StudentCourse;
use app\models\StudentLesson;
use app\models\StudentLessonSearch;
use app\models\StudentTeacher;
use app\models\TeacherNotice;
use app\models\TeacherReview;
use app\models\UserSearch;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\db\Query;
use app\models\User;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\rbac\Role;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Котроллер отвечающий за управление пользователями
 *
 * @copyright 	2017 Alex Nem
 * @link 		https://github.com/darkoffalex
 * @author 		Alex Nem
 *
 * @package app\modules\account\controllers
 */
class StudentsController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [Constants::RBAC_TEACHER],
                    ],
                ],
            ]
        ];
    }

    /**
     * Show students lits
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();

        $type = Yii::$app->request->get('type', 'all');

        if($type == 'new')
            $searchModel->teacher_id = -1;
        else
            $searchModel->teacher_id = Yii::$app->user->identity->getId();

        $searchModel->role = Constants::RBAC_STUDENT;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel','dataProvider'));
    }

    /**
     * Show student
     *
     * @param $id
     *
     * @return string
     */
    public function actionShow($id)
    {
        $model = User::findOne($id);

        $notices = TeacherNotice::findAll(['student_id' => $model->id, 'lesson_id' => null]);

        return $this->render('show', compact('model', 'notices'));
    }

    /**
     * Show student course
     *
     * @param int $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCourse($id)
    {
        $parent = StudentCourse::findOne($id);

        if(!$parent)
            throw new NotFoundHttpException('Курс не найден', 404);

        $searchModel = new StudentLessonSearch();

        $searchModel->parent_id = $id;
        $searchModel->user_id = $parent->user_id;
        $searchModel->type_id = LessonType::TYPE_OFFLINE;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, null);

        $course = $parent->course;

        return $this->render('course', compact('course', 'searchModel', 'dataProvider', 'parent'));
    }

    /**
     * Show student's lesson
     *
     * @param int $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionLesson($id)
    {
        $parent = StudentLesson::findOne($id);

        if(!$parent)
            throw new NotFoundHttpException('Урок не найден', 404);

        $model = Lesson::findOne($parent->lesson_id);

        $answer = StudentAnswer::findOne(['lesson_id' => $model->id, 'user_id' => $parent->user_id]);
        $review = TeacherReview::findOne(['lesson_id' => $parent->id, 'user_id' => Yii::$app->user->identity->getId()]);
        $notices = TeacherNotice::findAll(['user_id' => Yii::$app->user->identity->getId(), 'student_id' => $parent->user_id, 'lesson_id' => $parent->id]);

        if(!$review) {

            $review = new TeacherReview();

            $review->user_id = Yii::$app->user->identity->getId();
            $review->lesson_id = $parent->id;
        }

        if(Yii::$app->request->isPost && $review->load(Yii::$app->request->post())) {

            $review->save();

            $action = Yii::$app->request->post('action', 'save');

            if($action == 'finish')
                $parent->status_id = LessonStatus::STATUS_FINISHED;

            $parent->save();

            Mailer::send('new-review', $parent->user_id, [
                '{link}' => Url::toRoute(['/student/lessons/show', 'id' => $model->id], true)
            ]);

            if($action == 'finish') {

                /** @var StudentLesson $nextLesson */
                $nextLesson = StudentLesson::find()
                    ->where(['user_id' => $parent->user_id, 'parent_id' => $parent->parent_id, 'status_id' => LessonStatus::STATUS_PLANNED, 'type_id' => LessonType::TYPE_OFFLINE])
                    ->andWhere(['>=', 'priority', $parent->priority])
                    ->one();

                if($nextLesson) {
                    $nextLesson->status_id = LessonStatus::STATUS_AVAILABLE;
                    $nextLesson->save();
                }
            }
        }

        return $this->render('lesson', ['answer' => $answer, 'model' => $parent, 'review' => $review, 'notices' => $notices]);
    }

    /**
     * AJAX поиск пользователей для выпадающейго списка (по начальным символам имени)
     * @param null $q
     * @param null $id
     * @param null $role
     * @return array
     */
    public function actionAjaxSearch($q = null, $id = null, $role = null)
    {
        //формат ответе - JSON
        Yii::$app->response->format = Response::FORMAT_JSON;

        //ответ по умолчанию
        $out = ['results' => ['id' => '', 'text' => '']];

        //если запрос не пуст
        if (!is_null($q)) {

            //сформировать запрос к базе
            $query = new Query();
            $query->select('id, name, username')->from('user');
            $query->where(['like','name',$q]);
            if(!empty($role)){
                $query->andWhere(['role_id' => (int)$role]);
            }
            $query->limit(20);

            //получить данные и сформировать ответ
            $command = $query->createCommand();
            $data = array_values($command->queryAll());
            $tmp = [];

            foreach($data as $index => $arr){
                $tmp[] = ['id' => $arr['id'], 'text' => $arr['name']];
            }

            $out['results'] = $tmp;
        }
        //если пуст запрос но указан ID
        elseif ($id > 0) {
            //найти по ID и сформировать ответ
            $user = User::findOne((int)$id);
            if(!empty($user)){
                $out['results'] = ['id' => $id, 'text' => $user->name];
            }
        }

        //вернуть ответ
        return $out;
    }

    /**
     * Append situational lesson to student's course
     *
     * @param $id
     * @return string
     *
     * @throws NotFoundHttpException
     */
    public function actionAppendLesson($id)
    {
        $relation = StudentCourse::findOne($id);

        $course = $relation->course;

        $model = new StudentLesson();

        $model->user_id = $relation->user_id;
        $model->parent_id = $relation->id;

        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())){

            if($model->validate()) {

                $previous = StudentLesson::findOne($model->previous_id);

                if($previous)
                    $model->priority = $previous->priority;
                else
                    $model->priority = 1;

                $model->save();

                $this->redirect(Url::to(['/teacher/students/course/', 'id' => $id]));
            }
        }

        return $this->renderAjax('_ajax_append_lesson',compact('model','course'));
    }

    /**
     * Append notice to student
     *
     * @return Response
     */
    public function actionAppendNotice()
    {
        $model = new TeacherNotice();

        $model->user_id = Yii::$app->user->identity->getId();

        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            if($model->save()) {

            }

        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Append interactive lesson to student's course
     *
     * @param $id
     * @return string
     *
     * @throws NotFoundHttpException
     */
    public function actionAppendSession($id)
    {
        $relation = StudentCourse::findOne($id);

        $course = $relation->course;

        $model = new StudentLesson();

        $model->start_date = date('Y-m-d', strtotime('+1 day'));

        $model->user_id = $relation->user_id;
        $model->parent_id = $relation->id;
        $model->type_id = LessonType::TYPE_ONLINE;
        $model->status_id = LessonStatus::STATUS_AVAILABLE;

        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            $inputDate = $model->start_date;

            $model->start_date = $inputDate . ' ' . $model->start_time . ':00:00';
            $model->end_date = $inputDate . ' ' . ((int) $model->start_time + 1) . ':00:00';

            if($model->validate()) {

                $model->save();

                $this->redirect(Url::to(['/teacher/students/course/', 'id' => $id]));
            }
        }

        return $this->renderAjax('_ajax_append_session',compact('model','course'));
    }

    /**
     * Append interactive lesson to student's course
     *
     * @param $id
     * @return string
     *
     * @throws NotFoundHttpException
     */
    public function actionChangeSession($id)
    {

        $model = StudentLesson::findOne($id);

        $model->start_time = date('H', strtotime($model->start_date));
        $model->start_date = date('Y-m-d', strtotime($model->start_date));

        $course = $model->parent->course;
        $parent = $model->parent_id;

        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            $action = Yii::$app->request->post('action', 'update');

            if($action == 'delete') {
                $model->delete();
                return $this->redirect(Url::to(['/teacher/students/course/', 'id' => $parent]));
            }

            $inputDate = $model->start_date;

            $model->start_date = $inputDate . ' ' . $model->start_time . ':00:00';
            $model->end_date = $inputDate . ' ' . ((int) $model->start_time + 1) . ':00:00';

            if($model->validate()) {

                $model->save();

                $this->redirect(Url::to(['/teacher/students/course/', 'id' => $parent]));
            }
        }

        return $this->renderAjax('_ajax_append_session',compact('model','course'));
    }

    /**
     * Append teacher to student
     */
    public function actionAppendTeacher()
    {

        $params = Yii::$app->request->post('User');

        $user = User::findOne($params['id']);

        if($user && $user->hasRole(Constants::RBAC_STUDENT) && !$user->teacher_id) {

            $relation = new StudentTeacher();
            $relation->student_id = $user->id;
            $relation->teacher_id = Yii::$app->user->identity->getId();

            $relation->is_active = 1;

            $relation->save();

            $user->teacher_id = $relation->teacher_id;
            $user->save();
        }

        $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     *
     * @return array
     */
    public function actionEvents($id)
    {

        $relation = StudentCourse::findOne($id);

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $lessons = StudentLesson::findAll(['parent_id' => $relation->id, 'type_id' => LessonType::TYPE_ONLINE]);

        $events = [];

        foreach ($lessons as $lesson){

            $event = new \yii2fullcalendar\models\Event();

            $event->id = $lesson->id;
            $event->title = 'Скайп';

            $event->start = date('Y-m-d\TH:i:s\Z',strtotime($lesson->start_date));
            $event->end = date('Y-m-d\TH:i:s\Z',strtotime($lesson->end_date));
            $events[] = $event;
        }

        return $events;
    }

    /**
     * Unlock student lesson
     * @param integer $id
     */
    public function actionUnlock($id)
    {
        $model = StudentLesson::findOne($id);

        $model->status_id = LessonStatus::STATUS_AVAILABLE;

        $model->save();

        $this->redirect(Yii::$app->request->referrer);
    }
}
