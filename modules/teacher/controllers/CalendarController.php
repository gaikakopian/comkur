<?php

namespace app\modules\teacher\controllers;

use app\helpers\Access;
use app\helpers\Constants;
use app\helpers\Help;
use app\models\Course;
use app\models\Lesson;
use app\models\LessonType;
use app\models\StudentAnswer;
use app\models\StudentCourse;
use app\models\StudentLesson;
use app\models\StudentLessonSearch;
use app\models\UserSearch;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\db\Query;
use app\models\User;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\rbac\Role;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * @package app\modules\teacher\controllers
 */
class CalendarController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [Constants::RBAC_TEACHER],
                    ],
                ],
            ]
        ];
    }

    /**
     * Show calendar
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return array
     */
    public function actionEvents()
    {

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $students = ArrayHelper::getColumn(User::findAll(['teacher_id' => Yii::$app->user->identity->getId()]), 'id');

        $lessons = StudentLesson::findAll(['user_id' => $students, 'type_id' => LessonType::TYPE_ONLINE]);

        $events = [];

        /** @var StudentLesson $lesson */
        foreach ($lessons as $lesson){

            $event = new \yii2fullcalendar\models\Event();

            $event->id = $lesson->id;
            $event->title = 'Скайп ' . $lesson->user->name;

            $event->start = date('Y-m-d\TH:i:s\Z', strtotime($lesson->start_date));
            $event->end = date('Y-m-d\TH:i:s\Z', strtotime($lesson->end_date));

            $events[] = $event;
        }

        return $events;
    }
}
