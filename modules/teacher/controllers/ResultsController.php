<?php

namespace app\modules\teacher\controllers;

use app\helpers\Access;
use app\helpers\Constants;
use app\helpers\Help;
use app\models\Course;
use app\models\CourseType;
use app\models\Lesson;
use app\models\LessonStatus;
use app\models\LessonType;
use app\models\StudentAnswer;
use app\models\StudentCourse;
use app\models\StudentLesson;
use app\models\StudentLessonSearch;
use app\models\StudentTeacher;
use app\models\TeacherNotice;
use app\models\TeacherReview;
use app\models\UserSearch;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\db\Query;
use app\models\User;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\rbac\Role;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Котроллер отвечающий за управление пользователями
 *
 * @copyright 	2017 Alex Nem
 * @link 		https://github.com/darkoffalex
 * @author 		Alex Nem
 *
 * @package app\modules\account\controllers
 */
class ResultsController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [Constants::RBAC_TEACHER],
                    ],
                ],
            ]
        ];
    }

    /**
     * Show demo results
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new StudentLessonSearch();

        $demoCourses = Course::findAll(['type_id' => CourseType::TYPE_DEMO]);
        $searchModel->status_id = LessonStatus::STATUS_REVIEW;
        $searchModel->course_id = ArrayHelper::getColumn($demoCourses, 'id');

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel','dataProvider'));
    }

    /**
     * Show demo result
     *
     * @param $id
     *
     * @return string
     */
    public function actionShow($id)
    {
        $model = StudentLesson::findOne($id);
        
        $answer = StudentAnswer::findOne(['lesson_id' => $model->lesson_id,'user_id'=> $model->user_id]);
        
        $review = new TeacherReview();

        $review->lesson_id = $model->id;
        $review->user_id = Yii::$app->user->identity->getId();

        if(Yii::$app->request->isPost && $review->load(Yii::$app->request->post())) {

            // Save teacher review

            if($review->save()) {

                // Save lesson status

                $model->status_id = LessonStatus::STATUS_FINISHED;
                $model->save();

                // Save active teacher for student

                $user = $model->user;
                $user->teacher_id = Yii::$app->user->identity->getId();

                $user->save();

                // Save student-teacher relation

                $relation = new StudentTeacher();

                $relation->student_id = $user->id;
                $relation->teacher_id = $user->teacher_id;

                $relation->save();

                return $this->redirect(Url::to(['/teacher/results']));
            }
        }

        return $this->render('show', compact('model', 'answer', 'review'));
    }
}
