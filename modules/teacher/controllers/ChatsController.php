<?php
/**
 * Created by PhpStorm.
 * User: zhek
 * Date: 31.08.17
 * Time: 14:23
 */

namespace app\modules\teacher\controllers;

use app\models\Chat;
use app\models\ChatSearch;
use app\models\Message;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ChatsController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['teacher'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Show list chats
     *
     * @return string
     */
    public function actionIndex()
    {

        $searchModel = new ChatSearch();

        $searchModel->user = true;

        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        $model = new Chat();

        if(\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post())) {

            $model->author_id = \Yii::$app->user->identity->getId();
            $model->recipient_id = null;
            $model->recipient_unread_count = 1;

            $text = $model->message;

            if($model->validate()) {

                $model->save();

                if($text) {

                    $message = new Message();

                    $message->author_id = $model->author_id;
                    $message->recipient_id = $model->recipient_id;
                    $message->chat_id = $model->id;

                    $message->content = $text;

                    $message->save();

                    return $this->redirect(Url::to(['/teacher/chats/show/', 'id' => $model->id]));
                }
            }
        }

        return $this->render('index', compact('searchModel','dataProvider', 'model'));
    }

    /**
     * Show chat
     *
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionShow($id)
    {
        $model = Chat::findOne(['id' => $id]);

        $userId = \Yii::$app->user->identity->getId();

        if(!$model || ($model->author_id != \Yii::$app->user->identity->getId() && $model->recipient_id != \Yii::$app->user->identity->getId()))
            throw new NotFoundHttpException('Чат не найден');

        if($model->author_id == $userId) {
            $model->author_last_date = new Expression('NOW()');
            $model->author_unread_count = 0;
        }
        elseif($model->recipient_id == $userId) {
            $model->recipient_last_date = new Expression('NOW()');
            $model->recipient_unread_count = 0;
        }

        $model->save(false);

        if(\Yii::$app->request->isPost) {

            $text = \Yii::$app->request->post('message');

            if($text) {

                $message = new Message();

                $message->content = $text;
                $message->author_id = \Yii::$app->user->identity->getId();
                $message->chat_id = $id;

                if($model->author_id == $userId) {
                    $message->recipient_id = $model->recipient_id;
                    $model->recipient_unread_count = 1;
                } else {
                    $message->recipient_id = $model->author_id;
                    $model->author_unread_count = 1;
                }

                $model->save('false');
                $message->save();
            }
        }

        return $this->render('show', ['model' => $model]);
    }
}