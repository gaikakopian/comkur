<?php
/**
 * Created by PhpStorm.
 * User: zhek
 * Date: 28.08.17
 * Time: 13:29
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\CoursesController */
/* @var $model \app\models\StudentLesson */
/* @var $answer \app\models\StudentAnswer */
/* @var $review \app\models\TeacherReview */
/* @var $notices \app\models\TeacherNotice[] */

$this->title = $model->lesson->name;

if($model->course)
    $this->params['breadcrumbs'][] = $model->lesson->course->name;

$this->params['breadcrumbs'][] = $model->user->name;
$this->params['breadcrumbs'][] = $model->lesson->name;

$notice = new \app\models\TeacherNotice();

$notice->student_id = $model->user->id;
$notice->user_id = Yii::$app->user->identity->getId();
$notice->lesson_id = $model->id;

$editorInit = "
    $('textarea.editor-area').redactor({
        minHeight : 180,
        maxHeight : 180,
        toolbarFixed : false,
        scroll : true,
        autoSize : false,
        plugins: ['fontsize','fontcolor','fullscreen','table','video'],
        lang : '".Yii::$app->language."'
    });";
Yii::$app->view->registerJs($editorInit,\yii\web\View::POS_END);
?>

<?php Yii::$app->view->registerCssFile('/backend/js/imperavi-redactor/redactor.css'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/redactor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/lang/ru.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontsize/fontsize.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontcolor/fontcolor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fullscreen/fullscreen.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/table/table.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/video/video.js'); ?>

<style>
    .box-comments .comment-text {
        margin-left: 5px;
    }
</style>

<?php if(!$review->hasErrors() && Yii::$app->request->isPost): ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i>Сохранено</h4>
        Отчет успешно сохранен
    </div>
<?php endif; ?>

<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">
            <?= $model->lesson->name; ?>
        </h3>
    </div>
    <div class="box-body">
        <?= $model->lesson->description; ?>
        <?php if($model->lesson->video_url): ?>
            <hr />
            <?= \app\helpers\Help::parseVideoUrl($model->lesson->video_url); ?>
        <?php endif; ?>
    </div>
    <div class="box-footer">
    </div>
</div>

<?php $form = ActiveForm::begin([
    'id' => 'review-answer-form',
    'options' => ['role' => 'form', 'method' => 'post'],
    'enableClientValidation'=>true,
    'enableAjaxValidation' => false,
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}\n",
    ],
]); ?>

<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Отчет педагогу по уроку</h3>
    </div>
    <div class="box-body">
        <?= $answer ? $answer->content : '-'; ?>
        <?php if($answer && $answer->video_url): ?>
            <hr />
            <?= \app\helpers\Help::parseVideoUrl($answer->video_url); ?>
        <?php endif; ?>
    </div>
</div>

<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Ответ педагога</h3>
    </div>
    <div class="box-body">
    <?php if($model->status_id == \app\models\LessonStatus::STATUS_REVIEW): ?>
        <?= $form->field($review, 'content')->textarea(['class' => 'editor-area form-control'])->label(false); ?>
    <?php else: ?>
        <?= $review->content ? $review->content : '-'; ?>
    <?php endif; ?>
    </div>
    <div class="box-footer">
        <?php if($model->status_id == \app\models\LessonStatus::STATUS_REVIEW): ?>
            <?= \yii\helpers\Html::submitButton('Отправить', ['role' => 'button', 'class' => 'btn btn-info', 'name' => 'action', 'value' => 'save']); ?>
            <?= \yii\helpers\Html::submitButton('Открыть следующий урок', ['role' => 'button', 'class' => 'btn btn-success', 'name' => 'action', 'value' => 'finish']); ?>
        <?php endif; ?>
    </div>
</div>

<?php ActiveForm::end(); ?>


<?php $form = ActiveForm::begin([
    'id' => 'notice-form',
    'action' => Url::to(['/teacher/students/append-notice']),
    'options' => ['role' => 'form', 'method' => 'post']
]); ?>

<div class="hidden" style="display: none">
    <?= $form->field($notice, 'student_id'); ?>
    <?= $form->field($notice, 'lesson_id'); ?>
</div>

<div class="row">
    <div class="col-md-12">
        <!-- Box Comment -->
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Заметки по уроку</h3>
            </div>

            <!-- /.box-body -->
            <div class="box-footer box-comments">

                <?php foreach($notices as $comment): ?>

                    <div class="box-comment">
                        <div class="comment-text">
                              <span class="username">
                                  <?= $comment->user->name; ?>
                                  <span class="text-muted pull-right">
                                      <?= $comment->create_date; ?>
                                  </span>
                              </span><!-- /.username -->
                            <?= $comment->content; ?>
                        </div>
                        <!-- /.comment-text -->
                    </div>
                    <!-- /.box-comment -->

                <?php endforeach; ?>
            </div>

            <!-- /.box-footer -->
            <div class="box-footer">
                <?= $form->field($notice, 'content')->textarea(['placeholder' => 'Напишите текст заметки', 'rows' => 4])->label(false); ?>
                <?= \yii\helpers\Html::submitButton('Сохранить', ['class' => 'btn btn-info btn-flat']); ?>
            </div>
        </div>
        <!-- /.box-footer -->
    </div>
    <!-- /.box -->
</div>

<?php ActiveForm::end(); ?>