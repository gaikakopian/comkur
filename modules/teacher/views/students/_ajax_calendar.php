<?php
/**
 * Created by PhpStorm.
 * User: zhek
 * Date: 29.08.17
 * Time: 16:18
 */

/** @var string $url */

$trigger = <<<JS
function(calEvent, jsEvent, view) {
    $('#session').attr('href', '/teacher/students/change-session/' + calEvent.id).trigger('click');
}
JS;

echo yii2fullcalendar\yii2fullcalendar::widget([
    'options' => [
        'lang' => 'ru',
    ],
    'clientOptions' => [
        'minTime' => '08:00:00',
        'maxTime' => '20:00:00',
        'eventClick' => new \yii\web\JsExpression($trigger)
    ],
    'ajaxEvents' => $url,
    'defaultView' => 'agendaWeek'
]);
