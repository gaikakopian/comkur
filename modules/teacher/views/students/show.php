<?php

use app\models\User;
use branchonline\lightbox\LightboxAsset;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $model User */
/* @var $this \yii\web\View */
/* @var $user User */
/* @var $notices \app\models\TeacherNotice[] */

$this->registerAssetBundle(LightboxAsset::className());

$user = Yii::$app->user->identity;

$this->title = 'Просмотр ученика';
$this->params['breadcrumbs'][] = ['label' => 'Ученики', 'url' => Url::to(['/teacher/students/index'])];
$this->params['breadcrumbs'][] = $this->title;

$notice = new \app\models\TeacherNotice();

$notice->student_id = $model->id;
$notice->user_id = $user->getId();

?>

<style>
    .box-comments .comment-text {
        margin-left: 5px;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">

            <?php $form = ActiveForm::begin([
                'id' => 'student-active-form',
                'action' => Url::to(['/teacher/students/append-teacher']),
                'options' => ['role' => 'form', 'method' => 'post']
            ]); ?>

            <div class="hidden" style="display: none">
                <?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
            </div>

            <div class="box-body">
                <?php echo DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'name',
                        'last_online_at:datetime',
                        'skype'
                    ]
                ]); ?>
            </div>

            <div class="box-footer">

                <?php if ($model->status_id == \app\models\UserStatus::STATUS_ACTIVE && !$model->teacher_id): ?>
                    <?= \yii\helpers\Html::submitButton('Стать преподавателем', ['class' => 'btn btn-success']); ?>
                <?php endif; ?>

                <a class="btn btn-default" href="<?= Url::to(['/teacher/students/index']); ?>">Назад к
                    списку</a>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Оплаченные курсы</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover ajax-reloadable">
                            <?= $this->render('/students/_courses', ['coursesOfStudent' => $model->courseOfStudents]); ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <?php if($model->teacher_id == Yii::$app->user->identity->getId()): ?>

            <?php $form = ActiveForm::begin([
                'id' => 'notice-form',
                'action' => Url::to(['/teacher/students/append-notice']),
                'options' => ['role' => 'form', 'method' => 'post']
            ]); ?>

            <div class="hidden" style="display: none">
                <?= $form->field($notice, 'student_id'); ?>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- Box Comment -->
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">Заметки по ученику</h3>
                        </div>

                        <!-- /.box-body -->
                        <div class="box-footer box-comments">

                            <?php foreach($notices as $comment): ?>

                            <div class="box-comment">
                                <div class="comment-text">
                                  <span class="username">
                                    <?= $notice->user->name; ?>
                                    <span class="text-muted pull-right"><?= Yii::$app->formatter->asDatetime($comment->create_date); ?></span>
                                  </span><!-- /.username -->
                                    <?= $comment->content; ?>
                                </div>
                                <!-- /.comment-text -->
                            </div>
                            <!-- /.box-comment -->

                            <?php endforeach; ?>
                        </div>

                        <!-- /.box-footer -->
                        <div class="box-footer">
                            <?= $form->field($notice, 'content')->textarea(['placeholder' => 'Напишите текст заметки', 'rows' => 4])->label(false); ?>
                            <?= \yii\helpers\Html::submitButton('Сохранить', ['class' => 'btn btn-info btn-flat']); ?>
                        </div>
                    </div>
                        <!-- /.box-footer -->
                </div>
                <!-- /.box -->

                <?php ActiveForm::end(); ?>
            </div>
        </div>

        <?php endif; ?>
</div>
