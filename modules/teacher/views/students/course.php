<?php

use kartik\grid\GridView;
use yii\helpers\Url;
use app\models\User;
use branchonline\lightbox\LightboxAsset;

/* @var $student User */
/* @var $course \app\models\Course */
/* @var $parent \app\models\StudentCourse */
/* @var $this \yii\web\View */

$this->registerAssetBundle(LightboxAsset::className());

$user = Yii::$app->user->identity;

$this->title = $course->name;
$this->params['breadcrumbs'][] = ['label' => 'Ученики', 'url' => Url::to(['/teacher/students/index'])];
$this->params['breadcrumbs'][] = $parent->user->name;
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
    ],
    [
        'attribute' => 'id',
        'contentOptions' => ['style' => 'width:70px;'],
        'headerOptions' => [],
        'enableSorting' => false,
        'visible' => false
    ],
    [
        'attribute' => 'lesson.name',
        'enableSorting' => false,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'value' => function ($model) {

            $title = \yii\helpers\Html::a($model->lesson->name, \yii\helpers\Url::to(['/teacher/students/lesson', 'id' => $model->id]));

            if($model->lesson->theme)
                $title .= '<br />' . \yii\helpers\Html::tag('span', $model->lesson->theme->name, ['class' => 'text-muted']);

            return $title;
        }
    ],
    [
        'attribute' => 'status.name',
    ],
    [
        'label' => '',
        'format' => 'raw',
        'value' => function($model) {

            /** @var \app\models\StudentLesson $model */
            if($model->status_id == \app\models\LessonStatus::STATUS_PLANNED && (!$model->parent->access_for_part || ($model->lesson->course_part >= $model->parent->access_for_part)))
                return \yii\helpers\Html::a('Разблокировать', Url::to(['/teacher/students/unlock', 'id' => $model->id]), ['role' => 'button', 'class' => 'btn btn-success']);

            return '';
        }
    ]
];
?>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'pjax' => false,
                ]); ?>
            </div>

            <div class="box-footer">
                <a href="<?= Url::to(['/teacher/students/append-lesson', 'id' => $parent->id]); ?>" data-toggle="modal" data-target=".modal-main" class="btn btn-success">Добавить урок из каталога</a>
            </div>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <?php echo $this->render('_ajax_calendar', ['url' => Url::to(['/teacher/students/events', 'id' => $parent->id])]); ?>
            </div>

            <div class="box-footer">
                <a href="<?= Url::to(['/teacher/students/append-session', 'id' => $parent->id]); ?>" data-toggle="modal" data-target=".modal-main" class="btn btn-info">Добавить интерактивыный урок</a>
                <a href="<?= Url::to(['/teacher/students/change-session', 'id' => $parent->id]); ?>" data-toggle="modal" data-target=".modal-main" class="btn btn-info" id="session" style="display: none">Изменить интерактивыный урок</a>
            </div>
        </div>
    </div>
</div>