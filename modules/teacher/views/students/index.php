<?php

use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $searchModel \app\models\UserSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\teacher\controllers\StudentsController */
/* @var $user \app\models\User */

$controller = $this->context;
$user = Yii::$app->user->identity;

$type = Yii::$app->request->get('type', 'all');

$this->title = 'Ученики';

if($type == 'new')
    $this->title = 'Новые ученики';

$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'attribute' => 'name',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\User */
            return \yii\helpers\Html::a($model->name, Url::to(['/teacher/students/show', 'id' => $model->id]));
        },
    ],
    [
        'label' => 'Кол-во курсов',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\User */
            return sizeof($model->courseOfStudents);
        },
    ]
];

?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header"></div>
            <div class="box-body">
                <?= GridView::widget([
                    'filterModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'pjax' => false,
                ]); ?>
            </div>
            <div class="box-footer">

            </div>
        </div>
    </div>
</div>
