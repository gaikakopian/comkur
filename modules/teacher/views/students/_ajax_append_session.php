<?php

use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;

/* @var $model \app\models\StudentLesson */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\UsersController */
/* @var $course \app\models\Course */

$controller = $this->context;
?>

<div class="modal-header">
    <h4 class="modal-title">Интерактивный урок в рамках курса "<?= $course->name; ?>"</h4>
</div>

<?php $form = ActiveForm::begin([
    'id' => 'append-session-form',
    'options' => ['role' => 'form', 'method' => 'post'],
    'enableClientValidation'=>false,
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}\n",
    ],
]); ?>

<div class="modal-body">
    <div class="row">
        <div class="col-sm-8">
            <?= $form->field($model, 'start_date')->widget(DatePicker::className(),[
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]); ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'start_time')->dropDownList([
                9 => '9:00 - 10:00',
                10 => '10:00 - 11:00',
                11 => '11:00 - 12:00',
                12 => '12:00 - 13:00',
                13 => '13:00 - 13:00',
                14 => '14:00 - 15:00',
                15 => '15:00 - 16:00',
                16 => '16:00 - 17:00',
                17 => '17:00 - 18:00',
                18 => '18:00 - 19:00',
                19 => '19:00 - 20:00',
            ]); ?>
        </div>
    </div>

</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
    <?php if($model->isNewRecord): ?>
        <button type="submit" class="btn btn-primary" name="action" value="append">Добавить</button>
    <?php else: ?>
        <button type="submit" class="btn btn-primary" name="action" value="update">Сохранить</button>
        <button type="submit" class="btn btn-danger" name="action" value="delete">Удалить</button>
    <?php endif; ?>
</div>

<?php ActiveForm::end(); ?>
