<?php

use app\helpers\Constants;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Course;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $model \app\models\StudentLesson */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\UsersController */
/* @var $course \app\models\Course */

$controller = $this->context;
?>

<div class="modal-header">
    <h4 class="modal-title">Урок из каталога рамках курса "<?= $course->name; ?>"</h4>
</div>

<?php $form = ActiveForm::begin([
    'id' => 'append-lesson-form',
    'options' => ['role' => 'form', 'method' => 'post'],
    'enableClientValidation'=>false,
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}\n",
    ],
]); ?>

<div class="modal-body">
    <?php
    $availableLessons = \app\models\Lesson::findAll(['course_id' => null]);
    $courseLessons = \app\models\StudentLesson::findAll(['parent_id' => $model->parent_id, 'user_id' => $model->user_id, 'type_id' => \app\models\LessonType::TYPE_OFFLINE]);
    $themes = \app\models\LessonTheme::find()->all();
    ?>

    <?= $form->field($model, 'theme')->dropDownList(ArrayHelper::map($themes,'id','name'), ['prompt' => 'Выберите тему урока']); ?>

    <div class="theme-lessons" style="display: none">
        <?= $form->field($model, 'lesson_id')->dropDownList([]); ?>
        <?= $form->field($model, 'previous_id')->dropDownList(ArrayHelper::map($courseLessons,'id','lesson.name')); ?>
        <?= $form->field($model, 'status_id')->dropDownList(ArrayHelper::map(\app\models\LessonStatus::find()->where(['id' => [\app\models\LessonStatus::STATUS_PLANNED, \app\models\LessonStatus::STATUS_AVAILABLE]])->all(), 'id', 'name')); ?>

    </div>

</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
    <button type="submit" class="btn btn-primary" id="process" disabled>Добавить</button>
</div>

<?php ActiveForm::end(); ?>

<script>
    $('#studentlesson-theme').change(function() {

        var id = $(this).val();

        $('.theme-lessons').hide();
        $('#process').attr('disabled', 'disabled');

        if(id > 0) {

            var count = 0;

            $.get('/api/lessons', {theme_id: id, 'access-token': '<?= Yii::$app->user->identity->getAuthKey(); ?>'}, function(data) {

                var options, index, select, option;

                // Get the raw DOM object for the select box
                select = document.getElementById('studentlesson-lesson_id');

                // Clear the old options
                select.options.length = 0;

                // Load the new options
                options = data; // Or whatever source information you're working with

                for (index = 0; index < options.length; ++index) {
                    option = options[index];
                    select.options.add(new Option(option.name, option.id));
                    count++;
                }

                if(count > 0) {
                    $('.theme-lessons').show();
                    $('#process').removeAttr('disabled');
                }
            });
        }

    });
</script>