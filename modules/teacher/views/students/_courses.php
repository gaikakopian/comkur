<?php

use yii\helpers\Url;
use app\helpers\Constants;

/* @var $coursesOfStudent \app\models\StudentCourse[] */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\UsersController */

$controller = $this->context;
?>
<thead>
<tr>
    <th>Наименование курса</th>
    <th>Тип курса</th>
    <th>Доступ до части</th>
</tr>
</thead>
<tbody>
<?php if(!empty($coursesOfStudent)): ?>
    <?php foreach($coursesOfStudent as $course): ?>
        <tr>
            <td>
                <?php if($course->user->teacher_id == Yii::$app->user->identity->getId()): ?>
                    <?= \yii\helpers\Html::a($course->course->name, Url::to(['/teacher/students/course', 'id' => $course->id])) ?>
                <?php else: ?>
                    <?= $course->course->name; ?>
                <?php endif; ?>
            </td>
            <td>
                <?php $types = [
                    Constants::COURSE_TYPE_DEMO => 'Тест',
                    Constants::COURSE_TYPE_REGULAR => 'Обычный'
                ]; ?>
                <?= !empty($types[$course->course->type_id]) ? $types[$course->course->type_id] : null; ?>
            </td>
            <td>
                <?= $course->access_for_part ? $course->access_for_part : 'Полный доступ'; ?>
            </td>
        </tr>
    <?php endforeach; ?>
<?php else: ?>
    <tr>
        <td colspan="4">Нет курсов</td>
    </tr>
<?php endif; ?>
</tbody>
