<?php
use app\models\User;
use app\helpers\Access;
?>

<?php /* @var $user User */ ?>
<?php $user = Yii::$app->user->identity; ?>

<aside class="main-sidebar">

    <section class="sidebar">
        <?php $c = Yii::$app->controller->id; ?>
        <?php $a = Yii::$app->controller->action->id; ?>
        <?php $t = Yii::$app->request->get('type', 'all'); ?>
        <?php $m = Yii::$app->controller->module->unreadCount; ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    [
                        'label' => 'Ученики',
                        'icon' => 'users',
                        'active' => $c == 'students' && $t == 'all',
                        'url' => ['/teacher/students'],
                    ],
                    [
                        'label' => 'Новые ученики',
                        'icon' => 'fire',
                        'active' => $c == 'students' && $t == 'new',
                        'url' => ['/teacher/students?type=new'],
                    ],
                    [
                        'label' => 'Результаты теста',
                        'icon' => 'check-circle',
                        'active' => $c == 'results',
                        'url' => ['/teacher/results'],
                    ],
                    [
                        'label' => 'Расписание',
                        'icon' => 'calendar',
                        'active' => $c == 'calendar',
                        'url' => ['/teacher/calendar'],
                    ],
                    [
                        'label' => 'Сообщение администратору',
                        'icon' => 'comments',
                        'active' => $c == 'chats',
                        'url' => ['/teacher/chats'],
                        'template' => $m ? '<a href="{url}"><span class="pull-left-container"><small class="label pull-left bg-green">'. Yii::$app->controller->module->unreadCount .'</small></span>{label}</a>' : '<a href="{url}">{icon} {label}</a>'
                    ],
                    [
                        'label' => 'Выход',
                        'icon' => 'sign-out',
                        'url' => ['/logout']
                    ]
                ],
            ]
        ) ?>

    </section>

</aside>
