<?php
/**
 * Created by PhpStorm.
 * User: zhek
 * Date: 28.08.17
 * Time: 13:29
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\CoursesController */
/* @var $model \app\models\StudentLesson */
/* @var $answer \app\models\StudentAnswer */
/* @var $review \app\models\TeacherReview */
/* @var $notices \app\models\TeacherNotice[] */

$this->title = $model->lesson->name;

if($model->course)
    $this->params['breadcrumbs'][] = $model->lesson->course->name;

$this->params['breadcrumbs'][] = $model->user->name;
$this->params['breadcrumbs'][] = $model->lesson->name;

$notice = new \app\models\TeacherNotice();

$notice->student_id = $model->user->id;
$notice->user_id = Yii::$app->user->identity->getId();
$notice->lesson_id = $model->id;

$editorInit = "
    $('textarea.editor-area').redactor({
        minHeight : 180,
        maxHeight : 180,
        toolbarFixed : false,
        scroll : true,
        autoSize : false,
        plugins: ['fontsize','fontcolor','fullscreen','table','video'],
        lang : '".Yii::$app->language."'
    });";
Yii::$app->view->registerJs($editorInit,\yii\web\View::POS_END);
?>

<?php Yii::$app->view->registerCssFile('/backend/js/imperavi-redactor/redactor.css'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/redactor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/lang/ru.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontsize/fontsize.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontcolor/fontcolor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fullscreen/fullscreen.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/table/table.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/video/video.js'); ?>

<style>
    .box-comments .comment-text {
        margin-left: 5px;
    }
</style>

<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">
            <?= $model->lesson->name; ?>
        </h3>
    </div>
    <div class="box-body">
        <?= $model->lesson->description; ?>
    </div>
    <div class="box-footer">
    </div>
</div>

<?php $form = ActiveForm::begin([
    'id' => 'review-answer-form',
    'options' => ['role' => 'form', 'method' => 'post'],
    'enableClientValidation'=>true,
    'enableAjaxValidation' => false,
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}\n",
    ],
]); ?>

<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Отчет педагогу по уроку</h3>
    </div>
    <div class="box-body">
        <?= $answer ? $answer->content : '-'; ?>
    </div>
    <?php if(!empty($answer) && !empty($answer->video_url)){ ?>
        <div class="box-header">
            <h3 class="box-title">Ссылка на видео</h3>
        </div>
        <div class="box-body">
            <a target="_blank" href='<?= $answer->video_url ?>'><?= $answer->video_url ?></a>
        </div>
    <?php } ?>
    <div class="box-footer">

    </div>
</div>

<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Ответ педагога</h3>
    </div>
    <div class="box-body">
        <?= $form->field($review, 'content')->textarea(['class' => 'editor-area form-control'])->label(false); ?>
    </div>
    <div class="box-footer">
        <?php if($model->status_id == \app\models\LessonStatus::STATUS_REVIEW): ?>
            <?= \yii\helpers\Html::submitButton('Завершить', ['role' => 'button', 'class' => 'btn btn-success']); ?>
        <?php endif; ?>
    </div>
</div>

<?php ActiveForm::end(); ?>