<?php

use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $searchModel \app\models\StudentLessonSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\teacher\controllers\StudentsController */
/* @var $user \app\models\User */

$controller = $this->context;
$user = Yii::$app->user->identity;

$this->title = 'Результаты теста';

$this->params['breadcrumbs'][] = $this->title;

$ids = \yii\helpers\ArrayHelper::getColumn($dataProvider->models, 'user_id');

$gridColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
    ],
    [
        'attribute' => 'user_id',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\StudentLesson */
            return \yii\helpers\Html::a($model->user->name, Url::to(['/teacher/results/show', 'id' => $model->id]));
        },
        'filter' => \yii\helpers\ArrayHelper::map(\app\models\User::find()->where(['id' => $ids])->all(), 'id', 'name'),
    ],
    [
        'attribute' => 'course_id',
        'format' => 'raw',
        'filter' =>  \yii\helpers\ArrayHelper::map(\app\models\Course::find()->where(['type_id' => \app\models\CourseType::TYPE_DEMO])->all(), 'id', 'name'),
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\StudentLesson */
            return $model->course->name;
        },
    ],
    [
        'attribute' => 'create_date',
        'label' => 'Дата прохождения теста',
        'filter' => \kartik\daterange\DateRangePicker::widget([
            'model' => $searchModel,
            'convertFormat' => true,
            'attribute' => 'create_date',
            'pluginOptions' => [
                'locale' => [
                    'format'=>'d.m.Y',
                    'separator'=>' - ',
                ],
            ],
        ]),
        'enableSorting' => true,
        'format' => 'datetime',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\StudentLesson */
            return $model->create_date;
        },
    ]
];

?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header"></div>
            <div class="box-body">
                <?= GridView::widget([
                    'filterModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'pjax' => false,
                ]); ?>
            </div>
            <div class="box-footer">

            </div>
        </div>
    </div>
</div>
