<?php

namespace app\modules\student;
use app\models\Message;
use app\models\User;
use Yii;
use yii\db\Expression;

/**
 * student module definition class
 */
class StudentModule extends \yii\base\Module
{
    public $unreadCount = 0;

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\student\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->layoutPath = "@app/modules/student/views/layouts";
        $this->viewPath = "@app/modules/student/views";
        $this->layout = 'main';
        $this->registerComponents();
    }

    /**
     * Override components settings
     */
    public function registerComponents(){
        \Yii::$app->setComponents([
            'assetManager' => [
                'bundles' => [
                    'dmstr\web\AdminLteAsset' => [
                        'skin' => 'skin-green'
                    ],
                ],
                'class' => 'yii\web\AssetManager'
            ],
        ]);
    }

    /**
     * Переопределение before action метода (выполнение перед каждым действием)
     *
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if(!parent::beforeAction($action)){
            return false;
        }

        Yii::$app->user->loginUrl = '/login';

        /* @var $user User */
        $user = !Yii::$app->user->isGuest ? Yii::$app->user->identity : null;

        if(!Yii::$app->user->isGuest) {

            $user->last_online_at = new Expression('NOW()');
            $user->update();

            $ownCount = Message::find()
                ->joinWith('chat')
                ->where(['{{message}}.recipient_id' => $user->id])
                ->andWhere(['<', '{{chat}}.author_last_date', new Expression('{{message}}.create_date')])
                ->andWhere(['{{chat}}.author_id' => $user->id])
                ->andWhere(['<>', '{{message}}.author_id', $user->id])
                ->count();

            $twoCount = Message::find()
                ->joinWith('chat')
                ->where(['{{message}}.recipient_id' => $user->id])
                ->andWhere(['<', '{{chat}}.recipient_last_date', new Expression('{{message}}.create_date')])
                ->andWhere(['{{chat}}.recipient_id' => $user->id])
                ->andWhere(['<>', '{{message}}.author_id', $user->id])
                ->count();

            $this->unreadCount = $ownCount + $twoCount;
        }

        return true;
    }
}
