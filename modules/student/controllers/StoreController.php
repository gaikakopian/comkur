<?php

namespace app\modules\student\controllers;

use app\helpers\Help;
use app\models\GoodsSearch;
use app\models\Goods;
use app\models\StudentCourse;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\models\User;
use app\helpers\Constants;
use app\models\Course;
use app\models\StudentLesson;
use app\models\CourseType;
use app\models\LessonStatus;
use yii\web\ForbiddenHttpException;
use yii\helpers\Url;
use YandexCheckout\Client;
use app\models\Payments;

/**
 * Котроллер отвечающий за управление витриной
 *
 * @package app\controllers
 */
class StoreController extends Controller
{

    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
       return [
           'access' => [
               'class' => AccessControl::className(),
               'rules' => [
                   [
                       'allow' => true,
                       'roles' => ['student'],
                   ],
                   [
                       'allow' => true,
                       'roles' => ['@'],
                       'actions' => ['buy-success']
                   ]
               ],
           ]
       ];
    }

    /**
     * Show and edit user profile
     *
     * @return string|array
     */
    public function actionIndex()
    {
        $searchModel = new GoodsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index',compact('searchModel','dataProvider'));
    }

    // Покупка курса через витрину
    public function actionBuy($id){

        $good = Goods::findOne(['id' => $id]);

        $student = User::findOne(Yii::$app->user->identity->id);

        if (!$student || !$student->hasRole(Constants::RBAC_STUDENT))
                throw new NotFoundHttpException('Student not found');

        $baseCourse = Course::findOne($good->course_id);
        if (!$baseCourse)
                throw new NotFoundHttpException('Course not found');


        $client = new Client();
        $client->setAuth(Yii::$app->params['yandex']['shop_id'], Yii::$app->params['yandex']['secret']);

        $payments = new Payments();

        $payments->user_id = $student->id;
        $payments->price = $good->price;
        $payments->status = 'pending';
        $payments->good_id = $good->id;
        $payments->created_at = date("Y-m-d H:i:s");

        $payments->save(false);

        $idempotenceKey = uniqid($payments->id, true);

        $response = $client->createPayment(
            array(
                'amount' => array(
                    'value' => $good->price,
                    'currency' => 'RUB',
                ),
                'confirmation' => array(
                    'type' => 'redirect',
                    'return_url' => 'http://cabinet.estermusic.ru/student/store'
                ),
                // 'capture' => true
            ),
            $idempotenceKey
        );

        $payments->order_id = $response->_id;
        $payments->save();

        $this->redirect($response->_confirmation->_confirmationUrl)->send();

    }

    // Поттвеждение покупки
    public function actionBuySuccess(){

        if ($_REQUEST){

            $payment = Payments::findOne(['order_id' => $_REQUEST['object']['id']]);

            if ($payment){

                $client = new Client();
                $client->setAuth(Yii::$app->params['yandex']['shop_id'], Yii::$app->params['yandex']['secret']);

                if ($_REQUEST['object']['status'] == 'succeeded' || $_REQUEST['object']['status'] == 'waiting_for_capture'){

                        $good = Goods::findOne(['id' => $payment->good_id]);
                        $baseCourse = Course::findOne($good->course_id);

                        $payment->status = 'success';
                        $payment->save();

                        $course = new StudentCourse();

                        $course->good_id = $good->id;
                        $course->user_id = $payment->user_id;
                        $course->course_id = $baseCourse->id;
                        $course->access_for_part = isset($good->part) ? $good->part : null;
                        $course->access_stage = isset($good->stage) ? $good->stage : null;
                        $course->good_id = $good->id;

                        $course->save();

                        foreach ($course->course->lessons as $courseLesson) {

                            $lesson = new StudentLesson();

                            $lesson->lesson_id = $courseLesson->id;
                            $lesson->course_id = $baseCourse->id;
                            $lesson->parent_id = $course->id;
                            $lesson->user_id = $payment->user_id;
                            $lesson->status_id = LessonStatus::STATUS_PLANNED;

                            // Set availability for demo courses

                            if($course->course->type_id == CourseType::TYPE_DEMO)
                                $lesson->status_id = LessonStatus::STATUS_AVAILABLE;

                            $lesson->save();
                        }
                } elseif ($_REQUEST['object']['status'] == 'canceled'){

                    $payment->status = 'canceled';
                    $payment->save();
                }
            }
        }elseif (Yii::$app->request->post()){

            $data = Yii::$app->request->post('object');

            $payment = Payments::findOne(['order_id' => $data['id']]);

            if ($payment){

                $client = new Client();
                $client->setAuth(Yii::$app->params['yandex']['shop_id'], Yii::$app->params['yandex']['secret']);

                if ($data['status'] == 'succeeded' || $data['status'] == 'waiting_for_capture'){

                        $good = Goods::findOne(['id' => $payment->good_id]);
                        $baseCourse = Course::findOne($good->course_id);

                        $payment->status = 'success';
                        $payment->save();

                        $course = new StudentCourse();

                        $course->good_id = $good->id;
                        $course->user_id = $payment->user_id;
                        $course->course_id = $baseCourse->id;
                        $course->access_for_part = isset($good->part) ? $good->part : null;
                        $course->access_stage = isset($good->stage) ? $good->stage : null;
                        $course->good_id = $good->id;

                        $course->save();

                        foreach ($course->course->lessons as $courseLesson) {

                            $lesson = new StudentLesson();

                            $lesson->lesson_id = $courseLesson->id;
                            $lesson->course_id = $baseCourse->id;
                            $lesson->parent_id = $course->id;
                            $lesson->user_id = $payment->user_id;
                            $lesson->status_id = LessonStatus::STATUS_PLANNED;

                            // Set availability for demo courses

                            if($course->course->type_id == CourseType::TYPE_DEMO)
                                $lesson->status_id = LessonStatus::STATUS_AVAILABLE;

                            $lesson->save();
                        }
                } elseif ($data['status'] == 'canceled'){

                    $payment->status = 'canceled';
                    $payment->save();

                }
            }
        }

        return true;
    }
}
