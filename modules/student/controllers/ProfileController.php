<?php

namespace app\modules\student\controllers;

use app\helpers\Help;
use app\models\User;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

/**
 * Котроллер отвечающий за управление профилем
 *
 * @package app\controllers
 */
class ProfileController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['student'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Show and edit user profile
     *
     * @return string|array
     */
    public function actionIndex()
    {

        $model = User::findOne(Yii::$app->user->identity->getId());

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        //сохранить старый пароль
        $oldPassHash = $model->password_hash;
        $oldAuthKey = $model->auth_key;

        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            //если пароль был указан - использовать его
            if(!empty($model->password)){
                $model->setPassword($model->password);
                $model->generateAuthKey();

            }
            //если не был указан - использовать старый пароль
            else {
                $model->password_hash = $oldPassHash;
                $model->auth_key = $oldAuthKey;
            }

            if(!empty($model->birth_date)){
                $model->birth_date = date('Y-m-d', strtotime($model->birth_date));
            }

            if($model->validate()) {

                $model->updated_at = date('Y-m-d H:i:s', time());
                $model->updated_by_id = Yii::$app->user->id;

                $model->update();
                $model->refresh();
            }
        }

        return $this->render('index', compact('model'));
    }
}
