<?php

namespace app\modules\student\controllers;

use app\helpers\Mailer;
use app\helpers\Sort;
use app\models\Lesson;
use app\models\LessonSearch;
use app\models\LessonStatus;
use app\models\LessonType;
use app\models\StudentAnswer;
use app\models\StudentLesson;
use app\models\StudentLessonSearch;
use app\models\TeacherReview;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\Controller;

/**
 * Котроллер отвечающий за управление уроками
 *
 * @copyright 	2017 Alex Nem
 * @link 		https://github.com/darkoffalex
 * @author 		Alex Nem
 *
 * @package app\controllers
 */
class LessonsController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['student'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Show lessons list
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new StudentLessonSearch();

        $searchModel->user_id = Yii::$app->user->identity->getId();
        $searchModel->type_id = LessonType::TYPE_OFFLINE;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, null);

        return $this->render('index', compact('searchModel','dataProvider'));
    }

    /**
     * Show and submit answer on lesson
     *
     * @param $id
     *
     * @return string
     */
    public function actionShow($id)
    {
        $model = StudentLesson::findOne($id);

        $answer = StudentAnswer::findOne(['lesson_id' => $model->lesson_id, 'user_id' => Yii::$app->user->getId()]);

        $review = TeacherReview::findOne(['lesson_id' => $model->id]);

        if(!$answer) {

            $answer = new StudentAnswer();

            $answer->user_id = Yii::$app->user->getId();
            $answer->lesson_id = $model->lesson_id;
            $answer->course_id = $model->course_id;

        }

        if($model->status_id == LessonStatus::STATUS_AVAILABLE) {

            $model->status_id = LessonStatus::STATUS_LEARNING;
            $model->save();

            $model->refresh();
        }

        if(Yii::$app->request->isPost && $answer->load(Yii::$app->request->post())) {

            if($answer->validate()) {

                $answer->save();

                if($model->status_id != LessonStatus::STATUS_REVIEW)
                    $model->status_id = LessonStatus::STATUS_REVIEW;

                $model->save();
                $model->refresh();

                if($model->user->teacher_id) {

                    Mailer::send('new-answer', $model->user->teacher_id, [
                        '{name}' => $model->user->name,
                        '{link}' => Url::toRoute(['/teacher/students/lesson', 'id' => $model->id], true)
                    ]);
                }
            }
        }

        return $this->render('show', compact('model', 'answer', 'review'));
    }
}
