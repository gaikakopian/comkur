<?php

namespace app\modules\student\controllers;

use app\models\Course;
use app\models\CourseSearch;
use app\models\StudentCourse;
use app\models\StudentCourseSearch;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\Controller;

/**
 * Котроллер отвечающий за управление курсами
 *
 * @copyright 	2017 Alex Nem
 * @link 		https://github.com/darkoffalex
 * @author 		Alex Nem
 *
 * @package app\controllers
 */
class CoursesController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['student'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Вывод списка курсов
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new StudentCourseSearch();

        $searchModel->user_id = \Yii::$app->user->identity->getId();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel','dataProvider'));
    }
}