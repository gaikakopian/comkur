<?php

namespace app\modules\student\controllers;

use yii\helpers\Url;
use yii\web\Controller;

/**
 * Default controller for the `student` module
 */
class DefaultController extends Controller
{
    /**
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        return $this->redirect(Url::to(['/student/courses/index']));
    }
}
