<?php

use kartik\grid\GridView;
use yii\helpers\Url;
use app\helpers\Access;
use yii\bootstrap\Html;
use app\helpers\Constants;
use yii\helpers\ArrayHelper;
use app\models\Course;


/* @var $searchModel \app\models\GoodsSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\GoodsController */
/* @var $user \app\models\User */

$controller = $this->context;
$user = Yii::$app->user->identity;

$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
    ],
    [
        'label' => 'Товар',
        'attribute' => 'title',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw'

    ],
    [
        'attribute' => 'photo',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'filter' => false,
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\Goods */
            return !empty($model->photo) ? Html::img('/backend/images/goods/'.$model->photo , ['width' => 150 , 'height' => 150]) : 'Нет фотографии';
        },
    ],
    [
        'label' => 'Описание',
        'attribute' => 'short_descr',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'filter' => false

    ],
    [
        'attribute' => 'price',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'filter' => false
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'contentOptions'=>['style'=>'width: 140px; text-align: center;'],
        'header' => 'Действия',
        'template' => '{buy-good}',
        'buttons' => [
            'buy-good' => function ($url,$model,$key) {
                /* @var $model \app\models\Goods */
                if (!$model->stored){
                    return Html::a('<span class="btn btn-danger">Купить</span>', Url::to(['/student/store/buy', 'id' => $model->id]), ['title' => 'Купить']);
                }else{
                    return Html::a('<span class="btn btn-success">Куплено</span>', '#' , ['title' => 'Куплено']);
                }         
            }
        ],
        'visibleButtons' => [
            'delete' => function ($model, $key, $index) use ($user) {/* @var $model \app\models\Goods */ return Yii::$app->authManager->checkAccess($user->id, 'deleteCourse');},
            'update' => function ($model, $key, $index) use ($user) {/* @var $model \app\models\Goods */ return Yii::$app->authManager->checkAccess($user->id, 'updateCourse');},
        ],
    ],
];

?>

<?php Yii::$app->view->registerCssFile('/backend/js/imperavi-redactor/redactor.css'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/redactor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/lang/ru.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontsize/fontsize.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontcolor/fontcolor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fullscreen/fullscreen.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/table/table.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/video/video.js'); ?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <?php if(Yii::$app->authManager->checkAccess($user->id, 'createCourse')): ?>
                    <a href="<?php echo Url::to(['/account/goods/create']); ?>" data-toggle="modal" data-target=".modal-main" class="btn btn-primary">Добавить товар</a>
                <?php endif; ?>
            </div>
            <div class="box-body">
                <?= GridView::widget([
                    'filterModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'pjax' => false,
                ]); ?>
            </div>
        </div>
    </div>
</div>
