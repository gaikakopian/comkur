<?php

/* @var $this \yii\web\View */

use yii\helpers\Url;

$user = Yii::$app->user->identity;

$this->title = 'Расписание';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <?php echo $this->render('_ajax_calendar', ['url' => Url::to(['/student/calendar/events'])]); ?>
            </div>
        </div>
    </div>
</div>