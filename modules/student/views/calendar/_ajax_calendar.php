<?php
/**
 * Created by PhpStorm.
 * User: zhek
 * Date: 29.08.17
 * Time: 16:18
 */

/** @var string $url */

echo yii2fullcalendar\yii2fullcalendar::widget([
    'options' => [
        'lang' => 'ru',
    ],
    'clientOptions' => [
        'minTime' => '08:00:00',
        'maxTime' => '20:00:00'
    ],
    'ajaxEvents' => $url,
    'defaultView' => 'agendaWeek'
]);
