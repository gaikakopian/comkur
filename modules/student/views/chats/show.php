<?php
/**
 * Created by PhpStorm.
 * User: zhek
 * Date: 31.08.17
 * Time: 14:25
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/** @var \app\models\Chat $model */

$this->title = 'Сообщение пользователя ' . $model->author->name;
$this->params['breadcrumbs'][] = ['label' => 'Сообщения', 'url' => Url::to(['/student/chats'])];
$this->params['breadcrumbs'][] = $this->title;

?>

<style>
    .direct-chat-messages {
        height: 600px;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <!-- DIRECT CHAT -->
        <div class="box box-warning direct-chat direct-chat-warning">
            <!--
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    <span data-toggle="tooltip" title="3 New Messages" class="badge bg-yellow">3</span>
                </div>
            </div>
            -->
            <!-- /.box-header -->
            <div class="box-body">
                <!-- Conversations are loaded here -->
                <div class="direct-chat-messages">
                    <!-- Message. Default to the left -->

                    <?php foreach ($model->messages as $message): ?>

                        <div class="direct-chat-msg <?= $model->author_id == $message->author_id ? 'right' : ''; ?>">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name <?= $model->author_id == $message->author_id ? 'pull-right' : 'pull-left'; ?>"><?= $message->author->name; ?></span>
                                <span class="direct-chat-timestamp <?= $model->author_id == $message->author_id ? 'pull-left' : 'pull-right'; ?>"><?= Yii::$app->formatter->asDatetime($message->create_date); ?></span>
                            </div>
                            <!-- /.direct-chat-info -->

                            <!-- /.direct-chat-img -->
                            <div class="direct-chat-text">
                                <?= $message->content; ?>
                            </div>
                            <!-- /.direct-chat-text -->
                        </div>

                        <hr/>

                    <?php endforeach; ?>

                </div>
                <!--/.direct-chat-messages-->

            </div>
            <!-- /.direct-chat-pane -->
        </div>

        <?php $form = ActiveForm::begin([
            'id' => 'chat-form',
            'options' => ['role' => 'form', 'method' => 'post'],
            'enableClientValidation'=>false,
            'enableAjaxValidation' => true,
            'fieldConfig' => [
                'template' => "{label}\n{input}\n{error}\n",
            ],
        ]); ?>

        <!-- /.box-body -->
        <div class="box-footer">
            <form action="#" method="post">
                <div class="input-group">
                    <?= \yii\helpers\Html::textInput('message', '', ['class' => 'form-control', 'placeholder' => 'Введите сообщение']); ?>
                    <span class="input-group-btn">
                        <?= \yii\helpers\Html::submitButton('Отправить', ['class' => 'btn btn-warning btn-flat']); ?>
                      </span>
                </div>
            </form>
        </div>

        <?php ActiveForm::end(); ?>

        <!-- /.box-footer-->
    </div>
    <!--/.direct-chat -->
</div>
<!-- /.col -->
