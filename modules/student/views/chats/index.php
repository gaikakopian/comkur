<?php
/**
 * Created by PhpStorm.
 * User: zhek
 * Date: 31.08.17
 * Time: 14:25
 */

use kartik\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\helpers\Access;
use yii\bootstrap\Html;
use app\helpers\Constants;

/* @var $searchModel \app\models\ChatSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\ChatsController */
/* @var $user \app\models\User */
/* @var $model \app\models\Chat */

$controller = $this->context;
$user = Yii::$app->user->identity;

$this->title = 'Сообщения';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
    ],
    [
        'attribute' => 'title',
        'format' => 'raw',
        'value' => function($model) {
            return Html::a($model->title ? $model->title : '(без темы)', Url::to(['/student/chats/show', 'id' => $model->id]));
        }
    ],
    [
        'label' => 'Отправитель',
        'attribute' => 'author_id',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'value' => function($model) {
            return $model->author->name;
        }
    ],
    [
        'label' => 'Получатель',
        'attribute' => 'recipient_id',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'value' => function($model) {
            return $model->recipient_id ? $model->recipient->name : '(Администратор)';
        }
    ],
    'create_date:datetime',
    'update_date:datetime',
    [
        'header' => 'Кол-во сообщений',
        'enableSorting' => false,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\Chat */
            return (int) count($model->messages);
        },
    ]
];

?>

<?php $form = ActiveForm::begin([
    'id' => 'chat-form',
    'options' => ['role' => 'form', 'method' => 'post'],
    'enableClientValidation'=>false,
    'enableAjaxValidation' => false,
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}\n"
    ]
]); ?>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">
                    Новое сообщение
                </h3>
            </div>
            <div class="box-body">
                <?= $form->field($model, 'title')->textInput(); ?>
                <?= $form->field($model, 'message')->textarea(['class' => 'form-control', 'rows' => 4]); ?>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-success">Отправить</button>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>


<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    История сообщенией
                </h3>
            </div>
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'pjax' => false,
                    'rowOptions' => function($chat) {
                        return ['class' => (\app\models\Chat::unreadCount($chat->id, Yii::$app->user->identity->getId()) > 0) ? 'bg-success text-bold' : ''];
                    }
                ]); ?>
            </div>
        </div>
    </div>
</div>