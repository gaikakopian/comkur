<?php

use kartik\grid\GridView;
use app\models\Course;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;

/* @var $searchModel \app\models\StudentLessonSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\CoursesController */
/* @var $user \app\models\User */

$controller = $this->context;
$user = Yii::$app->user->identity;

$this->title = 'Мои уроки';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'label' => 'Название курса',
        'attribute' => 'parent_id',
       // 'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'filter' => ArrayHelper::map(\app\models\StudentCourse::find()->where(['user_id' => Yii::$app->user->identity->getId()])->all(),'id','course.name'),
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){

            $title = null;

            if($model->parent && $model->parent->course)
                $title = $model->parent->course->name;

            return $title;
        },
        'group' => true,
    ],
    [
        'label' => 'Название урока',
        'attribute' => 'lesson_id',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'value' => function ($model) use ($user) {

            $title = $model->lesson->name;

            if($model->lesson->theme_id)
                $title .= '<br /><span class="text-muted">Урок из каталога: ' . $model->lesson->theme->name . '</span>';

            /** @var \app\models\StudentLesson $model */

            if($model->isAvailable())
                return \yii\helpers\Html::a($title, \yii\helpers\Url::to(['/student/lessons/show/', 'id' => $model->id]));

            return $title;
        }
    ],

    [
        'attribute' => 'status_id',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'filter' => ArrayHelper::map(\app\models\LessonStatus::find()->all(),'id','name'),
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\StudentLesson */
            return !empty($model->status_id) ? $model->status->name : null;
        },
    ]
];

?>

<?php if(!$user->teacher_id): ?>
    <div class="alert alert-danger">
        <h4><i class="icon fa fa-info-circle"></i>Внимание</h4>
        Без преподавателя возможно прохождение только демо-курсов.
    </div>
<?php else: ?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">
            Преподаватель
        </h3>
    </div>
    <div class="box-body">
        <?php echo DetailView::widget([
            'model' => $user->teacher,
            'attributes' => [
                'name',
                'last_online_at:datetime',
                'skype'
            ]
        ]); ?>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header"></div>
            <div class="box-body">
                <?= GridView::widget([
                    'filterModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'pjax' => false,
                ]); ?>
            </div>
        </div>
    </div>
</div>
