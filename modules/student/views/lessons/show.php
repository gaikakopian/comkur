<?php
/**
 * Created by PhpStorm.
 * User: zhek
 * Date: 28.08.17
 * Time: 13:29
 */

use kartik\form\ActiveForm;

/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\CoursesController */
/* @var $model \app\models\StudentLesson */
/* @var $answer \app\models\StudentAnswer */
/* @var $review \app\models\TeacherReview */

$this->title = $model->course ? $model->course->name : 'Мои уроки';

$this->params['breadcrumbs'][] = $this->title;

if($model->course)
    $this->params['breadcrumbs'][] = $model->course->name;

$this->params['breadcrumbs'][] = $model->lesson->name;

$editorInit = "
    $('textarea.editor-area').redactor({
        minHeight : 180,
        maxHeight : 180,
        toolbarFixed : false,
        scroll : true,
        autoSize : false,
        plugins: ['fontsize','fontcolor','fullscreen','table','video'],
        lang : '".Yii::$app->language."'
    });";
Yii::$app->view->registerJs($editorInit,\yii\web\View::POS_END);

?>

<?php Yii::$app->view->registerCssFile('/backend/js/imperavi-redactor/redactor.css'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/redactor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/lang/ru.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontsize/fontsize.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontcolor/fontcolor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fullscreen/fullscreen.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/table/table.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/video/video.js'); ?>

<div class="row">
    <div class="col-sm-3">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Уроки курса</h3>
            </div>
            <div class="box-body no-padding" style="">
                <ul class="nav nav-pills nav-stacked">
                    <?php
                    /** @var \app\models\StudentLesson $lesson */
                    foreach($model->parent->getLessons()->orderBy(['priority' => SORT_ASC, 'create_date' => SORT_ASC])->all() as $lesson): ?>
                        <?php if($lesson->lesson): ?>
                            <li class="<?= $lesson->id == $model->id ? 'active' : ''; ?>">
                                <?php if($lesson->isAvailable()): ?>
                                    <?= \yii\bootstrap\Html::a($lesson->lesson->name . '<span class="label label-default pull-right">' . $lesson->status->name . '</span>', \yii\helpers\Url::to(['/student/lessons/show', 'id' => $lesson->id])); ?>
                                <?php else: ?>
                                    <a><?= $lesson->lesson->name; ?> <span class="label label-default pull-right"><?= $lesson->status->name; ?></span></a>
                                <?php endif; ?>
                            </li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
            <!-- /.box-body -->
        </div>
    </div>

    <div class="col-sm-9">

        <?php if(!$answer->hasErrors() && Yii::$app->request->isPost): ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i>Сохранено</h4>
                Ответ отправлен преподавателю
            </div>
        <?php endif; ?>

        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">
                    <?= $model->lesson->name; ?>
                </h3>
            </div>
            <div class="box-body">
                <?= $model->lesson->description; ?>
                <?php if($model->lesson->video_url): ?>
                    <hr />
                    <?= \app\helpers\Help::parseVideoUrl($model->lesson->video_url); ?>
                <?php endif; ?>
            </div>
        </div>

        <?php if($model->status_id == \app\models\LessonStatus::STATUS_LEARNING || $model->status_id == \app\models\LessonStatus::STATUS_AVAILABLE || $model->status_id == \app\models\LessonStatus::STATUS_REVIEW): ?>

            <?php $form = ActiveForm::begin([
                'id' => 'submit-answer-form',
                'options' => ['role' => 'form', 'method' => 'post'],
                'enableClientValidation'=>true,
                'enableAjaxValidation' => false,
                'fieldConfig' => [
                    'template' => "{label}\n{input}\n{error}\n",
                ],
            ]); ?>

            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Отчет педагогу по уроку</h3>
                </div>
                <div class="box-body">
                    <?= $form->field($answer, 'content')->textarea(['class' => 'editor-area form-control'])->label(false); ?>
                    <hr />
                    <?= $form->field($answer, 'video_url'); ?>
                </div>
                <div class="box-footer">
                    <?= \yii\helpers\Html::submitButton('Отправить', ['role' => 'button', 'class' => 'btn btn-success']); ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        <?php else: ?>

            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Отчет педагогу по уроку</h3>
                </div>
                <div class="box-body">
                    <?= $answer->content; ?>
                    <?php if($answer->video_url): ?>
                        <hr />
                        <?= \app\helpers\Help::parseVideoUrl($answer->video_url); ?>
                    <?php endif; ?>
                </div>
            </div>

        <?php endif; ?>

        <?php if($review): ?>
            <div class="box box-warning">
                <div class="box-header">
                    <h3 class="box-title">Ответ педагога</h3>
                </div>
                <div class="box-body">
                    <?= $review->content; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
