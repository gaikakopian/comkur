<?php

use kartik\grid\GridView;
use yii\helpers\Url;
use app\helpers\Access;
use yii\bootstrap\Html;
use app\helpers\Constants;

/* @var $searchModel \app\models\StudentCourseSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\CoursesController */
/* @var $user \app\models\User */

$controller = $this->context;
$user = Yii::$app->user->identity;

$this->title = 'Мои курсы';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
    ],
    [
        'attribute' => 'id',
        'filter' => false,
        'contentOptions' => ['style' => 'width:70px;'],
        'headerOptions' => [],
        'visible' => false
    ],
    [
        'label' => 'Название курса',
        'attribute' => 'course.name',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
    ],
    [
        'attribute' => 'course.type_id',
        'contentOptions' => [],
        'headerOptions' => [],
        'filter' => [
            Constants::COURSE_TYPE_DEMO => 'Тест',
            Constants::COURSE_TYPE_REGULAR => 'Обычный'
        ],
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){

            $types = [
                Constants::COURSE_TYPE_DEMO => 'Тест',
                Constants::COURSE_TYPE_REGULAR => 'Обычный'
            ];

            /* @var $model \app\models\StudentCourse */
            return !empty($types[$model->course->type_id]) ? $types[$model->course->type_id] : null;
        },
    ],
    [
        'header' => 'Кол-во уроков',
        'enableSorting' => false,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\StudentCourse */
            return $model->getLessons()->where(['type_id' => \app\models\LessonType::TYPE_OFFLINE])->count();
        },
    ],
    [
        'header' => '',
        'enableSorting' => false,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\StudentCourse */
            return Html::a('Приступить к изучению', Url::to(['/student/lessons', 'StudentLessonSearch[parent_id]' => $model->id]), ['role' => 'button', 'class' => 'btn btn-success']);
        },
    ]
];

?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header"></div>
            <div class="box-body">
                <?= GridView::widget([
                    //'filterModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'pjax' => false,
                ]); ?>
            </div>
        </div>
    </div>
</div>
