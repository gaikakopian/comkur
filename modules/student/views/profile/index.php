<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\models\User;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use app\helpers\Constants;
use branchonline\lightbox\LightboxAsset;
use yii\web\JsExpression;

/* @var $model User */
/* @var $this \yii\web\View */
/* @var $user User */

$this->registerAssetBundle(LightboxAsset::className());

$user = Yii::$app->user->identity;

$this->title = 'Личные данные';
$this->params['breadcrumbs'][] = $this->title;

$model->birth_date = $model->birth_date ? date('d.m.Y', strtotime($model->birth_date)) : $model->birth_date;

?>

<div class="row">
    <div class="col-md-12">

        <div class="box box-primary">

            <?php $form = ActiveForm::begin([
                'id' => 'edit-users-form',
                'options' => ['role' => 'form', 'method' => 'post'],
                'enableClientValidation'=>false,
                'fieldConfig' => [
                    'template' => "{label}\n{input}\n{error}\n",
                ],
            ]); ?>

            <div class="box-body">
                <?php if(!$model->hasErrors() && Yii::$app->request->isPost): ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i>Сохранено</h4>
                        Изменения успешно сохранены
                    </div>
                <?php endif; ?>

                <?= $form->field($model, 'username')->textInput(['readonly' => 'readonly']); ?>

                <hr />

                <?= $form->field($model, 'name')->textInput(); ?>

                <?= $form->field($model, 'birth_date')->widget(DatePicker::className(),[
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy'
                    ]
                ]); ?>

                <?= $form->field($model, 'phone_number')->textInput(); ?>

                <?= $form->field($model, 'skype')->textInput(); ?>

            </div>

            <div class="box-footer">
                <?= \yii\helpers\Html::submitButton('Сохранить', ['class' => 'btn btn-success']); ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

    </div>
</div>