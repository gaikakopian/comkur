<?php
use app\models\User;
use app\helpers\Access;
?>

<?php /* @var $user User */ ?>
<?php $user = Yii::$app->user->identity; ?>

<aside class="main-sidebar">

    <section class="sidebar">
        <?php $c = Yii::$app->controller->id; ?>
        <?php $a = Yii::$app->controller->action->id; ?>
        <?php $m = Yii::$app->controller->module->unreadCount; ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    [
                        'label' => 'Курсы',
                        'icon' => 'th-large',
                        'active' => $c == 'courses',
                        'url' => ['/student/courses']
                    ],
                    [
                        'label' => 'Уроки',
                        'icon' => 'file-text-o',
                        'active' => $c == 'lessons',
                        'url' => ['/student/lessons']
                    ],
                    [
                        'label' => 'Расписание',
                        'icon' => 'calendar',
                        'active' => $c == 'calendar',
                        'url' => ['/student/calendar'],
                    ],
                    [
                        'label' => 'Личные данные',
                        'icon' => 'user',
                        'active' => $c == 'profile',
                        'url' => ['/student/profile']
                    ],
                    [
                        'label' => 'Сообщение администратору',
                        'icon' => 'comments',
                        'active' => $c == 'chats',
                        'url' => ['/student/chats'],
                        'template' => $m ? '<a href="{url}"><span class="pull-left-container"><small class="label pull-left bg-green">'. Yii::$app->controller->module->unreadCount .'</small></span>{label}</a>' : '<a href="{url}">{icon} {label}</a>'
                    ],
                    [
                        'label' => 'Витрина',
                        'icon' => 'shopping-cart',
                        'active' => $c == 'store',
                        'url' => ['/student/store']
                    ],
                    [
                        'label' => 'Выход',
                        'icon' => 'sign-out',
                        'url' => ['/logout']
                    ]
                ],
            ]
        ) ?>

    </section>

</aside>
