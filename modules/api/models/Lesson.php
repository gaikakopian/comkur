<?php
/**
 * Created by PhpStorm.
 * User: zhek
 * Date: 30.08.17
 * Time: 14:39
 */

namespace app\modules\api\models;

use yii\base\Model;
use yii\db\ActiveQuery;

class Lesson extends Model
{

    /**
     * @param array $conditions
     *
     * @return ActiveQuery
     */
    public static function find($conditions = [])
    {
        return \app\models\Lesson::find()
            ->select(['id', 'name', 'course_id'])
            ->andFilterWhere($conditions);
    }

    /**
     * @param array $conditions
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function findAll($conditions = [])
    {
        return static::find($conditions)->all();
    }
}