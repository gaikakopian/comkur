<?php
/**
 * Created by PhpStorm.
 * User: zhek
 * Date: 30.08.17
 * Time: 14:39
 */

namespace app\modules\api\models;

use app\models\User as UserModel;
use yii\base\Model;
use yii\db\ActiveQuery;

class User extends Model
{

    public $id;
    public $email;
    public $name;
    public $skype;
    public $phone;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['email', 'name'], 'required'],
            [['email'], 'email'],
            [['name', 'skype', 'phone'], 'string', 'max' => 255],
            [['part'], 'integer'],
            [['notify'], 'boolean']
        ];
    }

    /**
     * @param array $conditions
     *
     * @return ActiveQuery
     */
    public static function find($conditions = [])
    {
        return UserModel::find()
            ->select(['id', 'username', 'name', 'skype', 'phone_number'])
            ->joinWith(['roles'])
            ->andFilterWhere($conditions);
    }

    /**
     * @param array $conditions
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function findAll($conditions = [])
    {
        return static::find($conditions)->all();
    }
}