<?php
/**
 * Created by PhpStorm.
 * User: zhek
 * Date: 30.08.17
 * Time: 14:39
 */

namespace app\modules\api\models;

use app\helpers\Constants;
use app\models\User;
use yii\base\Model;
use yii\db\ActiveQuery;

class Student extends Model
{

    public $email;
    public $name;
    public $skype;
    public $phone;

    public $courseId = null;
    public $part = null;
    public $notify = false;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['email', 'name', 'courseId'], 'required'],
            [['email'], 'email'],
            [['name', 'skype', 'phone'], 'string', 'max' => 255],
            [['part'], 'integer'],
            [['notify'], 'boolean']
        ];
    }

    /**
     * @param array $conditions
     *
     * @return ActiveQuery
     */
    public static function find($conditions = [])
    {
        return User::find()
            ->select(['id', 'username', 'name', 'skype', 'phone_number'])
            ->joinWith(['roles'])
            ->where(['item_name' => Constants::RBAC_STUDENT])
            ->andFilterWhere($conditions);
    }

    /**
     * @param array $conditions
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function findAll($conditions = [])
    {
        return static::find($conditions)->all();
    }
}