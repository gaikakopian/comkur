<?php

namespace app\modules\api\controllers;

use app\helpers\Constants;
use app\modules\api\models\User;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController as Controller;

/**
 * Users controller for the `api` module
 */
class UsersController extends Controller
{

    public $modelClass = 'app\modules\api\models\User';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => [Constants::RBAC_DIRECTOR, Constants::RBAC_ADMIN]
                ],
            ]
        ];

        return $behaviors;
    }

    /**
     * @return array
     */
    protected function verbs()
    {
        return [
            'create' => ['POST'],
            'view' => ['GET'],
            'index' => ['GET'],

        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset($actions['delete'], $actions['create'], $actions['update']);

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    /**
     * Prepare data provider for index action
     */
    public function prepareDataProvider()
    {

        $query = User::find();

        $name = \Yii::$app->request->get('name');

        if($name)
            $query->andFilterWhere(['like', 'name', $name]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);
    }
}
