<?php

namespace app\modules\api\controllers;

use app\modules\api\models\Course;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController as Controller;

/**
 * Courses controller for the `api` module
 */
class CoursesController extends Controller
{

    public $modelClass = 'app\modules\api\models\Course';

    /*
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => [Constants::RBAC_DIRECTOR, Constants::RBAC_ADMIN]
                ],
            ]
        ];

        return $behaviors;
    }
    */

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['delete'], $actions['create'], $actions['update']);

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    /**
     *
     */
    public function prepareDataProvider()
    {

        $query = Course::find();

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);
    }
}
