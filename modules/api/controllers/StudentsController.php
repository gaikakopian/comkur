<?php

namespace app\modules\api\controllers;

use app\helpers\Constants;
use app\helpers\Mailer;
use app\models\Goods;
use app\models\Course;
use app\models\CourseType;
use app\models\LessonStatus;
use app\models\StudentCourse;
use app\models\StudentLesson;
use app\models\User;
use app\models\UserStatus;
use app\modules\api\models\Student;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\Url;
use yii\rest\ActiveController as Controller;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\helpers\Sort;

/**
 * Students controller for the `api` module
 */
class StudentsController extends Controller
{

    public $modelClass = 'app\modules\api\models\Student';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => [Constants::RBAC_DIRECTOR]
                ],
            ]
        ];

        return $behaviors;
    }

    /**
     * @return array
     */
    protected function verbs()
    {
        return [
            'create' => ['POST'],
            'view' => ['GET'],
            'index' => ['GET'],

        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset($actions['delete'], $actions['create'], $actions['update']);

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    /**
     * Prepare data provider for index action
     */
    public function prepareDataProvider()
    {

        $query = Student::find();

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);
    }

    /**
     * Create new student
     *
     * @return array
     *
     * @throws Exception
     */
    public function actionCreate()
    {
        $model = new Student();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $response = \Yii::$app->getResponse();

        $model->load(\Yii::$app->request->getBodyParams(), '');

        if(User::findOne(['username' => $model->email]))
            $model->addError('username', 'Данный емейл уже зарегистрирован');

        if (!$model->hasErrors() && $model->validate()) {

            $transaction = User::getDb()->beginTransaction();

            try {

                $user = new User();

                $user->username = $model->email;
                $user->skype = $model->skype;
                $user->name = $model->name;

                $random = \Yii::$app->security->generateRandomString(8);

                $user->generateAuthKey();
                $user->setPassword($random);

                $user->status_id = UserStatus::STATUS_NEW;

                if($model->notify)
                    $user->status_id = UserStatus::STATUS_ACTIVE;

                if ($user->save()) {

                    $role = \Yii::$app->authManager->getRole(Constants::RBAC_STUDENT);
                    \Yii::$app->authManager->assign($role, $user->id);

                    $course = new StudentCourse();

                    $course->user_id = $user->id;
                    $course->course_id = $model->courseId;
                    $course->access_for_part = $model->part;

                    $course->save();

                    foreach ($course->course->lessons as $courseLesson) {

                        $lesson = new StudentLesson();

                        $lesson->lesson_id = $courseLesson->id;
                        $lesson->course_id = $model->courseId;
                        $lesson->parent_id = $course->id;
                        $lesson->user_id = $user->id;
                        $lesson->status_id = LessonStatus::STATUS_PLANNED;

                        // Set availability for demo courses

                        if($course->course->type_id == CourseType::TYPE_DEMO)
                            $lesson->status_id = LessonStatus::STATUS_AVAILABLE;

                        $lesson->save();
                    }

                    if ($model->notify) {

                        Mailer::send('user-register', $user->id, [
                            '{login}' => $user->username,
                            '{name}' => $user->name,
                            '{password}' => $random,
                            '{link}' => Url::toRoute(['/login'], true)
                        ]);

                        /** @var User[] $teachers */
                        $teachers = User::find()
                            ->joinWith(['roles'])
                            ->where(['item_name' => Constants::RBAC_TEACHER])
                            ->andWhere(['status_id' => UserStatus::STATUS_ACTIVE])
                            ->all();

                        /** @var User $teacher */
                        foreach($teachers as $teacher) {

                            if($course->course->type_id == CourseType::TYPE_REGULAR) {

                                Mailer::send('new-student', $teacher->id, [
                                    '{name}' => $teacher->name,
                                    '{student}' => $user->name,
                                    '{course}' => $course->course->name,
                                    '{link}' => Url::toRoute(['/teacher/students/index?type=new'], true)
                                ]);

                            } else {

                                Mailer::send('new-student', $teacher->id, [
                                    '{name}' => $teacher->name,
                                    '{student}' => $user->name,
                                    '{course}' => $course->course->name,
                                    '{link}' => Url::toRoute(['/teacher/results'], true)
                                ]);

                            }
                        }
                    }

                    /** @var User[] $teachers */
                    $directors = User::find()
                        ->joinWith(['roles'])
                        ->where(['item_name' => Constants::RBAC_DIRECTOR])
                        ->andWhere(['status_id' => UserStatus::STATUS_ACTIVE])
                        ->all();

                    /** @var User $director */
                    foreach($directors as $director) {

                        Mailer::send('new-director', $director->id, [
                            '{name}' => $director->name,
                            '{student}' => $user->name,
                            '{course}' => $course->course->name,
                            '{link}' => Url::toRoute(['/teacher/results'], true)
                        ]);
                    }


                } else {
                    return ['status' => 'error', 'messages' => $user->getErrors()];
                }

                $transaction->commit();

                $response->setStatusCode(201);

                return ['status' => 'ok', 'id' => $user->id];

            } catch (Exception $ex) {

                \Yii::error($ex->getMessage());
                $transaction->rollBack();

                throw $ex;
            }

        } else {

            $response->setStatusCode(500);

            return ['status' => 'error', 'messages' => $model->getErrors()];
        }
    }

    /**
     * Append course to student by id
     *
     * @return array
     *
     * @throws BadRequestHttpException
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionAppendCourse()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $response = \Yii::$app->getResponse();
        $request = \Yii::$app->request->getBodyParams();

        if (isset($request['studentId']) && isset($request['courseId'])) {

            $student = User::findOne($request['studentId']);

            if (!$student || !$student->hasRole(Constants::RBAC_STUDENT))
                throw new NotFoundHttpException('Student not found');

            $baseCourse = Course::findOne($request['courseId']);

            if (!$baseCourse)
                throw new NotFoundHttpException('Course not found');

            $course = StudentCourse::findOne(['user_id' => $student->id, 'course_id' => $baseCourse->id]);

            if ($course)
                throw new ForbiddenHttpException('Student already has specified course');

            $course = new StudentCourse();

            $course->user_id = $student->id;
            $course->course_id = $baseCourse->id;
            $course->access_for_part = isset($request['part']) ? $request['part'] : null;
            $course->access_stage = isset($request['part']) ? 1 : null;

            if (!isset($request['part'])){
                $good = Goods::findGood($baseCourse->id);
            } else{
                $good = Goods::findGood($baseCourse->id , 1 , $request['part']);
            }

            if (!$good){
                $good = new Goods();

                $good->course_id = $baseCourse->id;
                $good->title = $baseCourse->name;
                $good->short_descr = $baseCourse->description;
                $good->type = isset($request['part']) ? 'stage_part' : 'all';
                $good->stage = isset($request['part']) ? 1 : null;
                $good->part = isset($request['part']) ? 1 : null;
                $good->priority = Sort::GetNextPriority(Goods::className());
                $good->price = 1;

                $good->save(false);
            }

            $course->good_id = $good->id;    

            $course->save();

            foreach ($course->course->lessons as $courseLesson) {

                $lesson = new StudentLesson();

                $lesson->lesson_id = $courseLesson->id;
                $lesson->course_id = $baseCourse->id;
                $lesson->parent_id = $course->id;
                $lesson->user_id = $student->id;
                $lesson->status_id = LessonStatus::STATUS_PLANNED;

                // Set availability for demo courses

                if($course->course->type_id == CourseType::TYPE_DEMO)
                    $lesson->status_id = LessonStatus::STATUS_AVAILABLE;

                $lesson->save();
            }

            $response->setStatusCode(201);

            return ['status' => 'ok'];
        }

        throw new BadRequestHttpException('Specify studentId and courseId parameters');
    }
}
