<?php

namespace app\modules\api\controllers;

use app\helpers\Constants;
use app\helpers\Mailer;
use app\models\Course;
use app\models\CourseType;
use app\models\Lesson;
use app\models\LessonStatus;
use app\models\StudentCourse;
use app\models\StudentLesson;
use app\models\User;
use app\models\UserStatus;
use app\modules\api\models\Student;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\Url;
use yii\rest\ActiveController as Controller;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Students controller for the `api` module
 */
class LessonsController extends Controller
{

    public $modelClass = 'app\modules\api\models\Lesson';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => [Constants::RBAC_DIRECTOR, Constants::RBAC_TEACHER]
                ],
            ]
        ];

        return $behaviors;
    }

    /**
     * @return array
     */
    protected function verbs()
    {
        return [
            'create' => ['POST'],
            'view' => ['GET'],
            'index' => ['GET'],

        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset($actions['delete'], $actions['create'], $actions['update']);

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    /**
     * Prepare data provider for index action
     */
    public function prepareDataProvider()
    {

        $query = Lesson::find();

        $params = \Yii::$app->request->getQueryParams();

        if(isset($params['access-token']))
            unset($params['access-token']);

        if(isset($params['_url']))
            unset($params['_url']);

        if(empty($params['course_id']))
            $params['course_id'] = null;

        $query->andFilterWhere($params);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);
    }
}
