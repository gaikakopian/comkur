<?php

namespace app\modules\account;

use app\models\Message;
use app\models\User;
use Yii;
use yii\db\Expression;
use yii\helpers\Url;
use app\helpers\Access;
/**
 * account module definition class
 */
class AccountModule extends \yii\base\Module
{
    public $unreadCount = 0;

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\account\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->layoutPath = "@app/modules/account/views/layouts";
        $this->viewPath = "@app/modules/account/views";
        $this->layout = 'main';

        $this->registerComponents();
    }

    /**
     * Override components settings
     */
    public function registerComponents(){
        \Yii::$app->setComponents([
            'assetManager' => [
                'bundles' => [
                    'dmstr\web\AdminLteAsset' => [
                        'skin' => 'skin-red'
                    ],
                ],
                'class' => 'yii\web\AssetManager'
            ],
        ]);
    }

    /**
     * Переопределение before action метода (выполнение перед каждым действием)
     *
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if(!parent::beforeAction($action)){
            return false;
        }

        Yii::$app->user->loginUrl = '/login';

        /* @var $user User */
        $user = !Yii::$app->user->isGuest ? Yii::$app->user->identity : null;

        if(!Yii::$app->user->isGuest){

            $user->last_online_at = new Expression('NOW()');
            $user->update();

            $ownCount = Message::find()
                ->joinWith('chat')
                ->where(['{{message}}.recipient_id' => $user->id])
                ->andWhere(['<', '{{chat}}.author_last_date', new Expression('{{message}}.create_date')])
                ->andWhere(['{{chat}}.author_id' => $user->id])
                ->andWhere(['<>', '{{message}}.author_id', $user->id])
                ->count();

            $twoCount = Message::find()
                ->joinWith('chat')
                ->where(['{{message}}.recipient_id' => null])
                ->andWhere(['<', '{{chat}}.recipient_last_date', new Expression('{{message}}.create_date')])
                ->andWhere(['{{chat}}.recipient_id' => null])
                ->andWhere(['<>', '{{message}}.author_id', $user->id])
                ->count();

            $this->unreadCount = $ownCount + $twoCount;
        }

        return true;
    }
}
