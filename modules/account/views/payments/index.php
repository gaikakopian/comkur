<?php

use kartik\grid\GridView;
use yii\helpers\Url;
use app\helpers\Access;
use yii\bootstrap\Html;
use app\helpers\Constants;
use yii\helpers\ArrayHelper;
use app\models\Goods;
use app\models\User;

/* @var $searchModel \app\models\PaymentsSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\GoodsController */
/* @var $user \app\models\User */

$controller = $this->context;
$user = Yii::$app->user->identity;
$status_array = ['pending' => 'В ожидании','success' => 'Успешно','canceled' => 'Отменено']; 

$this->title = 'Оплата';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
    ],
    [
        'label' => 'Товар',
        'attribute' => 'good_id',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\Goods */
            $good = Goods::findOne(['id' => $model->good_id]);
            return !empty($good->title) ? Html::a($good->title, Url::to(['/account/goods', 'GoodsSearch[id]' => $good->id])) : null;
        },
    ],
    [
        'attribute' => 'order_id',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
    ],
    [
        'attribute' => 'price',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'filter' => false
    ],
    [
        'attribute' => 'status',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'filter' => $status_array,
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            $status_array = ['pending' => 'В ожидании','success' => 'Успешно','canceled' => 'Отменено'];
            return !empty($status_array[$model->status]) ? $status_array[$model->status] : null;
        },
    ],
    [
        'label' => 'Пользователь',
        'attribute' => 'user_id',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\Goods */
            $user = User::findOne(['id' => $model->user_id]);
            return !empty($user->username) ? Html::a($user->username, Url::to(['/account/users', 'UserSearch[id]' => $user->id])) : null;
        },
    ],
    [
        'attribute' => 'created_at',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'filter' => false
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'contentOptions'=>['style'=>'width: 140px; text-align: center;'],
        'header' => 'Действия',
        'template' => '{buy-good}',
        'buttons' => [
            'buy-good' => function ($url,$model,$key) {
                /* @var $model \app\models\Payments */
                if ($model->status == 'pending'){
                    
                    return Html::a('<span class="btn btn-success">Подтвердить</span>', Url::to(['/account/payments/buy-success', 'id' => $model->id]), [
                                                'title' => 'Удалить',
                                                'class' => 'btn btn-sm btn-default',
                                                'data-method' => 'post',
                                                'data-confirm' => 'Вы уверены, что хотите одобрить оплату?',
                                            ]);
                }       
            }
        ]
    ]
];

?>

<?php Yii::$app->view->registerCssFile('/backend/js/imperavi-redactor/redactor.css'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/redactor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/lang/ru.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontsize/fontsize.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontcolor/fontcolor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fullscreen/fullscreen.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/table/table.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/video/video.js'); ?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                <?= GridView::widget([
                    'filterModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'pjax' => false,
                ]); ?>
            </div>
        </div>
    </div>
</div>
