<?php
use yii\bootstrap\ActiveForm;
use app\helpers\Constants;
use kartik\date\DatePicker;

/* @var $model \app\models\User*/
/* @var $this \yii\web\View */
/* @var $user \app\models\User */
/* @var $controller \app\modules\account\controllers\UsersController */

$controller = $this->context;
$user = Yii::$app->user->identity;

$model->birth_date = $model->birth_date ? date('d.m.Y', strtotime($model->birth_date)) : $model->birth_date;

?>

    <div class="modal-header">
        <h4 class="modal-title">Новый участник</h4>
    </div>

<?php $form = ActiveForm::begin([
    'id' => 'create-post-form',
    'options' => ['role' => 'form', 'method' => 'post', 'enctype' => 'multipart/form-data'],
    'enableClientValidation'=>false,
    'enableAjaxValidation' => true,
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}\n",
        //'labelOptions' => ['class' => 'col-lg-1 control-label'],
    ],
]); ?>

    <div class="modal-body">

        <?= $form->field($model, 'name')->textInput(); ?>

        <?= $form->field($model, 'username')->textInput(); ?>

        <?= $form->field($model, 'password')->passwordInput(); ?>

        <hr>

        <?= $form->field($model, 'phone_number')->textInput(); ?>

        <?= $form->field($model, 'skype')->textInput(); ?>

        <?= $form->field($model, 'birth_date')->widget(DatePicker::className(),[
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd.mm.yyyy'
            ]
        ]); ?>

        <hr />

        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'role')->dropDownList(\yii\helpers\ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description')); ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'status_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\UserStatus::find()->all(), 'id', 'name')); ?>
            </div>
        </div>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Отмена</button>
        <button type="submit" class="btn btn-success">Создать</button>
    </div>

<?php ActiveForm::end(); ?>