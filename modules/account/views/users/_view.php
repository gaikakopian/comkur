<?php
/**
 * Created by PhpStorm.
 * User: zhek
 * Date: 28.08.17
 * Time: 20:44
 */

use yii\widgets\DetailView;

/* @var $model \app\models\User */

echo DetailView::widget([
    'model' => $model,
    'options' => ['class' => 'table detail-view'],
    'template' => '<tr><td{contentOptions}><label>{label}</label><br />{value}</td></tr>',
    'attributes' => [
        'name',
        'username',
        'birth_date:date',
        'phone_number',
        'skype',
        'last_login_at',
        'status.name'
    ],
]);
