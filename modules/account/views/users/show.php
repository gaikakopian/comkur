<?php

use app\models\User;
use branchonline\lightbox\LightboxAsset;
use yii\helpers\Url;

/* @var $model User */
/* @var $this \yii\web\View */
/* @var $user User */

$this->registerAssetBundle(LightboxAsset::className());

$user = Yii::$app->user->identity;

$this->title = 'Просмотр участника';
$this->params['breadcrumbs'][] = ['label' => 'Участники', 'url' => Url::to(['/account/users/index'])];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">

        <div class="box box-primary">

            <div class="box-body">
                <?= $this->render('/users/_view', ['model' => $model]); ?>
            </div>

            <div class="box-footer">
                <?php if (Yii::$app->authManager->checkAccess($user->id, 'manageUsers')): ?>
                    <a href="<?php echo Url::to(['/account/chats/message', 'userId' => $model->id]); ?>" data-toggle="modal" data-target=".modal-main" class="btn btn-success">Отправить сообщение</a>
                    <a class="btn btn-primary" href="<?= Url::to(['/account/users/update', 'id' => $model->id]); ?>">Редактировать</a>
                <?php endif; ?>
                <a class="btn btn-default" href="<?= Url::to(['/account/users/index']); ?>">Назад к
                    списку</a>
            </div>
        </div>
    </div>
</div>