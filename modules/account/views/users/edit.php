<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\models\User;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use app\helpers\Constants;
use branchonline\lightbox\LightboxAsset;
use yii\web\JsExpression;

/* @var $model User */
/* @var $this \yii\web\View */
/* @var $user User */

$this->registerAssetBundle(LightboxAsset::className());

$user = Yii::$app->user->identity;

$this->title = $model->isNewRecord ? 'Добавление участника' : 'Редактирование участника';
$this->params['breadcrumbs'][] = ['label' => 'Участники', 'url' => Url::to(['/account/users/index'])];
$this->params['breadcrumbs'][] = $this->title;

$model->birth_date = $model->birth_date ? date('d.m.Y', strtotime($model->birth_date)) : $model->birth_date;

?>

<div class="row">
    <div class="col-md-12">

        <div class="box box-primary">
            <div class="box-header with-border"><h3 class="box-title">Учетная запись</h3></div>

            <?php $form = ActiveForm::begin([
                'id' => 'edit-users-form',
                'options' => ['role' => 'form', 'method' => 'post', 'enctype' => 'multipart/form-data'],
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'template' => "{label}\n{input}\n{error}\n",
                    //'labelOptions' => ['class' => 'col-lg-1 control-label'],
                ],
            ]); ?>


            <div class="box-body">
                <?php if(!$model->hasErrors() && Yii::$app->request->isPost): ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i>Сохранено</h4>
                        Изменения успешно внесены в базу данных
                    </div>
                <?php endif; ?>

                <?= $form->field($model, 'username')->textInput(); ?>

                <?= $form->field($model, 'password')->passwordInput(); ?>

                <?= $form->field($model, 'role')->dropDownList(\yii\helpers\ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description')); ?>

                <hr />

                <?= $form->field($model, 'name')->textInput(); ?>

                <?= $form->field($model, 'birth_date')->widget(DatePicker::className(),[
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy',
                    ],
                ]); ?>

                <?= $form->field($model, 'phone_number')->textInput(); ?>

                <?= $form->field($model, 'skype')->textInput(); ?>

                <?= $form->field($model, 'status_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\UserStatus::find()->all(), 'id', 'name')); ?>

                <?php if($model->hasRole(Constants::RBAC_STUDENT)): ?>

                    <?= $form->field($model, 'teacher_id')->widget(Select2::className(),[
                        'model' => $model,
                        'attribute' => 'teacher_id',
                        'initValueText' => !empty($model->teacher_id) ? $model->teacher->name : '',
                        'options' => ['placeholder' => 'Найти пользователя'],
                        'language' => Yii::$app->language,
                        'theme' => Select2::THEME_DEFAULT,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 2,
                            'language' => [
                                'noResults' => new JsExpression("function () { return 'Нет результатов'; }"),
                                'searching' => new JsExpression("function () { return 'Поиск...'; }"),
                                'inputTooShort' => new JsExpression("function(args) {return 'Ввдите больше символов...';}"),
                                'errorLoading' => new JsExpression("function () { return 'Ошибка'; }"),
                            ],
                            'ajax' => [
                                'url' => Url::to(['/account/users/ajax-search', 'role' => Constants::ROLE_TEACHER]),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(user) { return user.text; }'),
                            'templateSelection' => new JsExpression('function (user) { return user.text; }'),
                        ],
                    ]); ?>
                <?php endif; ?>
            </div>

            <div class="box-footer">
                <?= \yii\helpers\Html::submitButton('Сохранить', ['class' => 'btn btn-success']); ?>
                <?= \yii\helpers\Html::a('Назад к списку', Url::to(['/account/users/index']), ['role' => 'button', 'class' => 'btn btn-default']); ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

    </div>
</div>