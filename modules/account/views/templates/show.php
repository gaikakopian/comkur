<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\helpers\Constants;

/* @var $model \app\models\Course */
/* @var $this \yii\web\View */
/* @var $user \app\models\User */
/* @var $controller \app\modules\account\controllers\CoursesController */

$controller = $this->context;
$user = Yii::$app->user->identity;

$editorInit = "
    $('textarea.editor-area').redactor({
        minHeight : 180,
        maxHeight : 180,
        toolbarFixed : false,
        scroll : true,
        autoSize : false,
//        imageUpload: '".Url::to(['/site/upload'])."',
        plugins: ['fontsize','fontcolor','fullscreen','table','video'],
        lang : '".Yii::$app->language."'
    });";
Yii::$app->view->registerJs($editorInit,\yii\web\View::POS_END);
?>

    <div class="modal-header">
        <h4 class="modal-title"><?= $model->isNewRecord ? 'Добавить курс' : 'Изменить курс'; ?></h4>
    </div>

<?php $form = ActiveForm::begin([
    'id' => 'edit-form',
    'options' => ['role' => 'form', 'method' => 'post', 'enctype' => 'multipart/form-data'],
    'enableClientValidation'=>false,
    'enableAjaxValidation' => true,
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}\n",
        //'labelOptions' => ['class' => 'col-lg-1 control-label'],
    ],
]); ?>

    <div class="modal-body">
        <?= $form->field($model, 'name')->textInput(); ?>
        <?= $form->field($model, 'description')->textarea(['class' => 'editor-area form-control']); ?>
        <?= $form->field($model, 'type_id')->dropDownList([
            Constants::COURSE_TYPE_DEMO => 'Тест',
            Constants::COURSE_TYPE_REGULAR => 'Обычный'
        ]); ?>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Отмена</button>
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </div>

<?php ActiveForm::end(); ?>