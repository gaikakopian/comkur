<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\helpers\Constants;

/* @var $model \app\models\Template */
/* @var $this \yii\web\View */
/* @var $user \app\models\User */
/* @var $controller \app\modules\account\controllers\TemplatesController */

$controller = $this->context;
$user = Yii::$app->user->identity;

$editorInit = "
    $('textarea.editor-area').redactor({
        minHeight : 180,
        maxHeight : 180,
        toolbarFixed : false,
        scroll : true,
        autoSize : false,
        plugins: ['fontsize','fontcolor','fullscreen','table','video'],
        lang : '".Yii::$app->language."'
    });";
Yii::$app->view->registerJs($editorInit,\yii\web\View::POS_END);
?>

    <div class="modal-header">
        <h4 class="modal-title"><?= $model->isNewRecord ? 'Добавить шаблон' : 'Изменить шаблон'; ?></h4>
    </div>

<?php $form = ActiveForm::begin([
    'id' => 'edit-form',
    'options' => ['role' => 'form', 'method' => 'post'],
    'enableClientValidation'=>false,
    'enableAjaxValidation' => true,
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}\n",
    ],
]); ?>

    <div class="modal-body">
        <?= $form->field($model, 'code')->textInput(); ?>
        <?= $form->field($model, 'title')->textInput(); ?>
        <?= $form->field($model, 'content')->textarea(['class' => 'editor-area form-control']); ?>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Отмена</button>
        <button type="submit" class="btn btn-success">Сохранить</button>
    </div>

<?php ActiveForm::end(); ?>