<?php

use kartik\grid\GridView;
use yii\helpers\Url;
use app\helpers\Access;
use yii\bootstrap\Html;
use app\helpers\Constants;

/* @var $searchModel \app\models\CourseSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\CoursesController */
/* @var $user \app\models\User */

$controller = $this->context;
$user = Yii::$app->user->identity;

$this->title = 'Шаблоны';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'attribute' => 'id',
        'contentOptions' => ['style' => 'width:70px;'],
        'headerOptions' => [],
        'format' => 'raw',
        'value' => function ($model) {
            return $model->id;
        }
    ],
    [
        'attribute' => 'code',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],

    ],
    [
        'attribute' => 'title',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],

    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'contentOptions'=>['style'=>'width: 140px; text-align: center;'],
        'header' => 'Действия',
        'template' => '{update} &nbsp; {delete}',
        'buttons' => [
            'update' => function ($url,$model,$key) {
                /* @var $model \app\models\Course */
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['/account/templates/update', 'id' => $model->id]), ['title' => 'Изменить', 'data-target' => '.modal-main', 'data-toggle'=>'modal']);
            },
        ]
    ],
];

?>

<?php Yii::$app->view->registerCssFile('/backend/js/imperavi-redactor/redactor.css'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/redactor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/lang/ru.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontsize/fontsize.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontcolor/fontcolor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fullscreen/fullscreen.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/table/table.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/video/video.js'); ?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <a href="<?php echo Url::to(['/account/templates/create']); ?>" data-toggle="modal" data-target=".modal-main" class="btn btn-primary">Добавить</a>
            </div>
            <div class="box-body">
                <?= GridView::widget([
                    'filterModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'pjax' => false,
                ]); ?>
            </div>
        </div>
    </div>
</div>
