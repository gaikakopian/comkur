<?php

use kartik\grid\GridView;
use yii\helpers\Url;
use app\helpers\Access;
use app\helpers\Constants;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $searchModel \app\models\UserSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\UsersController */
/* @var $user \app\models\User */

$controller = $this->context;
$user = Yii::$app->user->identity;

$this->title = 'Педагоги';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
    ],
    [
        'attribute' => 'name',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\User */
            return \yii\helpers\Html::a($model->name, Url::to(['/account/teachers/show', 'id' => $model->id]));
        },
    ],
    ['attribute' => 'username'],
    [
        'attribute' => 'last_online_at',
        'filter' => \kartik\daterange\DateRangePicker::widget([
            'model' => $searchModel,
            'convertFormat' => true,
            'attribute' => 'last_online_at',
            'pluginOptions' => [
                'locale' => [
                    'format'=>'d.m.Y',
                    'separator'=>' - ',
                ],
            ],
        ]),
        'enableSorting' => true,
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\User */
            $date = DateTime::createFromFormat('Y-m-d H:i:s',$model->last_online_at);
            return !empty($date) ? $date->format('d.m.y H:i:s') : 'Нет данных';
        },
    ],
    [
        'attribute' => 'created_at',
        'filter' => \kartik\daterange\DateRangePicker::widget([
            'model' => $searchModel,
            'convertFormat' => true,
            'attribute' => 'created_at',
            'pluginOptions' => [
                'locale' => [
                    'format'=>'d.m.Y',
                    'separator'=>' - ',
                ],
            ],
        ]),
        'enableSorting' => true,
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\User */
            $date = DateTime::createFromFormat('Y-m-d H:i:s',$model->created_at);
            return !empty($model->created_at) ? $date->format('d.m.y H:i:s') : 'Нет данных';
        },
    ],
    [
        'label' => 'Статус',
        'attribute' => 'status_id',
        'filter' => \yii\helpers\ArrayHelper::map(\app\models\UserStatus::find()->all(), 'id', 'name'),
        'enableSorting' => true,
        'format' => 'raw',
        'value' => function($model) {
            return $model->status->name;
        }
    ],
    [
        'label' => 'Кол-во учеников',
        'format' => 'raw',
        'value' => function($model) {
            return sizeof($model->students);
        }
    ],
];

?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header"></div>
            <div class="box-body">
                <?= GridView::widget([
                    'filterModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'pjax' => false,
                ]); ?>
            </div>
        </div>
    </div>
</div>
