<?php

use yii\helpers\Url;
use app\helpers\Constants;

/* @var $model \app\models\User */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\UsersController */

$controller = $this->context;
?>
<thead>
<tr>
    <th>ФИО</th>
    <th>Действия</th>
</tr>
</thead>
<tbody>
<?php if(sizeof($model->students) > 0): ?>
    <?php foreach($model->students as $student): ?>
        <tr>
            <td>
                <?= \yii\helpers\Html::a($student->name, Url::to(['/account/teachers/student/', 'id' => $student->id])); ?>
            </td>
            <td class="action-column">
                <a href="<?= Url::to(['/account/users/delete-course', 'id' => $student->id]); ?>" data-ajax-reloader=".ajax-reloadable" title="Удалить" aria-label="Удалить" data-confirm-ajax="Удалить ?"><span class="glyphicon glyphicon-trash"></span></a>&nbsp;
            </td>
        </tr>
    <?php endforeach; ?>
<?php else: ?>
    <tr>
        <td colspan="2">Нет студентов</td>
    </tr>
<?php endif; ?>
</tbody>

