<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\models\User;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use app\helpers\Constants;
use branchonline\lightbox\LightboxAsset;
use yii\web\JsExpression;
use yii\widgets\DetailView;

/* @var $model User */
/* @var $this \yii\web\View */
/* @var $user User */

$this->registerAssetBundle(LightboxAsset::className());

$user = Yii::$app->user->identity;

$this->title = 'Просмотр педагога';
$this->params['breadcrumbs'][] = ['label' => 'Педагоги', 'url' => Url::to(['/account/teachers'])];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">

        <div class="box box-primary">

            <div class="box-body">
                <?= $this->render('/users/_view', ['model' => $model]); ?>
            </div>

            <div class="box-footer">
                <a class="btn btn-default" href="<?= Url::to(['/account/teachers/index']); ?>">Назад к списку</a>
            </div>
        </div>

        <?php if($model->hasRole(Constants::RBAC_TEACHER)): ?>
            <div class="box box-primary">
                <div class="box-header"><h3 class="box-title">Студенты</h3></div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover ajax-reloadable-student" data-reload-url="<?= Url::to(['/account/teachers/list-students', 'id' => $model->id]); ?>">
                        <?= $this->render('/teachers/_ajax_list_students', ['model' => $model]); ?>
                    </table>
                </div>
                <div class="box-footer">
                    <a href="<?= Url::to(['/account/teachers/append-student','id' => $model->id]); ?>" data-toggle="modal" data-target=".modal-main" class="btn btn-success">Добавить студента</a>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>