<?php

use app\helpers\Constants;
use app\models\User;
use branchonline\lightbox\LightboxAsset;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $model User */
/* @var $this \yii\web\View */
/* @var $user User */
/* @var $notices \app\models\TeacherNotice[] */

$this->registerAssetBundle(LightboxAsset::className());

$user = Yii::$app->user->identity;

$this->title = 'Просмотр ученика';
$this->params['breadcrumbs'][] = ['label' => 'Студенты', 'url' => Url::to(['/account/students'])];
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .box-comments .comment-text {
        margin-left: 5px;
    }
</style>

<div class="row">
    <div class="col-md-12">

        <div class="box box-primary">

            <div class="box-body">
                <?= $this->render('/users/_view', ['model' => $model]); ?>
            </div>

            <?php $form = ActiveForm::begin([
                'id' => 'student-active-form',
                'action' => Url::to(['/account/students/change-status']),
                'options' => ['role' => 'form', 'method' => 'post']
            ]); ?>

            <div class="box-footer">

                <div class="hidden" style="display: none">
                    <?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
                </div>

                <?php if ($model->status_id == \app\models\UserStatus::STATUS_NEW || $model->status_id == \app\models\UserStatus::STATUS_BLOCKED): ?>
                    <?= \yii\helpers\Html::submitButton('Активировать', ['class' => 'btn btn-primary', 'name' => 'action', 'value' => 'activate']); ?>
                <?php else: ?>
                    <?= \yii\helpers\Html::submitButton('Заблокировать', ['class' => 'btn btn-danger', 'name' => 'action', 'value' => 'block']); ?>
                <?php endif; ?>

                <a class="btn btn-default pull-right" href="<?= Url::to(['/account/students/index']); ?>">Назад к
                    списку</a>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

        <?php if ($model->hasRole(Constants::RBAC_STUDENT)): ?>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Преподаватель</h3>
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover ajax-reloadable-teacher"
                           data-reload-url="<?= Url::to(['/account/students/list-teachers', 'id' => $model->id]); ?>">
                        <?= $this->render('/students/_ajax_list_teachers', ['model' => $model]); ?>
                    </table>
                </div>

                <div class="box-footer">
                    <?php if (empty($model->teacher_id)): ?>
                        <a href="<?= Url::to(['/account/students/append-teacher', 'id' => $model->id]); ?>"
                           data-toggle="modal" data-target=".modal-main" class="btn btn-success">Добавить
                            преподавателя</a>
                    <?php else: ?>
                        <a href="<?= Url::to(['/account/students/append-teacher', 'id' => $model->id]); ?>"
                           data-toggle="modal" data-target=".modal-main" class="btn btn-primary">Изменить
                            преподавателя</a>
                    <?php endif; ?>
                </div>

            </div>
        <?php endif; ?>

        <?php if ($model->hasRole(Constants::RBAC_STUDENT)): ?>
            <div class="box box-primary">
                <div class="box-header"><h3 class="box-title">Доступные курсы</h3></div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover ajax-reloadable-course"
                           data-reload-url="<?= Url::to(['/account/students/list-courses', 'id' => $model->id]); ?>">
                        <?= $this->render('/teachers/_ajax_list_courses', ['coursesOfStudent' => $model->courseOfStudents]); ?>
                    </table>
                </div>
                <div class="box-footer">
                    <a href="<?= Url::to(['/account/students/append-course', 'id' => $model->id]); ?>"
                       data-toggle="modal" data-target=".modal-main" class="btn btn-success">Добавить курс</a>
                </div>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-md-12">
                <!-- Box Comment -->
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Заметки по ученику</h3>
                    </div>

                    <!-- /.box-body -->
                    <div class="box-footer box-comments">

                        <?php foreach ($notices as $comment): ?>

                            <div class="box-comment">
                                <div class="comment-text">
                              <span class="username">
                                <?= $comment->user->name; ?>
                                  <?php if ($comment->lesson_id): ?>
                                      [<?= $comment->lesson && $comment->lesson->lesson->course ? $comment->lesson->lesson->course->name . ' / ' : ''; ?><?= $comment->lesson->lesson->name; ?>]
                                  <?php endif; ?>
                                  <span class="text-muted pull-right"><?= $comment->create_date; ?></span>
                              </span><!-- /.username -->
                                    <?= $comment->content; ?>
                                </div>
                                <!-- /.comment-text -->
                            </div>
                            <!-- /.box-comment -->

                        <?php endforeach; ?>
                    </div>

                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>

        <!--
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Календарь занятий</h3>
                    </div>
                    <div class="box-body">
                        <?php echo $this->render('_ajax_calendar', ['url' => Url::to(['/account/teachers/events', ['studentId' => $model->id]])]); ?>
                    </div>
                </div>
            </div>
        </div>
        -->

    </div>
</div>