<?php
use app\models\User;
use app\helpers\Access;
?>

<?php /* @var $user User */ ?>
<?php $user = Yii::$app->user->identity; ?>

<aside class="main-sidebar">

    <section class="sidebar">
        <?php $c = Yii::$app->controller->id; ?>
        <?php $a = Yii::$app->controller->action->id; ?>
        <?php $m = Yii::$app->controller->module->unreadCount; ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Участники', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Все участники',
                        'icon' => 'users',
                        'active' => $c == 'users',
                        'visible' => Yii::$app->authManager->checkAccess($user->id, 'manageUsers'),
                        'url' => ['/account/users/index'],
                    ],
                    [
                        'label' => 'Ученики',
                        'icon' => 'graduation-cap',
                        'active' => $c == 'students',
                        'visible' => Yii::$app->authManager->checkAccess($user->id, 'manageStudents'),
                        'url' => ['/account/students'],
                    ],
                    [
                        'label' => 'Педагоги',
                        'icon' => 'user-secret',
                        'active' => $c == 'teachers',
                        'visible' => Yii::$app->authManager->checkAccess($user->id, 'manageTeachers'),
                        'url' => ['/account/teachers'],
                    ],
                    [
                        'label' => 'Учебный процесс',
                        'options' => ['class' => 'header'],
                        'visible' => Yii::$app->authManager->checkAccess($user->id, 'manageCourses')
                    ],
                    [
                        'label' => 'Курсы',
                        'icon' => 'th-large',
                        'active' => $c == 'courses',
                        'visible' => Yii::$app->authManager->checkAccess($user->id, 'manageCourses'),
                        'url' => ['/account/courses/index']
                    ],
                    [
                        'label' => 'Уроки',
                        'icon' => 'file-text-o',
                        'active' => $c == 'lessons',
                        'visible' => Yii::$app->authManager->checkAccess($user->id, 'manageLessons'),
                        'url' => ['/account/lessons/index']
                    ],
                    [
                        'label' => 'Каталог уроков',
                        'icon' => 'th-large',
                        'active' => $c == 'situative-lessons' || $c == 'themes',
                        'visible' => Yii::$app->authManager->checkAccess($user->id, 'manageLessons'),
                        'url' => ['/account/themes']
                    ],
                    [
                        'label' => 'Товары',
                        'icon' => 'th-large',
                        'active' => $c == 'situative-lessons' || $c == 'themes',
                        'visible' => Yii::$app->authManager->checkAccess($user->id, 'manageLessons'),
                        'url' => ['/account/goods']
                    ],
                    [
                        'label' => 'Оплаты',
                        'icon' => 'th-large',
                        'active' => $c == 'situative-lessons' || $c == 'themes',
                        'visible' => Yii::$app->authManager->checkAccess($user->id, 'manageLessons'),
                        'url' => ['/account/payments']
                    ],
                    [
                        'label' => 'Настройки',
                        'options' => ['class' => 'header'],
                        'visible' => Yii::$app->authManager->checkAccess($user->id, 'manageUsers')
                    ],
                    [
                        'label' => 'Шаблоны',
                        'icon' => 'file-text',
                        'active' => $c == 'templates',
                        'visible' => Yii::$app->authManager->checkAccess($user->id, 'manageUsers'),
                        'url' => ['/account/templates/index'],
                    ],
                    [
                        'label' => 'API',
                        'icon' => 'cogs',
                        'active' => $c == 'api',
                        'visible' => Yii::$app->authManager->checkAccess($user->id, 'manageUsers'),
                        'url' => ['/account/api/index'],
                    ],
                    ['label' => '', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Сообщения пользователей',
                        'icon' => 'comments',
                        'active' => $c == 'chats',
                        'url' => ['/account/chats'],
                        'template' => $m ? '<a href="{url}"><span class="pull-left-container"><small class="label pull-left bg-green">'. Yii::$app->controller->module->unreadCount .'</small></span>{label}</a>' : '<a href="{url}">{icon} {label}</a>'

                    ],
                    [
                        'label' => 'Выход',
                        'icon' => 'sign-out',
                        'url' => ['/logout']
                    ]
                ],
            ]
        ) ?>

    </section>

</aside>
