<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Course;

/* @var $model \app\models\Lesson*/
/* @var $this \yii\web\View */
/* @var $user \app\models\User */
/* @var $controller \app\modules\account\controllers\LessonsController */

$controller = $this->context;
$user = Yii::$app->user->identity;

$editorInit = "
    $('textarea.editor-area').redactor({
        minHeight : 180,
        maxHeight : 180,
        toolbarFixed : false,
        scroll : true,
        autoSize : false,
//        imageUpload: '".Url::to(['/site/upload'])."',
        plugins: ['fontsize','fontcolor','fullscreen','table','video'],
        lang : '".Yii::$app->language."'
    });";
Yii::$app->view->registerJs($editorInit,\yii\web\View::POS_END);
?>

    <div class="modal-header">
        <h4 class="modal-title"><?= $model->isNewRecord ? 'Добавить урок' : 'Изменить урок'; ?></h4>
    </div>

<?php $form = ActiveForm::begin([
    'id' => 'create-form',
    'options' => ['role' => 'form', 'method' => 'post', 'enctype' => 'multipart/form-data'],
    'enableClientValidation'=>false,
    'enableAjaxValidation' => true,
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}\n",
        //'labelOptions' => ['class' => 'col-lg-1 control-label'],
    ],
]); ?>

    <div class="modal-body">
        <?= $form->field($model, 'name')->textInput(); ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'course_id')->dropDownList(ArrayHelper::map(Course::find()->all(),'id','name')); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'course_stage')->dropDownList([1,2,3,4,5,6,7,8,9,10]); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'course_part')->dropDownList([1,2,3,4,5,6,7,8,9,10]); ?>
            </div>
        </div>
        <?= $form->field($model, 'description')->textarea(['class' => 'editor-area form-control']); ?>

        <hr />

        <?= $form->field($model, 'video_url')->textInput(); ?>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Отмена</button>
        <button type="submit" class="btn btn-success">Сохранить</button>
    </div>

<?php ActiveForm::end(); ?>