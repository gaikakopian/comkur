<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\models\User;
use branchonline\lightbox\LightboxAsset;
use yii\helpers\ArrayHelper;
use app\models\Course;
use app\models\Lesson;

/* @var $model User */
/* @var $this \yii\web\View */
/* @var $user User */
/* @var $model Lesson */

$this->registerAssetBundle(LightboxAsset::className());

$user = Yii::$app->user->identity;

$this->title = $model->isNewRecord ? 'Добавить урок' : 'Изменить урок';
$this->params['breadcrumbs'][] = ['label' => 'Участники', 'url' => Url::to(['/account/lessons/index'])];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php Yii::$app->view->registerCssFile('/backend/js/imperavi-redactor/redactor.css'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/redactor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/lang/ru.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontsize/fontsize.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontcolor/fontcolor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fullscreen/fullscreen.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/table/table.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/video/video.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/imagemanager/imagemanager.js'); ?>

<?php
$editorInit = "
    $('textarea.editor-area').redactor({
        minHeight : 180,
        maxHeight : 180,
        toolbarFixed : false,
        scroll : true,
        autoSize : false,
        plugins : ['imagemanager'],
        imageUpload: '".Url::to(['/account/main/upload'])."',
        // imagePosition: true,
        // imageResizable: true,
        // plugins: ['imagemanager','fontsize','fontcolor','fullscreen','table','video'],
        lang : '".Yii::$app->language."',
        imageUploadCallback: function(image, json)
        {
            console.log(image.parent());
            image.attr('src' , json.url);
            image.attr('data-image' , 'img'+json.id);
        },
    });";
Yii::$app->view->registerJs($editorInit,\yii\web\View::POS_END);
?>

<div class="row">
    <div class="col-md-12">

        <div class="box box-primary">

            <?php $form = ActiveForm::begin([
                'id' => 'edit-users-form',
                'options' => ['role' => 'form', 'method' => 'post', 'enctype' => 'multipart/form-data'],
                'enableClientValidation'=>false,
                'enableAjaxValidation' => true,
                'fieldConfig' => [
                    'template' => "{label}\n{input}\n{error}\n",
                    //'labelOptions' => ['class' => 'col-lg-1 control-label'],
                ],
            ]); ?>


            <div class="box-body">

                <?php if(Yii::$app->session->getFlash(Lesson::className()) == true): ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i>Сохранено</h4>
                        Изменения успешно внесены в базу данных
                    </div>
                <?php endif; ?>

                <?= $form->field($model, 'name')->textInput(); ?>
                <?= $form->field($model, 'course_id')->dropDownList(ArrayHelper::map(Course::find()->all(),'id','name'), ['prompt' => '']); ?>
                <?= $form->field($model, 'description')->textarea(['class' => 'editor-area form-control']); ?>

                <hr />

                <?= $form->field($model, 'video_url')->textInput(); ?>
            </div>

            <div class="box-footer">
                <?= \yii\helpers\Html::submitButton('Сохранить', ['class' => 'btn btn-success']); ?>

                <?php if($model->course_id): ?>
                    <?= \yii\helpers\Html::a('Назад к списку', Url::to(['/account/lessons', 'LessonSearch[course_id]' => $model->course_id]), ['role' => 'button', 'class' => 'btn btn-default']); ?>
                <?php else: ?>
                    <?= \yii\helpers\Html::a('Назад к списку', Url::to(['/account/lessons']), ['role' => 'button', 'class' => 'btn btn-default']); ?>
                <?php endif; ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
