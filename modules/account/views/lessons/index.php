<?php

use kartik\grid\GridView;
use yii\helpers\Url;
use app\helpers\Access;
use yii\bootstrap\Html;
use app\models\Course;
use yii\helpers\ArrayHelper;

/* @var $searchModel \app\models\LessonSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\CoursesController */
/* @var $user \app\models\User */

$controller = $this->context;
$user = Yii::$app->user->identity;

$this->title = 'Уроки';

if($searchModel->course)
    $this->title = 'Уроки курса ' . $searchModel->course->name;

$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
    ],
    [
        'attribute' => 'name',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
    ],
    [
        'attribute' => 'course_id',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'filter' => ArrayHelper::map(Course::find()->all(),'id','name'),
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\Lesson */
            return !empty($model->course_id) ? $model->course->name : null;
        },
    ],
    [
        'class' => \yii\grid\ActionColumn::className(),
        'contentOptions'=>['style'=>'width: 140px; text-align: center;'],
        'header' => 'Действия',
        'template' => '{update} &nbsp; {delete} &nbsp; {move-up} &nbsp; {move-down}',
        'buttons' => [
            'move-up' => function ($url,$model,$key) {
                /* @var $model \app\models\Lesson */
                return Html::a('<span class="glyphicon glyphicon-arrow-up"></span>', Url::to(['/account/lessons/move', 'id' => $model->id, 'dir' => 'up']), ['title' => 'Вверх']);
            },
            'move-down' => function ($url,$model,$key) {
                /* @var $model \app\models\Lesson */
                return Html::a('<span class="glyphicon glyphicon-arrow-down"></span>', Url::to(['/account/lessons/move', 'id' => $model->id, 'dir' => 'down']), ['title' => 'Вниз']);
            },
        ],
        'visibleButtons' => [
            'delete' => function ($model, $key, $index) use ($user) {/* @var $model \app\models\Lesson */ return Yii::$app->authManager->checkAccess($user->id, 'deleteLesson');},
            'update' => function ($model, $key, $index) use ($user) {/* @var $model \app\models\Lesson */ return Yii::$app->authManager->checkAccess($user->id, 'updateLesson');},
            'move-up' => function ($model, $key, $index) use ($user, $searchModel) {/* @var $model \app\models\Lesson */ return !empty($searchModel->course_id);},
            'move-down' => function ($model, $key, $index) use ($user, $searchModel) {/* @var $model \app\models\Lesson */ return !empty($searchModel->course_id);},
        ],
    ],
];

?>

<?php Yii::$app->view->registerCssFile('/backend/js/imperavi-redactor/redactor.css'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/redactor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/lang/ru.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontsize/fontsize.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontcolor/fontcolor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fullscreen/fullscreen.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/table/table.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/video/video.js'); ?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <?php if(Yii::$app->authManager->checkAccess($user->id, 'createLesson')): ?>
                    <a href="<?php echo Url::to(['/account/lessons/create?courseId=' . $searchModel->course_id]) ?>" data-toggle="modal" data-target=".modal-main" class="btn btn-primary">Добавить</a>
                <?php endif; ?>
            </div>
            <div class="box-body">
                <?= GridView::widget([
                    'filterModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'pjax' => false,
                ]); ?>
            </div>
        </div>
    </div>
</div>
