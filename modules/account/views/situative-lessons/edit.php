<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\models\User;
use branchonline\lightbox\LightboxAsset;
use yii\helpers\ArrayHelper;
use app\models\Lesson;
use app\models\LessonTheme;
use kartik\typeahead\Typeahead;

/* @var $model \app\models\Lesson */
/* @var $this \yii\web\View */
/* @var $user User */

$this->registerAssetBundle(LightboxAsset::className());

$user = Yii::$app->user->identity;

$this->title = $model->isNewRecord ? 'Новый урок' : 'Изменить урок';
$this->params['breadcrumbs'][] = ['label' => 'Каталог уроков', 'url' => Url::to(['/account/situative-lessons/index'])];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php Yii::$app->view->registerCssFile('/backend/js/imperavi-redactor/redactor.css'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/redactor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/lang/ru.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontsize/fontsize.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontcolor/fontcolor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fullscreen/fullscreen.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/table/table.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/video/video.js'); ?>

<?php
$editorInit = "
    $('textarea.editor-area').redactor({
        minHeight : 180,
        maxHeight : 180,
        toolbarFixed : false,
        scroll : true,
        autoSize : false,
//        imageUpload: '".Url::to(['/site/upload'])."',
        plugins: ['fontsize','fontcolor','fullscreen','table','video'],
        lang : '".Yii::$app->language."'
    });";
Yii::$app->view->registerJs($editorInit,\yii\web\View::POS_END);
?>

<div class="row">
    <div class="col-md-12">

        <div class="box box-primary">


            <?php $form = ActiveForm::begin([
                'id' => 'edit-users-form',
                'options' => ['role' => 'form', 'method' => 'post'],
                'enableClientValidation'=>false,
                'enableAjaxValidation' => true,
                'fieldConfig' => [
                    'template' => "{label}\n{input}\n{error}\n"
                ],
            ]); ?>


            <div class="box-body">
                <?php if(Yii::$app->session->getFlash(\app\models\Lesson::className()) == true): ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i>Сохранено</h4>
                        Изменения успешно внесены в базу данных
                    </div>
                <?php endif; ?>
                <?= $form->field($model, 'theme_name')->widget(Typeahead::classname(), [
                    'options' => ['placeholder' => 'Введите название'],
                    'pluginOptions' => ['highlight'=>true],
                    'dataset' => [
                        [
                            'local' => array_values(ArrayHelper::map(LessonTheme::find()->all(),'id','name')),
                            'limit' => 10
                        ]
                    ]
                ]); ?>
                <?= $form->field($model, 'name')->textInput(); ?>
                <?= $form->field($model, 'description')->textarea(['class' => 'editor-area form-control']); ?>

                <hr />

                <?= $form->field($model, 'video_url')->textInput(); ?>
            </div>

            <div class="box-footer">
                <?= \yii\helpers\Html::submitButton('Сохранить', ['class' => 'btn btn-success']); ?>
                <?php if($model->theme_id): ?>
                    <?= \yii\helpers\Html::a('Назад к списку', Url::to(['/account/situative-lessons', 'LessonSearch[theme_id]' => $model->theme_id]), ['role' => 'button', 'class' => 'btn btn-default pull-right']); ?>
                <?php else: ?>
                    <?= \yii\helpers\Html::a('Назад к списку', Url::to(['/account/situative-lessons/index']), ['role' => 'button', 'class' => 'btn btn-default pull-right']); ?>
                <?php endif; ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>