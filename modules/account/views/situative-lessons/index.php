<?php

use kartik\grid\GridView;
use yii\helpers\Url;
use app\helpers\Access;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use app\models\FreeLessonSearch;
use yii\helpers\ArrayHelper;

/* @var $searchModel \app\models\LessonSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\SituativeLessonsController */
/* @var $user \app\models\User */

$controller = $this->context;
$user = Yii::$app->user->identity;

$this->title = 'Каталог уроков';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
    ],
    [
        'attribute' => 'name',
        'enableSorting' => true,
        'format' => 'raw',
        'value' => function ($model) {
            $title = $model->name;
            return $title;
        }
    ],
    [
        'attribute' => 'theme_id',
        'enableSorting' => true,
        'filter' => ArrayHelper::map(\app\models\LessonTheme::find()->all(),'id','name'),
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\Lesson */
            return !empty($model->theme_id) ? $model->theme->name : null;
        },
    ],
    [
        'class' => \yii\grid\ActionColumn::className(),
        'contentOptions'=>['style'=>'width: 140px; text-align: center;'],
        'header' => 'Действия',
        'template' => '{update} &nbsp; {delete}',

        'visibleButtons' => [
            'delete' => function ($model, $key, $index) use ($user) {/* @var $model \app\models\Lesson */ return Yii::$app->authManager->checkAccess($user->id, 'deleteLesson');},
            'update' => function ($model, $key, $index) use ($user) {/* @var $model \app\models\Lesson */ return Yii::$app->authManager->checkAccess($user->id, 'updateLesson');},
            ],
    ],
];

?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <?php if(Yii::$app->authManager->checkAccess($user->id, 'createLesson')): ?>
                    <a href="<?php echo Url::to(['/account/situative-lessons/create', 'themeId' => $searchModel->theme_id]); ?>" data-toggle="modal" data-target=".modal-main" class="btn btn-primary">Добавить</a>
                <?php endif; ?>
            </div>
            <div class="box-body">
                <?= GridView::widget([
                    'filterModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'pjax' => false,
                ]); ?>
            </div>
            <div class="box-footer">
                <?= Html::a('Вернуться к темам', Url::to(['/account/themes']), ['class' => 'btn btn-default']); ?>
            </div>
        </div>
    </div>
</div>

