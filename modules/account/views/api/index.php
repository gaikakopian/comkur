<?php
/**
 * Created by PhpStorm.
 * User: zhek
 * Date: 30.08.17
 * Time: 16:57
 */

use app\models\User;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $model User */
/* @var $this \yii\web\View */
/* @var $user User */

$user = Yii::$app->user->identity;

$this->title = 'API';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Доступ к API</h3>
            </div>

            <?php $form = ActiveForm::begin([
                'id' => 'api-form',
                'options' => ['role' => 'form', 'method' => 'post'],
                'enableClientValidation'=>false,
                'fieldConfig' => [
                    'template' => "{label}\n{input}\n{error}\n",
                ],
            ]); ?>

            <div class="box-body">

                <div class="form-group">
                    <label>Хост</label>
                    <?= \yii\helpers\Html::textInput('api_host', Url::toRoute(['/api'], true), ['class' => 'form-control', 'readonly' => 'readonly']); ?>
                </div>


                <?= $form->field($user, 'auth_key')->label('Access-Token'); ?>

            </div>

            <div class="box-footer">
                <?= \yii\helpers\Html::submitButton('Сгенерировать', ['class' => 'btn btn-primary']); ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
