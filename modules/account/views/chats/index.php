<?php
/**
 * Created by PhpStorm.
 * User: zhek
 * Date: 31.08.17
 * Time: 14:25
 */

use kartik\grid\GridView;
use kartik\select2\Select2;
use kartik\typeahead\Typeahead;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\helpers\Access;
use yii\bootstrap\Html;
use app\helpers\Constants;
use yii\web\JsExpression;

/* @var $searchModel \app\models\ChatSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\ChatsController */
/* @var $user \app\models\User */
/* @var $model \app\models\Chat */

$controller = $this->context;
$user = Yii::$app->user->identity;

$this->title = 'Сообщения';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
    ],
    [
        'attribute' => 'title',
        'format' => 'raw',
        'value' => function($model) {
            return Html::a($model->title ? $model->title : '(без темы)', Url::to(['/account/chats/show', 'id' => $model->id]));
        }
    ],
    [
        'label' => 'Отправитель',
        'attribute' => 'author_id',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'value' => function($model) {
            return $model->author->name;
        }
    ],
    [
        'label' => 'Получатель',
        'attribute' => 'recipient_id',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'value' => function($model) {
            return $model->recipient_id ? $model->recipient->name : '(Администратор)';
        }
    ],
    'create_date:datetime',
    'update_date:datetime',
    [
        'header' => 'Кол-во сообщений',
        'enableSorting' => false,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\Chat */
            return (int) count($model->messages);
        },
    ]
];

?>

<?php $form = ActiveForm::begin([
    'id' => 'chat-form',
    'options' => ['role' => 'form', 'method' => 'post'],
    'enableClientValidation'=>false,
    'enableAjaxValidation' => true,
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}\n"
    ]
]); ?>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">
                    Новое сообщение
                </h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-5">

                        <?= $form->field($model, 'recipient_name')->widget(Select2::classname(), [
                            'options' => ['placeholder' => 'Введите имя получателя (получателей)', 'multiple' => true],
                            'language' => Yii::$app->language,
                            'theme' => Select2::THEME_DEFAULT,
                            'data' => ArrayHelper::map(\app\models\User::find()->all(), 'id', 'name'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); ?>

                        <div style="display: none">
                            <?= $form->field($model, 'recipient_id')->textInput(); ?>
                        </div>
                    </div>
                    <div class="col-sm-2 text-center text-bold">
                        <br />
                        или
                    </div>
                    <div class="col-sm-5">

                        <?= $form->field($model, 'recipient_group')->widget(Select2::classname(), [
                            'options' => ['placeholder' => 'Выберите группу получателей', 'multiple' => true],
                            'language' => Yii::$app->language,
                            'theme' => Select2::THEME_DEFAULT,
                            'data' => ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description'),
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); ?>
                        <?php // $form->field($model, 'recipient_group')->dropDownList(ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description'), ['prompt' => 'Индивидуальная отправка']); ?>
                    </div>
                </div>

                <?= $form->field($model, 'title')->textInput(); ?>
                <?= $form->field($model, 'message')->textarea(['class' => 'form-control', 'rows' => 4]); ?>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-success">Отправить</button>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    История сообщений
                </h3>
            </div>
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'pjax' => false,
                    'rowOptions' => function($chat) {
                        return ['class' => (\app\models\Chat::unreadCount($chat->id, Yii::$app->user->identity->getId(), true) > 0) ? 'bg-success text-bold' : ''];
                    }
                ]); ?>
            </div>
        </div>
    </div>
</div>
