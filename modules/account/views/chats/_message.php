<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\helpers\Constants;

/* @var $model \app\models\Message */
/* @var $this \yii\web\View */
/* @var $user \app\models\User */
/* @var $controller \app\modules\account\controllers\ChatsController */

$controller = $this->context;
$user = Yii::$app->user->identity;

?>

    <div class="modal-header">
        <h4 class="modal-title"><?= $model->isNewRecord ? 'Новое сообщение' : 'Изменение сообщение'; ?></h4>
    </div>

<?php $form = ActiveForm::begin([
    'id' => 'message-form',
    'options' => ['role' => 'form', 'method' => 'post'],
    'enableClientValidation'=>false,
    'enableAjaxValidation' => true,
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}\n",
    ],
]); ?>

    <div class="modal-body">
        <?= $form->field($model, 'recipient_name')->textInput(['readonly' => 'readonly']); ?>
        <?= $form->field($model, 'title')->textInput(); ?>
        <?= $form->field($model, 'message')->textarea(['rows' => 6, 'placeholder' => 'Введите текст сообщения']); ?>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Отмена</button>
        <button type="submit" class="btn btn-success">Отправить</button>
    </div>

<?php ActiveForm::end(); ?>