<?php

use kartik\grid\GridView;
use yii\helpers\Url;
use app\helpers\Access;
use yii\bootstrap\Html;
use app\helpers\Constants;

/* @var $searchModel \app\models\LessonThemeSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\ThemesController */
/* @var $user \app\models\User */

$controller = $this->context;
$user = Yii::$app->user->identity;

$this->title = 'Каталог уроков';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
    ],
    [
        'label' => 'Тема',
        'attribute' => 'name',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'value' => function ($model)
        {
            return Html::a($model->name, Url::to(['/account/situative-lessons', 'LessonSearch[theme_id]' => $model->id]));
        }

    ],
    [
        'header' => 'Кол-во уроков',
        'enableSorting' => false,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\Course */
            $count = (int) count($model->lessons);
            return $count;
        },
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'contentOptions'=>['style'=>'width: 140px; text-align: center;'],
        'header' => 'Действия',
        'template' => '{update} &nbsp; {delete}',
        'buttons' => [
            'update' => function ($url,$model,$key) {
                /* @var $model \app\models\Course */
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['/account/themes/update', 'id' => $model->id]), ['title' => 'Изменить', 'data-target' => '.modal-main', 'data-toggle'=>'modal']);
            },
        ],
        'visibleButtons' => [
            'delete' => function ($model, $key, $index) use ($user) {/* @var $model \app\models\Course */ return Yii::$app->authManager->checkAccess($user->id, 'deleteCourse');},
            'update' => function ($model, $key, $index) use ($user) {/* @var $model \app\models\Course */ return Yii::$app->authManager->checkAccess($user->id, 'updateCourse');},
        ],
    ],
];

?>

<?php Yii::$app->view->registerCssFile('/backend/js/imperavi-redactor/redactor.css'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/redactor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/lang/ru.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontsize/fontsize.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontcolor/fontcolor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fullscreen/fullscreen.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/table/table.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/video/video.js'); ?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <?php if(Yii::$app->authManager->checkAccess($user->id, 'createCourse')): ?>
                    <a href="<?php echo Url::to(['/account/themes/create']); ?>" data-toggle="modal" data-target=".modal-main" class="btn btn-primary">Добавить тему</a>
                <?php endif; ?>
            </div>
            <div class="box-body">
                <?= GridView::widget([
                    'filterModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'pjax' => false,
                ]); ?>
            </div>
        </div>
    </div>
</div>
