<?php

use kartik\grid\GridView;
use yii\helpers\Url;
use app\helpers\Access;
use app\helpers\Constants;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $searchModel \app\models\UserSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\UsersController */
/* @var $user \app\models\User */

$controller = $this->context;
$user = Yii::$app->user->identity;

$this->title = 'Ученики';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
    ],
    [
        'attribute' => 'name',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\User */
            return \yii\helpers\Html::a($model->name, Url::to(['/account/students/show', 'id' => $model->id]));
        }
    ],
    ['attribute' => 'username'],
    [
        'attribute' => 'last_online_at',
        'filter' => \kartik\daterange\DateRangePicker::widget([
            'model' => $searchModel,
            'convertFormat' => true,
            'attribute' => 'last_online_at',
            'pluginOptions' => [
                'locale' => [
                    'format'=>'d.m.Y',
                    'separator'=>' - ',
                ],
            ],
        ]),
        'enableSorting' => true,
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\User */
            $date = DateTime::createFromFormat('Y-m-d H:i:s',$model->last_online_at);
            return !empty($date) ? $date->format('d.m.y H:i:s') : 'Нет данных';
        },
    ],

    [
        'attribute' => 'created_at',
        'filter' => \kartik\daterange\DateRangePicker::widget([
            'model' => $searchModel,
            'convertFormat' => true,
            'attribute' => 'created_at',
            'pluginOptions' => [
                'locale' => [
                    'format'=>'d.m.Y',
                    'separator'=>' - ',
                ],
            ],
        ]),
        'enableSorting' => true,
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\User */
            $date = DateTime::createFromFormat('Y-m-d H:i:s',$model->created_at);
            return !empty($model->created_at) ? $date->format('d.m.y H:i:s') : 'Нет данных';
        },
    ],
    [
        'label' => 'Статус',
        'attribute' => 'status_id',
        'filter' => \yii\helpers\ArrayHelper::map(\app\models\UserStatus::find()->all(), 'id', 'name'),
        'enableSorting' => true,
        'format' => 'raw',
        'value' => function($model) {
            return $model->status->name;
        }
    ],
    [
        'attribute' => 'teacher_id',
        'filter' => Select2::widget([
            'model' => $searchModel,
            'attribute' => 'teacher_id',
            'initValueText' => !empty($searchModel->teacher_id) ? $searchModel->teacher->name : '',
            'options' => ['placeholder' => 'Найти пользователя'],
            'language' => Yii::$app->language,
            'theme' => Select2::THEME_DEFAULT,
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 2,
                'language' => [
                    'noResults' => new JsExpression("function () { return 'Нет результатов'; }"),
                    'searching' => new JsExpression("function () { return 'Поиск...'; }"),
                    'inputTooShort' => new JsExpression("function(args) {return 'Введите больше символов...';}"),
                    'errorLoading' => new JsExpression("function () { return 'Ошибка'; }"),
                ],
                'ajax' => [
                    'url' => Url::to(['/account/users/ajax-search', 'role' => Constants::ROLE_TEACHER]),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(user) { return user.text; }'),
                'templateSelection' => new JsExpression('function (user) { return user.text; }'),
            ],
        ]),
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column) {
            /* @var $model \app\models\User */
            return !empty($model->teacher_id) ? \yii\helpers\Html::a($model->teacher->name, Url::to(['/account/teachers/show', 'id' => $model->teacher_id])) : null;
        },
    ],
];

?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <a href="<?php echo Url::to(['/account/students/export', 'format' => 'csv']); ?>" class="btn btn-primary">Экспорт CSV</a>
            </div>
            <div class="box-body">
                <?= GridView::widget([
                    'filterModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'pjax' => false,
                ]); ?>
            </div>
        </div>
    </div>
</div>
