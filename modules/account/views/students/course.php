<?php

use kartik\grid\GridView;
use app\models\Course;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $searchModel \app\models\StudentLessonSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\CoursesController */
/* @var $user \app\models\User */
/* @var $course \app\models\StudentCourse */


$controller = $this->context;
$user = Yii::$app->user->identity;

$this->title = 'Мои уроки';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'attribute' => 'id',
        'contentOptions' => ['style' => 'width:70px;'],
        'headerOptions' => [],
        'enableSorting' => true,
    ],
    [
        'attribute' => 'lesson.name',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'value' => function ($model) {
            return \yii\helpers\Html::a($model->lesson->name, \yii\helpers\Url::to(['/account/students/lesson/', 'id' => $model->id]));
        }
    ],
    [
        'attribute' => 'course_id',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'filter' => ArrayHelper::map(Course::find()->all(),'id','name'),
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\Lesson */
            return !empty($model->course_id) ? $model->course->name : null;
        },
    ],
    [
        'attribute' => 'status_id',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'filter' => ArrayHelper::map(\app\models\LessonStatus::find()->all(),'id','name'),
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\StudentLesson */
            return !empty($model->status_id) ? $model->status->name : null;
        },
    ]
];

?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header"></div>
            <div class="box-body">
                <?= GridView::widget([
                    'filterModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'pjax' => false,
                ]); ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Календарь занятий</h3>
            </div>
            <div class="box-body">
                <?php echo $this->render('_ajax_calendar', ['url' => Url::to(['/account/students/events', 'courseId' => $course->id])]); ?>
            </div>
        </div>
    </div>
</div>
