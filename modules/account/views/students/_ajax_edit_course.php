<?php

use app\helpers\Constants;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Course;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $model \app\models\StudentCourse */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\UsersController */
/* @var $owner \app\models\User */

$controller = $this->context;
?>

<div class="modal-header">
    <h4 class="modal-title">Настройки доступа к курсу для "<?= $owner->name; ?>"</h4>
</div>

<?php $form = ActiveForm::begin([
    'id' => 'append-course-form',
    'options' => ['role' => 'form', 'method' => 'post'],
    'enableClientValidation'=>false,
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}\n",
    ],
]); ?>

<div class="modal-body">
    <?php
    $alreadyAdded = array_values(ArrayHelper::map($owner->courseOfStudents,'id','course_id'));
    $alreadyAddedStr = !empty($alreadyAdded) ? implode(',',$alreadyAdded) : null;
    $availableCourses = Course::find();
    if(!empty($alreadyAddedStr) && $model->isNewRecord) {
        $availableCourses->andWhere(new \yii\db\Expression('id NOT IN ('.$alreadyAddedStr.')'));
    }
    $availableCourses = $availableCourses->all();
    ?>

    <?= $form->field($model, 'course_id')->dropDownList(ArrayHelper::map($availableCourses,'id','name')); ?>

    <?= $form->field($model, 'access_for_part')->dropDownList([0 => 'Полный доступ',1,2,3,4,5,6,7,8,9,10]); ?>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
    <button type="button" class="btn btn-primary submit-ajax-btn" data-ajax-form="#append-course-form" data-ok-reload=".ajax-reloadable-course">Сохранить</button>
</div>

<?php ActiveForm::end(); ?>
