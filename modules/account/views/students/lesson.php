<?php
/**
 * Created by PhpStorm.
 * User: zhek
 * Date: 28.08.17
 * Time: 13:29
 */

/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\CoursesController */
/* @var $model \app\models\StudentLesson */
/* @var $answer \app\models\StudentAnswer */
/* @var $review \app\models\TeacherReview */

$this->title = $model->lesson->name;

if($model->course)
    $this->params['breadcrumbs'][] = $model->lesson->course->name;

$this->params['breadcrumbs'][] = $model->lesson->name;
?>

<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">
            <?= $model->lesson->name; ?>
        </h3>
    </div>
    <div class="box-body">
        <?= $model->lesson->description; ?>
    </div>
    <div class="box-footer">
    </div>
</div>

<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Отчет педагогу по уроку от ученика <?= $model->user->name; ?></h3>
    </div>
    <div class="box-body">
        <?= $answer ? $answer->content : '-'; ?>
        <?php if($answer && $answer->video_url): ?>
            <hr />
            <?= \app\helpers\Help::parseVideoUrl($answer->video_url); ?>
        <?php endif; ?>
    </div>
</div>

<div class="box box-warning">
    <div class="box-header">
        <h3 class="box-title">Ответ педагога <?= $review ? $review->user->name : ''; ?></h3>
    </div>
    <div class="box-body">
        <?= $review ? $review->content : '-'; ?>
    </div>
</div>