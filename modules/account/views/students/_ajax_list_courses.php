<?php

use yii\helpers\Url;
use app\helpers\Constants;

/* @var $coursesOfStudent \app\models\StudentCourse[] */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\UsersController */

$controller = $this->context;
?>
<thead>
<tr>
    <th>Наименование курса</th>
    <th>Тип курса</th>
    <th>Доступ до части</th>
    <th>Действия</th>
</tr>
</thead>
<tbody>
<?php if(!empty($coursesOfStudent)): ?>
    <?php foreach($coursesOfStudent as $course): ?>
        <?php if(!$course->course) continue; ?>
        <tr>
            <td>
                <?= \yii\helpers\Html::a($course->course->name, Url::to(['/account/students/course', 'id' => $course->id])); ?>
            </td>
            <td>
                <?php $types = [
                    Constants::COURSE_TYPE_DEMO => 'Тест',
                    Constants::COURSE_TYPE_REGULAR => 'Обычный'
                ]; ?>
                
                <?= !empty($types[$course->course->type_id]) ? $types[$course->course->type_id] : null; ?>
            </td>
            <td>
                <?= $course->access_for_part ? $course->access_for_part : 'Полный доступ'; ?>
            </td>
            <td>
                <a href="<?= Url::to(['/account/students/edit-course', 'id' => $course->id]); ?>" data-ajax-reloader=".ajax-reloadable-course" title="Изменить" aria-label="Изменить" data-toggle="modal" data-target=".modal-main"><span class="glyphicon glyphicon-pencil"></span></a>
                <a href="<?= Url::to(['/account/students/delete-course', 'id' => $course->id]); ?>" data-ajax-reloader=".ajax-reloadable-course" title="Удалить" aria-label="Удалить" data-confirm-ajax="Удалить ?"><span class="glyphicon glyphicon-trash"></span></a>
            </td>
        </tr>
    <?php endforeach; ?>
<?php else: ?>
    <tr>
        <td colspan="5">Нет курсов</td>
    </tr>
<?php endif; ?>
</tbody>

