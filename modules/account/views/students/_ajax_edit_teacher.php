<?php

use app\helpers\Constants;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Course;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $model \app\models\StudentTeacher */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\UsersController */
/* @var $student \app\models\User */

$controller = $this->context;
?>

<div class="modal-header">
    <h4 class="modal-title">Выбор преподавателя для "<?= $student->name; ?>"</h4>
</div>

<?php $form = ActiveForm::begin([
    'id' => 'assign-teacher-form',
    'options' => ['role' => 'form', 'method' => 'post'],
    'enableClientValidation'=>false,
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}\n",
    ],
]); ?>

<div class="modal-body">
    <?php
    $availableTeachers = \app\models\User::find()
        ->joinWith(['roles'])
        ->where(['item_name' => Constants::RBAC_TEACHER])
        ->all();
    ?>

    <?= $form->field($model, 'teacher_id')->dropDownList(ArrayHelper::map($availableTeachers,'id','name'), ['prompt' => 'Без преподавателя']); ?>

</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
    <button type="button" class="btn btn-primary submit-ajax-btn" data-ajax-form="#assign-teacher-form" data-ok-reload=".ajax-reloadable-teacher">Сохранить</button>
</div>

<?php ActiveForm::end(); ?>
