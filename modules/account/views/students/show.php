<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\models\User;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use app\helpers\Constants;
use branchonline\lightbox\LightboxAsset;
use yii\web\JsExpression;
use yii\widgets\DetailView;

/* @var $model User */
/* @var $this \yii\web\View */
/* @var $user User */

$this->registerAssetBundle(LightboxAsset::className());

$user = Yii::$app->user->identity;

$this->title = 'Просмотр ученика';
$this->params['breadcrumbs'][] = ['label' => 'Студенты', 'url' => Url::to(['/account/students'])];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">

        <div class="box box-primary">

            <div class="box-body">
                <?= $this->render('/users/_view', ['model' => $model]); ?>
            </div>

            <?php $form = ActiveForm::begin([
                'id' => 'student-active-form',
                'action' => Url::to(['/account/students/change-status']),
                'options' => ['role' => 'form', 'method' => 'post']
            ]); ?>

            <div class="box-footer">

                <div class="hidden" style="display: none">
                    <?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
                </div>

                <?php if($model->status_id == \app\models\UserStatus::STATUS_NEW || $model->status_id == \app\models\UserStatus::STATUS_BLOCKED): ?>
                    <?= \yii\helpers\Html::submitButton('Активировать', ['class' => 'btn btn-primary', 'name' => 'action', 'value' => 'activate']); ?>
                <?php else: ?>
                    <?= \yii\helpers\Html::submitButton('Заблокировать', ['class' => 'btn btn-danger', 'name' => 'action', 'value' => 'block']); ?>
                <?php endif; ?>

                <a class="btn btn-default" href="<?= Url::to(['/account/students/index']); ?>">Назад к списку</a>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

        <?php if($model->hasRole(Constants::RBAC_STUDENT)): ?>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Преподаватель</h3>
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover ajax-reloadable-teacher" data-reload-url="<?= Url::to(['/account/students/list-teachers', 'id' => $model->id]); ?>">
                        <?= $this->render('/students/_ajax_list_teachers', ['model' => $model]); ?>
                    </table>
                </div>

                <div class="box-footer">
                    <?php if(empty($model->teacher_id)): ?>
                        <a href="<?= Url::to(['/account/students/append-teacher', 'id' => $model->id]); ?>" data-toggle="modal" data-target=".modal-main" class="btn btn-success">Добавить преподавателя</a>
                    <?php else: ?>
                        <a href="<?= Url::to(['/account/students/append-teacher', 'id' => $model->id]); ?>" data-toggle="modal" data-target=".modal-main" class="btn btn-primary">Изменить преподавателя</a>
                    <?php endif; ?>
                </div>

            </div>
        <?php endif; ?>

        <?php if($model->hasRole(Constants::RBAC_STUDENT)): ?>
            <div class="box box-primary">
                <div class="box-header"><h3 class="box-title">Доступные курсы</h3></div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover ajax-reloadable-course" data-reload-url="<?= Url::to(['/account/students/list-courses', 'id' => $model->id]); ?>">
                        <?= $this->render('/students/_ajax_list_courses', ['coursesOfStudent' => $model->courseOfStudents]); ?>
                    </table>
                </div>
                <div class="box-footer">
                    <a href="<?= Url::to(['/account/students/append-course','id' => $model->id]); ?>" data-toggle="modal" data-target=".modal-main" class="btn btn-success">Добавить курс</a>
                </div>
            </div>
        <?php endif; ?>

    </div>
</div>