<?php

use yii\helpers\Url;
use app\helpers\Constants;

/* @var $model \app\models\User */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\UsersController */

$controller = $this->context;
?>
<thead>
<tr>
    <th>ФИО</th>
    <th>Действия</th>
</tr>
</thead>
<tbody>
<?php if(sizeof($model->teachers) > 0): ?>
    <?php foreach($model->teachers as $teacher): ?>
        <tr>
            <td><?= $teacher->name; ?></td>
            <td class="action-column">
                <a href="<?= Url::to(['/account/users/delete-teacher', 'id' => $teacher->id]); ?>" data-ajax-reloader=".ajax-reloadable" title="Удалить" aria-label="Удалить" data-confirm-ajax="Удалить ?"><span class="glyphicon glyphicon-trash"></span></a>&nbsp;
            </td>
        </tr>
    <?php endforeach; ?>
<?php else: ?>
    <tr>
        <td colspan="2">Нет преподавателя</td>
    </tr>
<?php endif; ?>
</tbody>


