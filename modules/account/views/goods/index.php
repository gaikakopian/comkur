<?php

use kartik\grid\GridView;
use yii\helpers\Url;
use app\helpers\Access;
use yii\bootstrap\Html;
use app\helpers\Constants;
use yii\helpers\ArrayHelper;
use app\models\Course;

/* @var $searchModel \app\models\GoodsSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $this \yii\web\View */
/* @var $controller \app\modules\account\controllers\GoodsController */
/* @var $user \app\models\User */

$controller = $this->context;
$user = Yii::$app->user->identity;

$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
    ],
    [
        'label' => 'Товар',
        'attribute' => 'title',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw'

    ],
    [
        'attribute' => 'course_id',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'filter' => ArrayHelper::map(Course::find()->all(),'id','name'),
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\Goods */
            return !empty($model->course_id) ? Html::a($model->course->name, Url::to(['/account/lessons', 'LessonSearch[course_id]' => $model->course->id])) : null;
        },
    ],
    [
        'attribute' => 'type',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'filter' => ['all'=>'Целый курс','stage'=>'Ступень','all_part'=>'Часть курса','stage_part'=>'Часть ступени'],
        'format' => 'raw',
        'value' => function ($model, $key, $index, $column){

            $types = ['all'=>'Целый курс','stage'=>'Ступень','all_part'=>'Часть курса','stage_part'=>'Часть ступени'];
            return !empty($types[$model->type]) ? $types[$model->type] : null;
        },
    ],
    [
        'attribute' => 'stage',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'filter' => false,
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\Goods */
            return isset($model->stage) ? $model->stage + 1 : null;
        },
    ],
    [
        'attribute' => 'part',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'filter' => false,
        'value' => function ($model, $key, $index, $column){
            /* @var $model \app\models\Goods */
            return isset($model->part) ? $model->part + 1 : null;
        },
    ],
    [
        'attribute' => 'price',
        'enableSorting' => true,
        'contentOptions' => [],
        'headerOptions' => [],
        'format' => 'raw',
        'filter' => false
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'contentOptions'=>['style'=>'width: 140px; text-align: center;'],
        'header' => 'Действия',
        'template' => '{update} &nbsp; {delete} &nbsp; {move-up} &nbsp; {move-down}',
        'buttons' => [
            'update' => function ($url,$model,$key) {
                /* @var $model \app\models\Goods */
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['/account/goods/update', 'id' => $model->id]), ['title' => 'Изменить', 'data-target' => '.modal-main', 'data-toggle'=>'modal']);
            },
            'move-up' => function ($url,$model,$key) {
                /* @var $model \app\models\Goods */
                return Html::a('<span class="glyphicon glyphicon-arrow-up"></span>', Url::to(['/account/goods/move', 'id' => $model->id, 'dir' => 'up']), ['title' => 'Вверх']);
            },
            'move-down' => function ($url,$model,$key) {
                /* @var $model \app\models\Goods */
                return Html::a('<span class="glyphicon glyphicon-arrow-down"></span>', Url::to(['/account/goods/move', 'id' => $model->id, 'dir' => 'down']), ['title' => 'Вниз']);
            },
        ],
        'visibleButtons' => [
            'delete' => function ($model, $key, $index) use ($user) {/* @var $model \app\models\Goods */ return Yii::$app->authManager->checkAccess($user->id, 'deleteCourse');},
            'update' => function ($model, $key, $index) use ($user) {/* @var $model \app\models\Goods */ return Yii::$app->authManager->checkAccess($user->id, 'updateCourse');},
        ],
    ],
];

?>

<?php Yii::$app->view->registerCssFile('/backend/js/imperavi-redactor/redactor.css'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/redactor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/lang/ru.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontsize/fontsize.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fontcolor/fontcolor.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/fullscreen/fullscreen.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/table/table.js'); ?>
<?php Yii::$app->view->registerJsFile('/backend/js/imperavi-redactor/plugins/video/video.js'); ?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <?php if(Yii::$app->authManager->checkAccess($user->id, 'createCourse')): ?>
                    <a href="<?php echo Url::to(['/account/goods/create']); ?>" data-toggle="modal" data-target=".modal-main" class="btn btn-primary">Добавить товар</a>
                <?php endif; ?>
            </div>
            <div class="box-body">
                <?= GridView::widget([
                    'filterModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'pjax' => false,
                ]); ?>
            </div>
        </div>
    </div>
</div>
