<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use app\helpers\Constants;
use yii\helpers\ArrayHelper;
use app\models\Course;

/* @var $model \app\models\Goods */
/* @var $this \yii\web\View */
/* @var $user \app\models\User */
/* @var $controller \app\modules\account\controllers\GoodsController */

$controller = $this->context;
$user = Yii::$app->user->identity;

$editorInit = "
    $('textarea.editor-area').redactor({
        minHeight : 180,
        maxHeight : 180,
        toolbarFixed : false,
        scroll : true,
        autoSize : false,
        plugins: ['fontsize','fontcolor','fullscreen','table','video'],
        lang : '".Yii::$app->language."'
    });
    ";
Yii::$app->view->registerJs($editorInit,\yii\web\View::POS_END);
?>

<div class="modal-header">
    <h4 class="modal-title"><?= $model->isNewRecord ? 'Новый товар' : 'Изменить товар'; ?></h4>
</div>

<?php $form = ActiveForm::begin([
    'id' => 'edit-form',
    'options' => ['role' => 'form', 'method' => 'post'],
    'enableClientValidation'=>false,
    'enableAjaxValidation' => true,
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}\n",
    ],
]); ?>

     
    <div class="modal-body">
    <?php if ($model->isNewRecord) { ?>
        <?= $form->field($model, 'course_id')->dropDownList(ArrayHelper::map(Course::find()->where(['active' => '1'])->all(),'id','name'),[
            'prompt'=>'Выберите курс',
        ]); ?>
        
        <div id="good-stage" class="hidden-input">
            <?= $form->field($model, 'stage')->dropDownList([]); ?>
        </div>
        <div id="good-part" class="hidden-input">
            <?= $form->field($model, 'part')->dropDownList([]); ?>
        </div>
    <?php } else { ?>
        <?= $form->field($model, 'course_id')->dropDownList(ArrayHelper::map(Course::find()->where(['active' => '1'])->all(),'id','name'),[
            'prompt'=>'Выберите курс',
        ]); ?>
        
        <div id="good-stage" class="">
            <?= $form->field($model, 'stage')->dropDownList($stages); ?>
        </div>
        <div id="good-part" class="<?= count($parts) ? '' : 'hidden-input' ?>">
            <?= $form->field($model, 'part')->dropDownList($parts); ?>
        </div>
    <?php } ?>
        <?= $form->field($model, 'title')->textInput(); ?>
        <?= $form->field($model, 'short_descr')->textarea(['class' => 'editor-area form-control']); ?>
        <?= $form->field($model, 'imageFile')->fileInput() ?>
        <?php if (!$model->isNewRecord && isset($model->photo)) { ?>
            <img src="/backend/images/goods/<?= $model->photo ?>" width=200 height=200 >
        <?php } ?>
        <?= $form->field($model, 'price')->textInput(['type' => 'number']); ?>
    </div>
        
    <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Отмена</button>
        <button type="submit" class="btn btn-success">Сохранить</button>
    </div>

<?php ActiveForm::end(); ?>