<?php

namespace app\modules\account\controllers;

use app\helpers\Access;
use app\helpers\Constants;
use app\helpers\Help;
use app\models\StudentCourse;
use app\models\StudentLesson;
use app\models\StudentTeacher;
use app\models\UserSearch;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\db\Query;
use app\models\User;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\rbac\Role;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Котроллер отвечающий за управление пользователями
 *
 * @copyright 	2017 Alex Nem
 * @link 		https://github.com/darkoffalex
 * @author 		Alex Nem
 *
 * @package app\modules\account\controllers
 */
class UsersController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['director'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Show users list
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', compact('searchModel','dataProvider'));
    }

    /**
     * Создать нового пользователя
     * @return array|string
     */
    public function actionCreateAjax()
    {
        // новый пользователь
        $model = new User();

        // AJAX валидация

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        // если пришли данные из POST и они успешно заружены в объект

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            if(User::findOne(['username' => $model->username]))
                $model->addError('username', 'Данный емейл уже зарегистрирован');

            // если все данные в итоге корректны

            if(!$model->hasErrors() && $model->validate()){

                $model->setPassword($model->password);
                $model->generateAuthKey();

                //конвертировать дату
                if(!empty($model->birth_date)){
                    $model->birth_date = date('Y-m-d', strtotime($model->birth_date));
                }

                //базовые параметры
                $model->created_at = date('Y-m-d H:i:s',time());
                $model->updated_at = date('Y-m-d H:i:s',time());
                $model->created_by_id = Yii::$app->user->id;
                $model->updated_by_id = Yii::$app->user->id;

                if($model->role) {
                    switch ($model->role) {
                        case Constants::RBAC_ADMIN:
                            $model->role_id = Constants::ROLE_ADMIN;
                            break;
                        case Constants::RBAC_DIRECTOR:
                            $model->role_id = Constants::ROLE_DIRECTOR;
                            break;
                    }
                }

                if($model->save()) {

                    if(!empty($model->role)) {
                        $role = Yii::$app->authManager->getRole($model->role);
                        Yii::$app->authManager->assign($role, $model->id);
                    }

                    $this->redirect(Url::to(['/account/users/update', 'id' => $model->id]));
                }
                //если не удалось сохранить
                else{
                    //логировать ошибки валидации модели
                    Yii::info($model->getFirstErrors(),'info');
                    $this->redirect(Url::to(['/account/users/index', 'id' => $model->id]));
                }
            } else {
                //логировать ошибки валидации модели
                Yii::info($model->getFirstErrors(),'info');
                $this->redirect(Url::to(['/account/users/index']));
            }
        }

        //вывести форму редактирования
        return $this->renderAjax('_create',compact('model'));
    }

    /**
     * Show user
     *
     * @param $id
     *
     * @return string
     */
    public function actionShow($id)
    {
        $model = User::findOne($id);

        return $this->render('show', compact('model'));
    }

    /**
     * Обновление существующего пользователя
     * @param $id
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /* @var $model User */
        $model = User::findOne($id);

        /* @var $user User */
        $user = Yii::$app->user->identity;

        //если не найден
        //if(!Yii::$app->authManager->checkAccess(Yii::$app->user->getId(), 'editUser')){
        //    throw new NotFoundHttpException('Page not found',404);
        //}

        //AJAX валидация
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        //сохранить старый пароль
        $oldPassHash = $model->password_hash;
        $oldAuthKey = $model->auth_key;
        $oldTeacher = $model->teacher_id;

        //если POST
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            //если пароль был указан - использовать его
            if(!empty($model->password)){
                $model->setPassword($model->password);
                $model->generateAuthKey();

            }
            //если не был указан - использовать старый пароль
            else {
                $model->password_hash = $oldPassHash;
                $model->auth_key = $oldAuthKey;
            }

            if($model->validate()) {

                if(!empty($model->birth_date)){
                    $model->birth_date = date('Y-m-d', strtotime($model->birth_date));
                }

                $model->updated_at = date('Y-m-d H:i:s',time());
                $model->updated_by_id = Yii::$app->user->id;

                if(!empty($model->role)) {

                    $role = Yii::$app->authManager->getRole($model->role);

                    if($role) {
                        Yii::$app->authManager->revokeAll($model->id);
                        Yii::$app->authManager->assign($role, $model->id);
                    }
                }

                if(!empty($model->teacher_id)) {

                    $relation = StudentTeacher::findOne(['student_id' => $model->id, 'is_active' => 1]);

                    if(!$relation) {

                        $relation = new StudentTeacher();
                        $relation->student_id = $model->id;
                    }

                    if($relation && $relation->teacher_id != $oldTeacher) {
                        StudentTeacher::updateAll(['is_active' => 0], ['student_id' => $model->id, 'is_active' => 1]);
                    }

                    $relation->teacher_id = $model->teacher_id;
                    $relation->is_active = 1;

                    $relation->save();

                } elseif($model->hasRole(Constants::RBAC_STUDENT)) {

                    StudentTeacher::updateAll(['is_active' => 0], ['student_id' => $model->id, 'is_active' => 1]);
                }

                $model->update();
                $model->refresh();
            }

        }

        //отобразить форму
        return $this->render('edit',compact('model'));
    }

    /**
     * Удаление пользователя
     *
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete($id)
    {
        /* @var $model User */
        $model = User::findOne($id);

        if(!$model) {
            throw new NotFoundHttpException('Page not found',404);
        }

        $model->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * AJAX поиск пользователей для выпадающейго списка (по начальным символам имени)
     *
     * @param null $q
     * @param null $id
     * @param null $role
     * @return array
     */
    public function actionAjaxSearch($q = null, $id = null, $role = null)
    {
        //формат ответе - JSON
        Yii::$app->response->format = Response::FORMAT_JSON;

        //ответ по умолчанию
        $out = ['results' => ['id' => '', 'text' => '']];

        //если запрос не пуст
        if (!is_null($q)) {

            //сформировать запрос к базе
            $query = new Query();
            $query->select('id, name, username')->from('user');
            $query->where(['like','name',$q]);
            if(!empty($role)){
                $query->andWhere(['role_id' => (int)$role]);
            }
            $query->limit(20);

            //получить данные и сформировать ответ
            $command = $query->createCommand();
            $data = array_values($command->queryAll());
            $tmp = [];

            foreach($data as $index => $arr){
                $tmp[] = ['id' => $arr['id'], 'text' => $arr['name']];
            }

            $out['results'] = $tmp;
        }
        //если пуст запрос но указан ID
        elseif ($id > 0) {
            //найти по ID и сформировать ответ
            $user = User::findOne((int)$id);
            if(!empty($user)){
                $out['results'] = ['id' => $id, 'text' => $user->name];
            }
        }

        //вернуть ответ
        return $out;
    }


}