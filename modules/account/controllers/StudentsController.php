<?php

namespace app\modules\account\controllers;

use app\helpers\Access;
use app\helpers\Constants;
use app\helpers\Help;
use app\helpers\Mailer;
use app\models\LessonStatus;
use app\models\LessonType;
use app\models\StudentAnswer;
use app\models\StudentCourse;
use app\models\StudentLesson;
use app\models\StudentLessonSearch;
use app\models\StudentTeacher;
use app\models\TeacherReview;
use app\models\UserSearch;
use app\models\UserStatus;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\db\Query;
use app\models\User;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\rbac\Role;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Котроллер отвечающий за управление пользователями
 *
 * @copyright 	2017 Alex Nem
 * @link 		https://github.com/darkoffalex
 * @author 		Alex Nem
 *
 * @package app\modules\account\controllers
 */
class StudentsController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['director', 'admin'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Show students list
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();

        $searchModel->role = Constants::RBAC_STUDENT;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel','dataProvider'));
    }

    /**
     * Show user
     *
     * @param $id
     *
     * @return string
     */
    public function actionShow($id)
    {
        $model = User::findOne($id);

        return $this->render('show', compact('model'));
    }

    /**
     * Assign teacher to student
     *
     * @param $id
     * @return string
     *
     * @throws NotFoundHttpException
     */
    public function actionAppendTeacher($id)
    {
        /* @var $student User */
        $student = User::findOne($id);

        if(empty($student) || (!$student->hasRole(Constants::RBAC_STUDENT))){
            throw new NotFoundHttpException('Page not found',404);
        }

        $model = StudentTeacher::findOne(['student_id' => $student->id, 'is_active' => 1]);

        if(!$model) {
            $model = new StudentTeacher();
            $model->student_id = $student->id;
        }

        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            $model->is_active = 1;

            if($model->validate()) {

                $model->save();

                if($student->teacher_id != $model->teacher_id) {
                    $student->teacher_id = $model->teacher_id;
                    $student->save();
                }

                return 'OK';
            }
        }

        return $this->renderAjax('_ajax_edit_teacher',compact('model','student'));
    }

    /**
     * List student teachers
     *
     * @param $id
     * @return string
     *
     * @throws NotFoundHttpException
     */
    public function actionListTeachers($id)
    {
        /* @var $student User */
        $student = User::findOne($id);

        if(empty($student) || (!$student->hasRole(Constants::RBAC_STUDENT))){
            throw new NotFoundHttpException('Page not found',404);
        }

        return $this->renderAjax('_ajax_list_teachers', ['model' => $student]);
    }

    /**
     * Append course to student
     *
     * @param $id
     * @return string
     *
     * @throws NotFoundHttpException
     */
    public function actionAppendCourse($id)
    {
        /* @var $owner User */
        $owner = User::findOne($id);

        if(empty($owner) || (!$owner->hasRole(Constants::RBAC_STUDENT))) {
            throw new NotFoundHttpException('Page not found',404);
        }

        $model = new StudentCourse();

        if(Yii::$app->request->isPost){

            //загрузить данные в объект
            $model->load(Yii::$app->request->post());

            //чей курс
            $model->user_id = $owner->id;

            //если все данные в норме
            if($model->validate()){

                //начать транзакцию (если возникнет ошибка можно будет отменить все изменения в базе)
                $transaction = Yii::$app->db->beginTransaction();

                //попытка сохранения
                try{

                    if(!$model->save()){
                        throw new \Exception('Seems like form has errors');
                    }

                    if(!empty($model->course)) {

                        $index = 1;

                        foreach ($model->course->lessons as $lesson) {

                            $studentLesson = new StudentLesson();

                            $studentLesson->user_id = $id;
                            $studentLesson->course_id = $model->course_id;
                            $studentLesson->lesson_id = $lesson->id;
                            $studentLesson->priority = $lesson->priority;
                            $studentLesson->parent_id = $model->id;
                            $studentLesson->status_id = LessonStatus::DEFAULT_STATUS;

                            // Change status for first lesson

                            if($index == 1)
                                $studentLesson->status_id = LessonStatus::STATUS_AVAILABLE;

                            $studentLesson->save();

                            $index++;
                        }
                    }

                    $transaction->commit();
                }

                catch (\Exception $ex){
                    $transaction->rollBack();
                }

                return 'OK';
            }
        }

        return $this->renderAjax('_ajax_edit_course',compact('model','owner'));
    }

    /**
     * Список привязанных курсов студента
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionListCourses($id)
    {
        /* @var $owner User */
        $owner = User::findOne($id);

        if(empty($owner) || (!$owner->hasRole(Constants::RBAC_STUDENT))){
            throw new NotFoundHttpException('Page not found',404);
        }

        return $this->renderAjax('_ajax_list_courses', ['coursesOfStudent' => $owner->courseOfStudents]);
    }

    /**
     * Удалить привязку к курсу
     * @param $id int
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionDeleteCourse($id)
    {
        /* @var $model StudentCourse */
        $model = StudentCourse::find()->where(['id' => (int)$id])->one();

        if(empty($model)){
            throw new NotFoundHttpException('Page not found',404);
        }

        /* @var $owner User */
        $owner = $model->user;

        StudentLesson::deleteAll(['course_id' => $model->course_id, 'user_id' => $owner->id]);

        $model->delete();

        //вывести таблицу со связями после удаления
        return $this->renderPartial('_ajax_list_courses', ['coursesOfStudent' => $owner->courseOfStudents]);
    }

    /**
     * Редактирование привяки курса
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionEditCourse($id)
    {
        /* @var $model StudentCourse */
        $model = StudentCourse::findOne($id);

        //если не найден - 404
        if(empty($model)){
            throw new NotFoundHttpException('Page not found',404);
        }

        /* @var $owner User ученик к которому был привязан курс */
        $owner = $model->user;

        //если пришли данные из POST
        if(Yii::$app->request->isPost){

            //загрузить данные в объект
            $model->load(Yii::$app->request->post());

            //если все данные в норме
            if($model->validate()){

                //начать транзакцию (если возникнет ошибка можно будет отменить все изменения в базе)
                $transaction = Yii::$app->db->beginTransaction();

                //попытка сохранения
                try {

                    if(!$model->save()){
                        throw new \Exception('Seems like form has errors');
                    }

                    $transaction->commit();
                }
                catch (\Exception $ex){
                    $transaction->rollBack();
                }

                return 'OK';
            }
        }

        return $this->renderAjax('_ajax_edit_course',compact('model','owner'));
    }

    /**
     * Change student status
     */
    public function actionChangeStatus()
    {

        $params = Yii::$app->request->post('User');
        $action = Yii::$app->request->post('action');

        $user = User::findOne($params['id']);

        if($user && $action) {

            switch ($action) {

                case 'activate':

                    $user->status_id = UserStatus::STATUS_ACTIVE;

                    $random = Yii::$app->security->generateRandomString(8);

                    $user->setPassword($random);
                    $user->save();

                    Mailer::send('user-register', $user->id, [
                        '{name}' => $user->name,
                        '{password}' => $random,
                        '{link}' => Url::toRoute(['/login'], true)
                    ]);

                    break;

                case 'block':

                    $user->status_id = UserStatus::STATUS_BLOCKED;
                    $user->save();
                    break;
            }
        }

        $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Export students contacts
     */
    public function actionExport()
    {
        $format = Yii::$app->request->get('format', 'csv');

        switch ($format) {

            case 'csv':

                $models = User::find()
                    ->joinWith(['roles'])
                    ->andWhere(['item_name' => 'student'])
                    ->all();

                $rows = [['email', 'phone', 'name']];

                /** @var User $model */
                foreach($models as $model) {
                    $rows[] = ['"' . $model->username . '"', '"' . $model->phone_number . '"', '"' . $model->name . '"'];
                }

                $this->array_to_csv_download($rows, 'students.csv');

                break;
        }

    }

    /**
     * Export array to CSV file
     *
     * @param $array
     * @param string $filename
     * @param string $delimiter
     */
    private function array_to_csv_download($array, $filename = "export.csv", $delimiter=";") {
        // open raw memory as file so no temp files needed, you might run out of memory though
        $f = fopen('php://memory', 'w');
        // loop over the input array
        foreach ($array as $line) {
            // generate csv lines from the inner arrays
            fputcsv($f, $line, $delimiter);
        }
        // reset the file pointer to the start of the file
        fseek($f, 0);
        // tell the browser it's going to be a csv file
        header('Content-Type: application/csv');
        // tell the browser we want to save it instead of displaying it
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        // make php send the generated csv lines to the browser
        fpassthru($f);
    }

    /**
     * Show students course
     *
     * @param $id
     *
     * @return string
     */
    public function actionCourse($id)
    {
        $course = StudentCourse::findOne($id);

        $searchModel = new StudentLessonSearch();

        $searchModel->user_id = $course->user_id;
        $searchModel->type_id = LessonType::TYPE_OFFLINE;
        $searchModel->parent_id = $course->id;
        $searchModel->course_id = $course->course_id;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, null);

        return $this->render('course', compact('searchModel','dataProvider', 'course'));
    }

    /**
     * Show lesson
     *
     * @param $id
     *
     * @return string
     */
    public function actionLesson($id)
    {
        $model = StudentLesson::findOne($id);

        $answer = StudentAnswer::findOne(['lesson_id' => $model->lesson_id]);

        $review = TeacherReview::findOne(['lesson_id' => $model->id]);

        return $this->render('lesson', ['answer' => $answer, 'model' => $model, 'review' => $review]);
    }

    /**
     * Student's events
     *
     * @return array
     */
    public function actionEvents()
    {

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $courseId = Yii::$app->request->get('courseId');
        $studentId = Yii::$app->request->get('studentId');

        if($studentId) {
            $lessons = StudentLesson::findAll(['user_id' => $studentId, 'type_id' => LessonType::TYPE_ONLINE]);
        } elseif($courseId) {
            $lessons = StudentLesson::findAll(['parent_id' => $courseId, 'type_id' => LessonType::TYPE_ONLINE]);
        } else {
            return [];
        }

        $events = [];

        /** @var StudentLesson $lesson */
        foreach ($lessons as $lesson){

            $event = new \yii2fullcalendar\models\Event();

            $event->id = $lesson->id;
            $event->title = 'Скайп ' . ($lesson->user->teacher_id ? $lesson->user->teacher->name : '');

            $event->start = date('Y-m-d\TH:i:s\Z', strtotime($lesson->start_date));
            $event->end = date('Y-m-d\TH:i:s\Z', strtotime($lesson->end_date));

            $events[] = $event;
        }

        return $events;
    }

}
