<?php

namespace app\modules\account\controllers;

use app\models\Template;
use app\models\TemplateSearch;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * @package app\controllers
 */
class TemplatesController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['director', 'admin'],
                    ],
                ],
            ]
        ];
    }

    /**
     * List templates
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new TemplateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel', 'dataProvider'));
    }

    /**
     * Создать новый курс
     * @return array|string
     */
    public function actionCreate()
    {
        $model = new Template();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            if ($model->validate()) {

                $model->save();

                return $this->redirect(Url::to(['/account/templates/index']));
            }
        }

        return $this->renderAjax('_edit', compact('model'));
    }

    /**
     * Show course
     *
     * @param $id
     *
     * @return array|string|Response
     * @throws NotFoundHttpException
     */
    public function actionShow($id)
    {
        /* @var $model Template */
        $model = Template::find()->where(['id' => (int)$id])->one();

        if (empty($model)) {
            throw new NotFoundHttpException('Страница не найдена', 404);
        }

        return $this->render('show', compact('model'));
    }

    /**
     * Template update
     *
     * @param $id
     *
     * @return array|string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /* @var $model Template */
        $model = Template::findOne($id);

        if (empty($model)) {
            throw new NotFoundHttpException('Страница не найдена', 404);
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            //если все данный в итоге корректны
            if ($model->validate()) {

                $model->save();

                return $this->redirect(Url::to(['/account/templates/index']));
            }
        }

        return $this->renderAjax('_edit', compact('model'));
    }

    /**
     * Template delete
     *
     * @param $id
     *
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        /* @var $model Template */
        $model = Template::findOne($id);

        if (empty($model)) {
            throw new NotFoundHttpException('Страница не найдена', 404);
        }

        $model->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }
}
