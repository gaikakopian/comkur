<?php

namespace app\modules\account\controllers;

use app\helpers\Access;
use app\helpers\Constants;
use app\helpers\Help;
use app\models\LessonType;
use app\models\StudentAnswer;
use app\models\StudentCourse;
use app\models\StudentLesson;
use app\models\StudentLessonSearch;
use app\models\StudentTeacher;
use app\models\TeacherNotice;
use app\models\TeacherReview;
use app\models\UserSearch;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\db\Query;
use app\models\User;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\rbac\Role;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Котроллер отвечающий за управление пользователями
 *
 * @copyright 	2017 Alex Nem
 * @link 		https://github.com/darkoffalex
 * @author 		Alex Nem
 *
 * @package app\modules\account\controllers
 */
class TeachersController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['director', 'admin'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Show students list
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();

        $searchModel->role = Constants::RBAC_TEACHER;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel','dataProvider'));
    }

    /**
     * Show teacher profile
     *
     * @param $id
     *
     * @return string
     */
    public function actionShow($id)
    {
        $model = User::findOne($id);

        return $this->render('show', compact('model'));
    }

    /**
     * Assign student to teacher
     *
     * @param $id
     * @return string
     *
     * @throws NotFoundHttpException
     */
    public function actionAppendStudent($id)
    {
        /* @var $teacher User */
        $teacher = User::findOne($id);

        if(empty($teacher) || (!$teacher->hasRole(Constants::RBAC_TEACHER))){
            throw new NotFoundHttpException('Page not found',404);
        }

        $model = new StudentTeacher();
        $model->teacher_id = $teacher->id;

        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            $relation = StudentTeacher::findOne(['teacher_id' => $teacher->id, 'student_id' => $model->student_id, 'is_active' => 1]);

            if($relation)
                $model = $relation;

            $model->is_active = 1;

            if($model->validate()) {

                StudentTeacher::updateAll(['is_active' => 0], ['student_id' => $model->student_id]);

                $model->save();
                $model->refresh();

                if($model->student->teacher_id != $model->teacher_id) {
                    $model->student->teacher_id = $model->teacher_id;
                    $model->student->save();
                }

                return 'OK';
            }
        }

        return $this->renderAjax('_ajax_edit_student',compact('model','teacher'));
    }

    /**
     * List teacher students
     *
     * @param $id
     * @return string
     *
     * @throws NotFoundHttpException
     */
    public function actionListStudents($id)
    {
        /* @var $teacher User */
        $teacher = User::findOne($id);

        if(empty($teacher) || (!$teacher->hasRole(Constants::RBAC_TEACHER))){
            throw new NotFoundHttpException('Page not found',404);
        }

        return $this->renderAjax('_ajax_list_students', ['model' => $teacher]);
    }

    /**
     * Show user
     *
     * @param $id
     *
     * @return string
     */
    public function actionStudent($id)
    {
        $model = User::findOne($id);

        $notices = TeacherNotice::findAll(['student_id' => $model->id]);

        return $this->render('student', compact('model', 'notices'));
    }

    /**
     * Show students course
     *
     * @param $id
     *
     * @return string
     */
    public function actionCourse($id)
    {
        $course = StudentCourse::findOne($id);

        $searchModel = new StudentLessonSearch();

        $searchModel->user_id = $course->user_id;
        $searchModel->type_id = LessonType::TYPE_OFFLINE;
        $searchModel->parent_id = $course->id;
        $searchModel->course_id = $course->course_id;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, null);

        return $this->render('course', compact('searchModel','dataProvider', 'course'));
    }

    /**
     * Show lesson
     *
     * @param $id
     *
     * @return string
     */
    public function actionLesson($id)
    {
        $model = StudentLesson::findOne($id);

        $answer = StudentAnswer::findOne(['lesson_id' => $model->lesson_id]);

        $review = TeacherReview::findOne(['lesson_id' => $model->id]);

        return $this->render('lesson', ['answer' => $answer, 'model' => $model, 'review' => $review]);
    }

    /**
     * Teacher's events
     *
     * @return array
     */
    public function actionEvents()
    {

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $courseId = Yii::$app->request->get('courseId');
        $studentId = Yii::$app->request->get('studentId');

        if($studentId) {
            $lessons = StudentLesson::findAll(['user_id' => $studentId, 'type_id' => LessonType::TYPE_ONLINE]);
        } elseif($courseId) {
            $lessons = StudentLesson::findAll(['parent_id' => $courseId, 'type_id' => LessonType::TYPE_ONLINE]);
        } else {
            return [];
        }

        $events = [];

        /** @var StudentLesson $lesson */
        foreach ($lessons as $lesson){

            $event = new \yii2fullcalendar\models\Event();

            $event->id = $lesson->id;
            $event->title = 'Скайп ' . $lesson->user->name;

            $event->start = date('Y-m-d\TH:i:s\Z', strtotime($lesson->start_date));
            $event->end = date('Y-m-d\TH:i:s\Z', strtotime($lesson->end_date));

            $events[] = $event;
        }

        return $events;
    }
}
