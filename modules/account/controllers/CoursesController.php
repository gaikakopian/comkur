<?php

namespace app\modules\account\controllers;

use app\models\Course;
use app\models\CourseSearch;
use app\models\Goods;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\Controller;

/**
 * Котроллер отвечающий за управление курсами
 *
 * @copyright 	2017 Alex Nem
 * @link 		https://github.com/darkoffalex
 * @author 		Alex Nem
 *
 * @package app\controllers
 */
class CoursesController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['director'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Вывод списка курсов
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new CourseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', compact('searchModel','dataProvider'));
    }

    /**
     * Создать новый курс
     * @return array|string
     */
    public function actionCreate()
    {
        //новый курс
        $model = new Course();

        //AJAX валидация
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        //если пришли данные из POST и они успешно заружены в объект
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            //если все данный в итоге корректны
            if($model->validate()){

                $model->save();

                //к списку
                return $this->redirect(Url::to(['/account/courses/index']));
            }
        }

        //вывести форму редактирования
        return $this->renderAjax('_edit',compact('model'));
    }

    /**
     * Show course
     *
     * @param $id
     * @return array|string|Response
     * @throws NotFoundHttpException
     */
    public function actionShow($id)
    {
        //курс
        /* @var $model Course */
        $model = Course::find()->where(['id' => (int)$id])->one();

        //если не найдено
        if(empty($model)){
            throw new NotFoundHttpException('Страница не найдена',404);
        }

        //вывести форму редактирования
        return $this->render('show',compact('model'));
    }

    /**
     * Изменение настроек сезна
     * @param $id
     * @return array|string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        //курс
        /* @var $model Course */
        $model = Course::find()->where(['id' => (int)$id])->one();

        //если не найдено
        if(empty($model)){
            throw new NotFoundHttpException('Страница не найдена',404);
        }

        //AJAX валидация
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        //если пришли данные из POST и они успешно заружены в объект
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            //если все данный в итоге корректны
            if($model->validate()){
                $goods = Goods::changeStatus($model->id , $model->active);
                $model->save();

                //к списку
                return $this->redirect(Url::to(['/account/courses/index']));
            }
        }

        //вывести форму редактирования
        return $this->renderAjax('_edit',compact('model'));
    }
    
    /**
     * Удаление курса
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        //курс
        /* @var $model Course */
        $model = Course::find()->where(['id' => (int)$id])->one();

        //если не найдено
        if(empty($model)){
            throw new NotFoundHttpException('Страница не найдена',404);
        }
        // Статус устаревший для товаров этого курса
        $goods = Goods::changeStatus($model->id , 0);
        //удаление
        $model->delete();

        //назад
        return $this->redirect(Yii::$app->request->referrer);
    }
}