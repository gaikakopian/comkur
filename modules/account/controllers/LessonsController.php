<?php

namespace app\modules\account\controllers;

use app\helpers\Sort;
use app\models\Lesson;
use app\models\LessonSearch;
use app\models\LessonStatus;
use app\models\LessonType;
use app\models\StudentCourse;
use app\models\StudentLesson;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\Controller;

/**
 * Котроллер отвечающий за управление уроками
 *
 * @copyright 	2017 Alex Nem
 * @link 		https://github.com/darkoffalex
 * @author 		Alex Nem
 *
 * @package app\controllers
 */
class LessonsController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['director'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Вывод списка заготовок-уроков
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new LessonSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', compact('searchModel','dataProvider'));
    }

    /**
     * Создать новый урок
     *
     * @return array|string
     */
    public function actionCreate()
    {
        $model = new Lesson();

        $model->course_id = Yii::$app->request->get('courseId');

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            if($model->validate()){

                $model->priority = Sort::GetNextPriority(Lesson::className(), ['course_id' => $model->course_id]);
                $model->save();

                if($model->course_id) {

                    $studentCourses = StudentCourse::findAll(['course_id' => $model->course_id]);

                    // Append new lessons for existing students

                    if(sizeof($studentCourses) > 0) {

                        foreach($studentCourses as $studentCourse)
                        {
                            $studentLesson = new StudentLesson();

                            $studentLesson->parent_id = $studentCourse->id;
                            $studentLesson->priority = $model->priority;
                            $studentLesson->course_id = $model->course_id;
                            $studentLesson->lesson_id = $model->id;
                            $studentLesson->user_id = $studentCourse->user_id;
                            $studentLesson->type_id = LessonType::TYPE_OFFLINE;
                            $studentLesson->status_id = LessonStatus::STATUS_PLANNED;

                            // Set as available for first course lesson

                            if(sizeof($studentCourse->lessons) == 0)
                                $studentLesson->status_id = LessonStatus::STATUS_AVAILABLE;

                            $studentLesson->save();
                        }
                    }
                }

                return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : Url::to(['/account/lessons']));
            }
        }

        return $this->renderAjax('_create',compact('model'));
    }

    /**
     * Изменение настроек сезна
     * @param $id
     * @return array|string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        //урок
        /* @var $model Lesson */
        $model = Lesson::find()->where(['id' => (int)$id])->one();

        //если не найдено
        if(empty($model)){
            throw new NotFoundHttpException('Страница не найдена',404);
        }

        //AJAX валидация
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        //ID курса до загрузки данных из формы
        $oldCourseId = $model->course_id;

        //если пришли данные из POST и они успешно заружены в объект
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            //если все данный в итоге корректны
            if($model->validate()) {

                //если курс сменился
                if($oldCourseId != $model->course_id) {

                    // обновить приоритет

                    $model->priority = Sort::GetNextPriority(Lesson::className(), ['course_id' => $model->course_id]);

                    // обновить привязки

                    $studentLessons = StudentLesson::findAll(['course_id' => $oldCourseId, 'lesson_id' => $model->id]);

                    if(sizeof($studentLessons) > 0) {

                        foreach($studentLessons as $studentLesson)
                        {

                            $studentCourse = StudentCourse::findOne(['user_id' => $studentLesson->user_id, 'course_id' => $model->course_id]);

                            if($studentCourse) {
                                $studentLesson->course_id = $model->course_id;
                                $studentLesson->save();
                            } else
                            {
                                $studentLesson->delete();
                            }
                        }
                    }
                }

                //если удалось созранить - записать в сессию состояние для данной формы (для вывода таблички с сообщением об успехе)
                if($model->save()){
                    Yii::$app->session->setFlash(Lesson::className(),'true');
                }
            }
        }

        //вывести форму редактирования
        return $this->render('edit',compact('model'));
    }

    /**
     * Удаление урока
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        //урок
        /* @var $model Lesson */
        $model = Lesson::find()->where(['id' => (int)$id])->one();

        //если не найдено
        if(empty($model)){
            throw new NotFoundHttpException('Страница не найдена',404);
        }

        //удаление
        $model->delete();

        //назад
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Сменить положение урока (порядок следования в списке)
     * @param $id int
     * @param $dir string
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionMove($id, $dir)
    {
        //найти тему
        /* @var $model Lesson */
        $model = Lesson::findOne((int)$id);

        //если не найдна - 404
        if(empty($model)){
            throw new NotFoundHttpException('Страница не найдена',404);
        }

        //сменить порядок следования, сдвинуть вверх или вниз (только для группы относящейся к данному курсу)
        Sort::Move($model,$dir,Lesson::className(),['course_id' => $model->course_id]);


        //венуться на предыдущую страницу
        return $this->redirect(Yii::$app->request->referrer);
    }
}