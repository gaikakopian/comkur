<?php
/**
 * Created by PhpStorm.
 * User: zhek
 * Date: 30.08.17
 * Time: 16:55
 */

namespace app\modules\account\controllers;

use app\models\User;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class ApiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['director', 'admin'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Show API information
     *
     * @return string
     */
    public function actionIndex()
    {

        if(\Yii::$app->request->isPost) {

            /** @var User $user */
            $user = \Yii::$app->user->identity;

            $user->generateAuthKey();

            $user->save();

            $user->refresh();
        }

        return $this->render('index');
    }
}