<?php

namespace app\modules\account\controllers;

use app\helpers\Constants;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use app\models\User;
use app\models\LoginForm;
use yii\web\Controller;

/**
 * Базовый контроллер для модуля "Account"
 *
 * @copyright 	2017 Alex Nem
 * @link 		https://github.com/darkoffalex
 * @author 		Alex Nem
 *
 * @package app\modules\account\controllers
 */
class MainController extends Controller
{

    public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['director', 'admin'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Точка входа. В случае если пользователь авторизован происходит перенаправление в зависимости от роли
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        /** @var User $user */
        $user = Yii::$app->getUser()->identity;

        if($user->hasRole(Constants::RBAC_DIRECTOR))
            return $this->redirect(Url::to(['/account/users']));
        else
            return $this->redirect(Url::to(['/account/students']));
    }

    // Загрузка изображений для урока
    public function actionUpload(){

        if ($_FILES){
            $file = $_FILES['file'];

            if ($file['type'] == 'image/png'
                || $file['type'] == 'image/jpg'
                || $file['type'] == 'image/gif'
                || $file['type'] == 'image/jpeg'
                || $file['type'] == 'image/pjpeg')
                {
                //Проверка на существование директорий для хранения картинок если не существует они добавляются
                if (!file_exists(Yii::getAlias('@webroot').'/backend/images')) {
                    mkdir(Yii::getAlias('@webroot').'/backend/images', 0777, true);
                }
                if (!file_exists(Yii::getAlias('@webroot').'/backend/images/lessons')) {
                    mkdir(Yii::getAlias('@webroot').'/backend/images/lessons', 0777, true);
                }
                $filename = md5(date('YmdHis')).'.jpg';
                $new_image = Yii::getAlias('@webroot').'/backend/images/lessons/'.$filename;

                move_uploaded_file($file['tmp_name'] , $new_image);

                $array = array(
                    'url' => '/backend/images/lessons/'.$filename,
                    'id' => md5(date('YmdHis'))
                );

                echo stripslashes(json_encode($array));
            }
            
        }
    }

}
