<?php

namespace app\modules\account\controllers;

use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\helpers\Constants;
use app\models\PaymentsSearch;
use Yii;
use yii\web\Response;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use app\helpers\Sort;
use app\models\Payments;
use app\models\Goods;
use app\models\Course;
use app\models\StudentCourse;
use app\models\StudentLesson;
use app\models\CourseType;
use app\models\LessonStatus;

/**
 * Котроллер отвечающий за управление оплат
 *
 * @copyright 	2017 Gaik Akopian
 * @link 		https://github.com/darkoffalex
 * @author 		Gaik Akopian
 *
 * @package app\controllers
 */
class PaymentsController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [Constants::RBAC_DIRECTOR],
                    ],
                ],
            ]
        ];
    }

    /**
     * Вывод списка оплат
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PaymentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index',compact('searchModel','dataProvider'));
    }

    // Поттвеждение покупки
    public function actionBuySuccess($id){

            $payment = Payments::findOne(['id' => $id]);
            if ($payment){
                $good = Goods::findOne(['id' => $payment->good_id]);
                $baseCourse = Course::findOne($good->course_id);

                $payment->status = 'success';
                $payment->save();

                $course = new StudentCourse();

                $course->good_id = $good->id;
                $course->user_id = $payment->user_id;
                $course->course_id = $baseCourse->id;
                $course->access_for_part = isset($good->part) ? $good->part : null;
                $course->access_stage = isset($good->stage) ? $good->stage : null;
                $course->good_id = $good->id;
        
                $course->save();
                       
                foreach ($course->course->lessons as $courseLesson) {
        
                    $lesson = new StudentLesson();
        
                    $lesson->lesson_id = $courseLesson->id;
                    $lesson->course_id = $baseCourse->id;
                    $lesson->parent_id = $course->id;
                    $lesson->user_id = $payment->user_id;
                    $lesson->status_id = LessonStatus::STATUS_PLANNED;
        
                    // Set availability for demo courses
        
                    if($course->course->type_id == CourseType::TYPE_DEMO)
                        $lesson->status_id = LessonStatus::STATUS_AVAILABLE;
        
                    $lesson->save();
                }
            }
            //к списку
            return $this->redirect(Url::to(['/account/payments/index']));
    }
   
}