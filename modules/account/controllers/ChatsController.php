<?php
/**
 * Created by PhpStorm.
 * User: zhek
 * Date: 31.08.17
 * Time: 14:23
 */

namespace app\modules\account\controllers;

use app\models\Chat;
use app\models\ChatSearch;
use app\models\Message;
use app\models\User;
use app\models\UserStatus;
use yii\bootstrap\ActiveForm;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;

class ChatsController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['director', 'admin'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Show list chats
     *
     * @return string|array
     */
    public function actionIndex()
    {

        $searchModel = new ChatSearch();

        $searchModel->admin = true;

        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        $model = new Chat();

        $recipient = \Yii::$app->request->get('userId');

        $model->recipient_id = $recipient;

        if($model->recipient_id)
            $model->recipient_name = [$model->recipient_id => $model->recipient->name];

        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if(\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post())) {

            $model->author_id = \Yii::$app->user->identity->getId();

            $text = $model->message;

            if(!$model->recipient_group && !$model->recipient_name)
                $model->addError('recipient_name', 'Не указаны получатели сообщения');

            if(!$model->hasErrors()) {

                if($model->recipient_group) {

                    /** @var User[] $teachers */
                    $_users = User::find()
                        ->joinWith(['roles'])
                        ->where(['item_name' => $model->recipient_group])
                        ->andWhere(['status_id' => UserStatus::STATUS_ACTIVE])
                        ->all();

                    /** @var User $_user */
                    foreach($_users as $_user) {

                        $_chat = new Chat();

                        $_chat->attributes = $model->attributes;
                        $_chat->id = null;
                        $_chat->recipient_id = $_user->id;
                        $_chat->author_last_date = new Expression('NOW()');
                        $_chat->recipient_last_date = '0000-00-00 00:00:00';
                        $_chat->recipient_unread_count = 1;
                        $_chat->author_unread_count = 0;
                        $_chat->message = $text;

                        $_chat->save('false');

                        if($text) {

                            $message = new Message();

                            $message->author_id = $_chat->author_id;
                            $message->recipient_id = $_chat->recipient_id;
                            $message->chat_id = $_chat->id;

                            $message->content = $text;

                            $message->save();
                        }
                    }

                    if(sizeof($_users) == 1)
                        return $this->redirect(Url::to(['/account/chats/show/', 'id' => $_chat->id]));
                    else
                        return $this->redirect(Url::to(['/account/chats']));

                } elseif(is_array($model->recipient_name)) {

                    foreach($model->recipient_name as $_user) {

                        $_chat = new Chat();

                        $_chat->attributes = $model->attributes;
                        $_chat->id = null;
                        $_chat->recipient_id = $_user;
                        $_chat->author_last_date = new Expression('NOW()');
                        $_chat->recipient_last_date = '0000-00-00 00:00:00';
                        $_chat->recipient_unread_count = 1;
                        $_chat->author_unread_count = 0;
                        $_chat->message = $text;

                        $_chat->save('false');

                        if($text) {

                            $message = new Message();

                            $message->author_id = $_chat->author_id;
                            $message->recipient_id = $_chat->recipient_id;
                            $message->chat_id = $_chat->id;

                            $message->content = $text;

                            $message->save();
                        }
                    }

                    if(sizeof($model->recipient_name) == 1)
                        return $this->redirect(Url::to(['/account/chats/show/', 'id' => $_chat->id]));
                    else
                        return $this->redirect(Url::to(['/account/chats']));
                }
            }
        }

        return $this->render('index', compact('searchModel','dataProvider', 'model'));
    }

    /**
     * Send direct message
     * @return array|string|Response
     */
    public function actionMessage()
    {

        $model = new Chat();

        $model->author_id = \Yii::$app->user->identity->getId();
        $model->recipient_id = \Yii::$app->request->get('userId');
        $model->recipient_name = $model->recipient->name;

        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if(\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post())) {

            $text = $model->message;

            $model->recipient_unread_count = 1;

            if($model->save('false')) {

                $message = new Message();

                $message->author_id = $model->author_id;
                $message->recipient_id = $model->recipient_id;
                $message->chat_id = $model->id;

                $message->content = $text;

                $message->save();

                return $this->redirect(Url::to(['/account/chats/show', 'id' => $model->id]));

            }
        }

        return $this->renderAjax('_message', compact('model'));
    }

    /**
     * Show chat
     *
     * @param $id
     *
     * @return string
     */
    public function actionShow($id)
    {
        $model = Chat::findOne($id);

        $userId = \Yii::$app->user->identity->getId();

        if(!$model->recipient_id) {

            $model->recipient_id = $userId;

            // update messages

            Message::updateAll(['recipient_id' => $model->recipient_id], ['chat_id' => $id, 'recipient_id' => null]);
        }

        if($model->author_id == $userId) {
            $model->author_last_date = new Expression('NOW()');
            $model->author_unread_count = 0;
        } elseif($model->recipient_id == $userId) {
            $model->recipient_last_date = new Expression('NOW()');
            $model->recipient_unread_count = 0;
        }

        $model->save(false);

        if(\Yii::$app->request->isPost) {

            $text = \Yii::$app->request->post('message');

            if($text) {

                $message = new Message();

                $message->content = $text;
                $message->author_id = \Yii::$app->user->identity->getId();
                $message->chat_id = $id;

                if($model->author_id == $userId) {
                    $message->recipient_id = $model->recipient_id;
                    $model->recipient_unread_count = 1;
                } else {
                    $message->recipient_id = $model->author_id;
                    $model->author_unread_count = 1;
                }

                $model->save('false');
                $message->save();
            }
        }

        return $this->render('show', ['model' => $model]);
    }
}