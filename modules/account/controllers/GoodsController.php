<?php

namespace app\modules\account\controllers;

use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\helpers\Constants;
use app\models\GoodsSearch;
use Yii;
use yii\web\Response;
use app\models\Goods;
use app\models\Course;
use app\models\Lesson;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use app\helpers\Sort;
use yii\web\NotFoundHttpException;

/**
 * Котроллер отвечающий за управление товарами
 *
 * @copyright 	2017 Gaik Akopian
 * @link 		https://github.com/darkoffalex
 * @author 		Gaik Akopian
 *
 * @package app\controllers
 */
class GoodsController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [Constants::RBAC_DIRECTOR],
                    ],
                ],
            ]
        ];
    }

    /**
     * Вывод списка товаров
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new GoodsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index',compact('searchModel','dataProvider'));
    }

    /**
     * Создать новый товар
     * @return array|string
     */
    public function actionCreate()
    {
        //новый товар
        $model = new Goods();

        //AJAX валидация
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            
                                
            //задаем тип товара
            if($model->stage == 'all')
            {
                $model->type = 'all';
                $model->stage = null;
            }elseif($model->part == 'all'){
                $model->type = 'stage';
                $model->part = null;
            }else{
                $model->type = 'stage_part';
            }


            $good = Goods::findGood($model->course_id , $model->stage , $model->part);
            //проверка товара на существование
            if($good){
                return false;
            } 

            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        //если пришли данные из POST и они успешно заружены в объект
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            
            //задаем тип товара
            if($model->stage == 'all')
            {

                $model->type = 'all';
                $model->stage = '';
            }elseif($model->part == 'all'){
                $model->type = 'stage';
                $model->part = '';
            }else{
                $model->type = 'stage_part';
            }

            //загрузка фотографии
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
           
            if ($model->imageFile && $model->upload()){
                $model->photo = $model->imageName;                    
            }

            //если все данный в итоге корректны
            if($model->validate()){

                $model->priority = Sort::GetNextPriority(Goods::className());
                $model->save();

                //к списку
                return $this->redirect(Url::to(['/account/goods/index']));
            }
        }

        //вывести форму редактирования
        return $this->renderAjax('create',compact('model'));
    }

    /**
     * Изменение настроек товара
     * @param $id
     * @return array|string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        //задаем массивы ступеней и частей
        $stages = [];
        $parts = [];
                
        $model = Goods::findOne(['id' => $id]);
        
        if (!$model)
            throw new NotFoundHttpException('Страница не найдена',404);

        //заполняем массивы ступеней и частей
        $stages['all'] = 'Все ступени';
        foreach($model->course->stages as $key => $value){
            $tmp = $value->course_stage + 1;
            $stages[$key] = 'Ступень '.$tmp;
        }

        if (isset($model->stage)){
            $parts['all'] = 'Все Части';
            foreach($model->course->parts as $key => $value){
                $tmp = $value->course_part + 1;
                $parts[$key] = 'Часть '.$tmp;
            }   
        }
        
        //AJAX валидация
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {

            //задаем тип товара
            if($model->stage == 'all')
            {
                $model->type = 'all';
                $model->stage = null;
            }elseif($model->part == 'all'){
                $model->type = 'stage';
                $model->part = null;
            }else{
                $model->type = 'stage_part';
            }

            $good = Goods::findGoodUnique($model->course_id ,$model->id, $model->stage , $model->part);
            
            //проверка товара на существование
            if($good){
                return false;
            } 
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        //если пришли данные из POST и они успешно заружены в объект
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            //если все данный в итоге корректны
            if($model->validate()){
                //задаем тип товара
                if($model->stage == 'all')
                {
                    $model->type = 'all';
                    $model->stage = '';
                }elseif($model->part == 'all'){
                    $model->type = 'stage';
                    $model->part = '';
                }else{
                    $model->type = 'stage_part';
                }

                //загрузка фотографии и удаление старой
                $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            
                if ($model->imageFile && $model->upload()){
                    if (file_exists(Yii::getAlias('@webroot').'/backend/images/goods/'.$model->photo)) {
                        unlink(Yii::getAlias('@webroot').'/backend/images/goods/'.$model->photo);
                    }
                    $model->photo = $model->imageName;                    
                }

                $model->save();

                //к списку
                return $this->redirect(Url::to(['/account/goods/index']));
            }
        }

        //вывести форму редактирования
        return $this->renderAjax('create',compact('model' , 'stages' , 'parts'));
    }
    
    /**
     * Удаление товара
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        //курс
        /* @var $model Goods */
        $model = Goods::findOne(['id' => $id]);

        //если не найдено
        if(empty($model)){
            throw new NotFoundHttpException('Страница не найдена',404);
        }

        //удаление Фото
        if ($model->photo)
        if (file_exists(Yii::getAlias('@webroot').'/backend/images/goods/'.$model->photo)){
            unlink(Yii::getAlias('@webroot').'/backend/images/goods/'.$model->photo);
        }   
        //удаление
        $model->delete();

        //назад
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Сменить положение товара (порядок следования в списке)
     * @param $id int
     * @param $dir string
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionMove($id, $dir)
    {
        //найти тему
        /* @var $model Goods */
        $model = Lesson::findOne((int)$id);

        //если не найдна - 404
        if(empty($model)){
            throw new NotFoundHttpException('Страница не найдена',404);
        }

        //сменить порядок следования, сдвинуть вверх или вниз (только для группы относящейся к данному курсу)
        Sort::Move($model,$dir,Goods::className());


        //венуться на предыдущую страницу
        return $this->redirect(Yii::$app->request->referrer);
    }
    /**
     * Проверяет наличие ступеней у курса
     *  @return $array
     */
    public function actionCheckCourseStages(){

        Yii::$app->response->format = Response::FORMAT_JSON;

        $data = Yii::$app->request->post();
        // находит курс и возвращает ступени
        $course = Course::findOne(['id'=>$data['id']]);
        
        if (!$course)
            return false;
        
        return array('stages' => $course->stages);
    }
    /**
     * Проверяет наличие частей у курса
     *  @return $array
     */
    public function actionCheckCoursePart(){

        Yii::$app->response->format = Response::FORMAT_JSON;

        $data = Yii::$app->request->post();
        // находит курс и возвращает части
        $course = Course::findOne(['id'=> $data['id']]);
        
        if (!$course)
            return false;
        
        $parts = Lesson::find()
            ->where(['course_id' => $data['id']])
            ->andWhere(['course_stage' => $data['stage']])
            ->groupBy('course_part')
            ->all();

        return array('parts' => $parts);
    }
}