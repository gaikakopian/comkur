<?php

namespace app\modules\account\controllers;


use app\models\FreeLesson;
use app\models\Lesson;
use app\models\LessonSearch;
use app\models\LessonTheme;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Котроллер отвечающий за управление ситуативными уроками
 *
 * @copyright    2017 Alex Nem
 * @link        https://github.com/darkoffalex
 * @author        Alex Nem
 *
 * @package app\controllers
 */
class SituativeLessonsController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new LessonSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, false);

        return $this->render('index', compact('searchModel', 'dataProvider'));
    }

    /**
     * Create new situative lesson
     *
     * @return string|array
     */
    public function actionCreate()
    {
        $model = new Lesson();

        $model->theme_id = Yii::$app->request->get('themeId');

        if($model->theme_id) {

            $model->theme_id = (int) $model->theme_id;
            $currentTheme = LessonTheme::findOne($model->theme_id);

            if($currentTheme)
                $model->theme_name = $currentTheme->name;
        }

        //AJAX валидация
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        //если пришли данные из POST и они успешно заружены в объект
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            //если все данный в итоге корректны
            if ($model->validate()) {

                if (!empty($model->theme_name)) {

                    $theme_name = $model->theme_name;

                    /* @var $theme LessonTheme */
                    $theme = LessonTheme::find()->where(['like', 'name', $theme_name])->one();

                    if (empty($theme)) {
                        $theme = new LessonTheme();
                        $theme->name = $theme_name;
                        $theme->save();
                    }

                    $model->theme_id = $theme->id;
                }

                $model->save();

                return $this->redirect(Url::to(['/account/situative-lessons', 'LessonSearch[theme_id]' => $model->theme_id]));
            }
        }

        return $this->renderAjax('edit', compact('model'));
    }

    /**
     * @param $id
     *
     * @return array|string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /** @var Lesson $model */
        $model = Lesson::findOne(['id' => $id, 'course_id' => null]);

        if (empty($model)) {
            throw new NotFoundHttpException('Страница не найдена', 404);
        }
        //AJAX валидация
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        //если пришли данные из POST и они успешно заружены в объект
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            //если все данный в итоге корректны
            if ($model->validate()) {

                if (!empty($model->theme_name)) {

                    $theme_name = $model->theme_name;

                    /* @var $theme LessonTheme */
                    $theme = LessonTheme::find()->where(['like', 'name', $theme_name])->one();

                    if (empty($theme)) {
                        $theme = new LessonTheme();
                        $theme->name = $theme_name;
                        $theme->save();
                    }

                    $model->theme_id = $theme->id;
                }

                //базовые параметры
                if ($model->save()) {
                    Yii::$app->session->setFlash(Lesson::className(), 'true');
                }

            }
        }

        //вывести форму редактирования
        if (!empty($model->theme_id)) {
            $model->theme_name = $model->theme->name;
        }

        return $this->render('edit', compact('model'));
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = Lesson::findOne(['id' => $id, 'course_id' => null]);

        if (empty($model)) {
            throw new NotFoundHttpException('Страница не найдена', 404);
        }

        $model->delete();

        //к списку
        return $this->redirect(Url::to(['/account/situative-lessons/index']));
    }
}


