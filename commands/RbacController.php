<?php

namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        // User

        $showProfile = $auth->createPermission('showProfile');
        $showProfile->description = 'Show profile';
        $auth->add($showProfile);

        $updateProfile = $auth->createPermission('updateProfile');
        $updateProfile->description = 'Update profile';
        $auth->add($updateProfile);

        $uploadFile = $auth->createPermission('uploadFile');
        $uploadFile->description = 'Upload file';
        $auth->add($uploadFile);

        $uploadImage = $auth->createPermission('uploadImage');
        $uploadImage->description = 'Upload image';
        $auth->add($uploadImage);

        $downloadFile = $auth->createPermission('downloadFile');
        $downloadFile->description = 'Download file';
        $auth->add($downloadFile);

        // Create default user role

        $user = $auth->createRole('user');
        $user->description = 'Пользователь';
        $auth->add($user);

        $auth->addChild($user, $showProfile);
        $auth->addChild($user, $updateProfile);
        $auth->addChild($user, $uploadFile);
        $auth->addChild($user, $uploadImage);
        $auth->addChild($user, $downloadFile);

        // Users management

        $manageUsers = $auth->createPermission('manageUsers');
        $manageUsers->description = 'Manage users';
        $auth->add($manageUsers);

        $manageStudents = $auth->createPermission('manageStudents');
        $manageStudents->description = 'Manage students';
        $auth->add($manageStudents);

        $manageTeachers = $auth->createPermission('manageTeachers');
        $manageTeachers->description = 'Manage teachers';
        $auth->add($manageTeachers);

        $createUser = $auth->createPermission('createUser');
        $createUser->description = 'Create new user';
        $auth->add($createUser);

        $updateUser = $auth->createPermission('updateUser');
        $updateUser->description = 'Update existing user';
        $auth->add($updateUser);

        $deleteUser = $auth->createPermission('deleteUser');
        $deleteUser->description = 'Delete existing user';
        $auth->add($deleteUser);

        // Courses

        $manageCourses = $auth->createPermission('manageCourses');
        $manageCourses->description = 'Manage courses';
        $auth->add($manageCourses);

        $createCourse = $auth->createPermission('createCourse');
        $createCourse->description = 'Create a course';
        $auth->add($createCourse);

        $updateCourse = $auth->createPermission('updateCourse');
        $updateCourse->description = 'Update course';
        $auth->add($updateCourse);

        $deleteCourse = $auth->createPermission('deleteCourse');
        $deleteCourse->description = 'Delete course';
        $auth->add($deleteCourse);

        // Lessons

        $manageLessons = $auth->createPermission('manageLessons');
        $manageLessons->description = 'Manage lessons';
        $auth->add($manageLessons);

        $createLesson = $auth->createPermission('createLesson');
        $createLesson->description = 'Create a lesson';
        $auth->add($createLesson);

        $updateLesson = $auth->createPermission('updateLesson');
        $updateLesson->description = 'Update lesson';
        $auth->add($updateLesson);

        $deleteLesson = $auth->createPermission('deleteLesson');
        $deleteLesson->description = 'Delete lesson';
        $auth->add($deleteLesson);

        $learnLesson = $auth->createPermission('learnLesson');
        $learnLesson->description = 'Learn lesson';
        $auth->add($learnLesson);

        $reviewLesson = $auth->createPermission('reviewLesson');
        $reviewLesson->description = 'Review lesson';
        $auth->add($reviewLesson);

        $planLesson = $auth->createPermission('planLesson');
        $planLesson->description = 'Plan (deferred) lesson';
        $auth->add($planLesson);

        // Create admin role

        $admin = $auth->createRole('admin');
        $admin->description = 'Администратор';
        $auth->add($admin);
        $auth->addChild($admin, $user);

        // Create teacher role

        $teacher = $auth->createRole('teacher');
        $teacher->description = 'Преподаватель';
        $auth->add($teacher);

        $auth->addChild($teacher, $user);
        $auth->addChild($teacher, $reviewLesson);
        $auth->addChild($teacher, $planLesson);

        // Create student role

        $student = $auth->createRole('student');
        $student->description = 'Ученик';
        $auth->add($student);

        $auth->addChild($student, $user);
        $auth->addChild($student, $learnLesson);

        // Assign permissions

        $auth->addChild($admin, $manageTeachers);
        $auth->addChild($admin, $manageStudents);

        // Create director role

        $director = $auth->createRole('director');
        $director->description = 'Директор';
        $auth->add($director);

        $auth->addChild($director, $user);
        $auth->addChild($director, $admin);
        $auth->addChild($director, $teacher);

        $auth->addChild($director, $manageUsers);
        $auth->addChild($director, $createUser);
        $auth->addChild($director, $updateUser);
        $auth->addChild($director, $deleteUser);

        $auth->addChild($director, $manageLessons);
        $auth->addChild($director, $createLesson);
        $auth->addChild($director, $updateLesson);
        $auth->addChild($director, $deleteLesson);

        $auth->addChild($director, $manageCourses);
        $auth->addChild($director, $createCourse);
        $auth->addChild($director, $updateCourse);
        $auth->addChild($director, $deleteCourse);

        $auth->assign($director, 1);
    }
}