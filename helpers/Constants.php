<?php

namespace app\helpers;

/**
 * Хелпер. Содержит используемые системой константы. Менять занчения крайне не рекомендуется если система уже развернута
 *
 * @copyright 	2017 Alex Nem
 * @link 		https://github.com/darkoffalex
 * @author 		Alex Nem
 *
 * @package app\helpers
 */
class Constants
{
    //общие состояния
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;
    const STATUS_DELETED = -1;

    //состояние пользователя
    const USR_STATUS_NEW = 0; //создан но не активирован
    const USR_STATUS_ENABLED = 1; //активирован
    const USR_STATUS_DISABLED = 2; //деактивирован (бан)
    const USR_STATUS_DEMO = 3; //демо-режим

    //роли пользователей
    const ROLE_DIRECTOR = 1;
    const ROLE_ADMIN = 2;
    const ROLE_TEACHER = 3;
    const ROLE_STUDENT = 4;

    const RBAC_DIRECTOR = 'director';
    const RBAC_ADMIN = 'admin';
    const RBAC_TEACHER = 'teacher';
    const RBAC_STUDENT = 'student';

    //тип курса
    const COURSE_TYPE_REGULAR = 1;
    const COURSE_TYPE_DEMO = 2;

    //тип урока ученика
    const LESSON_TYPE_REGULAR = 1;
    const LESSON_TYPE_SITUATIVE = 2;

    //статусы ответов ученика
    const LESSON_NEW = 1;
    const LESSON_AT_CHECK = 2;
    const LESSON_CHECKED = 3;
}
