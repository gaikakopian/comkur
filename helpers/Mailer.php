<?php
/**
 * Created by PhpStorm.
 * User: zhek
 * Date: 31.08.17
 * Time: 11:20
 */

namespace app\helpers;

use app\models\Template;
use app\models\User;
use yii\web\NotFoundHttpException;

class Mailer
{
    /**
     * Send notice by code
     *
     * @param string $code
     * @param User|int $recipient
     * @param array $variables
     *
     * @return mixed
     *
     * @throws NotFoundHttpException
     * @throws \Swift_TransportException
     */
    public static function send($code, $recipient, $variables = [])
    {

        /** @var Template $template */
        $template = Template::findOne(['code' => $code]);

        if(!$template)
            throw new NotFoundHttpException('Template not found');

        /** @var User $user */
        $user = is_numeric($recipient) ? User::findOne($recipient) : $recipient;

        if(!$user)
            throw new NotFoundHttpException('Recipient not found');

        if(is_array($variables)){
            $subject = trim(str_replace(array_keys($variables), array_values($variables), $template->title));
            $body = trim(str_replace(array_keys($variables), array_values($variables), $template->content));
        } else {
            $subject = trim($template->title);
            $body = trim($template->content);
        }

        $mail = \Yii::$app->mailer->compose()
            ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name])
            ->setTo(strtolower($user->username))
            ->setSubject($subject)
            ->setTextBody(strip_tags($body))
            ->setHtmlBody($body);

        try {
            return $mail->send();

        } catch(\Swift_TransportException $exception)
        {
            \Yii::error($exception->getMessage());
            throw $exception;
        }
    }
}
