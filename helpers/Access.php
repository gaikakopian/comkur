<?php

namespace app\helpers;

use app\models\User;
use yii\web\IdentityInterface;

/**
 * Хелпер. Предназначен для разграничения доступа по ролям.
 *
 * @copyright 	2017 Alex Nem
 * @link 		https://github.com/darkoffalex
 * @author 		Alex Nem
 *
 * @package app\helpers
 */
class Access
{
    /**
     * Проверка доступа. Может ли пользователь совершать указанное действие
     * @param User|IdentityInterface $user
     * @param string $controllerID
     * @param null|string $actionID
     * @return bool
     */
    public static function canPerformAction($user, $controllerID, $actionID = null)
    {
        if(is_numeric($user)){
            $user = User::findOne((int)$user);
        }

        if(empty($user)){
            return false;
        }

        //все методы и контроллеры что включены в этот массив - доступны только для указанных ролей
        $methods = [
            //пользователи
            'users/*' => [Constants::ROLE_DIRECTOR],
            'courses/*' => [Constants::ROLE_DIRECTOR],
            'lessons/*' => [Constants::ROLE_DIRECTOR],
            'situative-lessons/*' => [Constants::ROLE_DIRECTOR]
        ];

        if(empty($actionID)){
            if(!empty($methods[$controllerID.'/*'])) {
                $roles = $methods[$controllerID.'/*'];
                return in_array($user->role_id,$roles);
            }
        }else{
            if(!empty($methods[$controllerID.'/'.$actionID])){
                $roles = $methods[$controllerID.'/'.$actionID];
                return in_array($user->role_id,$roles);
            }elseif(!empty($methods[$controllerID.'/*'])){
                $roles = $methods[$controllerID.'/*'];
                return in_array($user->role_id,$roles);
            }
        }

        return true;
    }

    /**
     * Проверка уровня роли (у кого выше приоритет)
     * @param $user1 User|IdentityInterface
     * @param $user2 User|IdentityInterface|int
     * @param bool $andEqual
     * @return bool
     */
    public static function isHigherThan($user1, $user2, $andEqual = false){

        //приоритеты ролей
        $roleLevels = [
            Constants::ROLE_DIRECTOR=> 20,
            Constants::ROLE_ADMIN => 10,
            Constants::ROLE_TEACHER => 5,
            Constants::ROLE_STUDENT => 0,
        ];

        //в зависимости от того что передао - получить роль из объекта или из числа
        $role1 = is_numeric($user1) ? $user1 : $user1->role_id;
        $role2 = is_numeric($user2) ? $user2 : $user2->role_id;

        //сравнение - больше или равно или просто больше
        return $andEqual ? $roleLevels[$role1] >= $roleLevels[$role2] : $roleLevels[$role1] > $roleLevels[$role2];
    }

    /**
     * Проверка - может ли пользователь удалить дургого пользователя
     * @param $currentUser User|IdentityInterface
     * @param $user User|IdentityInterface
     * @return bool
     */
    public static function canDeleteUser($currentUser,$user)
    {
        if(!self::canPerformAction($currentUser,'users','delete')){
            return false;
        }

        if($currentUser->id == $user->id){
            return false;
        }

        return self::isHigherThan($currentUser,$user);
    }

    /**
     * Проверка - может ли пользователь менять другого пользователя
     * @param $currentUser User|IdentityInterface
     * @param $user User|IdentityInterface
     * @return bool
     */
    public static function canEditUser($currentUser,$user)
    {
        if(!self::canPerformAction($currentUser,'users','update')){
            return false;
        }

        if($currentUser->id == $user->id){
            return true;
        }

        return self::isHigherThan($currentUser,$user);
    }
}