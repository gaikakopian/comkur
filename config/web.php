<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'Application',
    'name' => 'Application',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log','thumbnail'],
    'language' => 'ru',
    'modules' => [
        'gridview' =>  'kartik\grid\Module',
        'account' => [
            'class' => 'app\modules\account\AccountModule'
        ],
        'teacher' => [
            'class' => 'app\modules\teacher\TeacherModule',
        ],
        'student' => [
            'class' => 'app\modules\student\StudentModule',
        ],
        'api' => [
            'class' => 'app\modules\api\ApiModule',
        ],
    ],
    'components' => [
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'sourceLanguage' => 'en-US',
                ],
            ],
        ],
        'thumbnail' => [
            'class' => 'himiklab\thumbnail\EasyThumbnail',
            'cacheAlias' => 'assets/thumbnails',
        ],
        'assetManager' => [
            'bundles' => [
                'dmstr\web\AdminLteAsset' => [
                    'skin' => 'skin-blue',
                ],
            ],
        ],
        'request' => [
            'cookieValidationKey' => 'Inv98aJIqVcdG-5g34NaHHMvOdbD3Z9q',
            'baseUrl' => '',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'main/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            /*'transport' => [
                'class' => 'Swift_SmtpTransport',

                'host' => 'smtp.gmail.com',
                'username' => 'webapplications.testing@gmail.com',
                'password' => '354fsdkiose4!',
                'port' => '465',
                'encryption' => 'ssl',
            ] */
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => \yii\log\FileTarget::className(),
                    'levels' => ['error', 'warning', 'info'],
                ],
                [
                    'class' => \yii\log\FileTarget::className(),
                    'levels' => ['info'],
                    'categories' => ['info'],
                    'logVars' => [],
                    'logFile' => '@app/runtime/logs/info.log'
                ]
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [

                'account' => 'account/main/index',
                'account/<controller>' => 'account/<controller>/index',
                'account/<controller>/<action>/<id:\d+>' => 'account/<controller>/<action>',
                'account/<controller>/<action>' => 'account/<controller>/<action>',

                'student' => 'student/default/index',
                'student/<controller>' => 'student/<controller>/index',
                'student/<controller>/<action>/<id:\d+>' => 'student/<controller>/<action>',
                'student/<controller>/<action>' => 'student/<controller>/<action>',

                'teacher' => 'teacher/default/index',
                'teacher/<controller>' => 'teacher/<controller>/index',
                'teacher/<controller>/<action>/<id:\d+>' => 'teacher/<controller>/<action>',
                'teacher/<controller>/<action>' => 'teacher/<controller>/<action>',


                '/' => 'main/index',
                '/login' => 'main/login',
                '/logout' => 'main/logout',

                '<controller>' => '<controller>/index',
                '<controller>/<action>/<id:\d+>/<title:\w+(-\w+)*>' => '<controller>/<action>',
                '<controller>/<action>/<id:\d+>/<status:\d+>' => '<controller>/<action>',
                '<controller>/<action>/<id:\d+>' => '<controller>/<action>',
                '<controller>/<action>' => '<controller>/<action>',
            ],
        ],
        'formatter' => [
            'dateFormat' => 'dd.MM.yyyy',
            'datetimeFormat' => 'dd.MM.yyyy HH:mm:ss',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => 'RUR',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1', '::1', '78.56.14.109', '78.31.184.83']
    ];
}

return $config;
